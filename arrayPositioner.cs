﻿using UnityEngine;

public class arrayPositioner : MonoBehaviour
{
    int x;
    int y;
    Block[,] m_arr;
    int m_Location;

    public void init(Block[,] arr, int xpos, int ypos, int location)
    {
        m_arr = arr;
        x = xpos;
        y = ypos;
        m_Location = location;
    }

    // Every time Enter is triggered, the "array coordinates" are set.
    // This can happen many times when things fall, however ultimately it will turn out correct.
    void OnTriggerEnter(Collider other)
    {
        BlockScript o = other.gameObject.GetComponent<BlockScript>();
        o.m_Location = m_Location;
        o.m_Block.arrPosX = x;
        o.m_Block.arrPosY = y;
        m_arr[x, y] = o.m_Block;
    }

    void OnTriggerExit(Collider other)
    {
        BlockScript o = other.gameObject.GetComponent<BlockScript>();
        if(o.m_Location == BlockScript.BLOCKLOCATION.SIDE)
        {
            m_arr[x, y] = null;
        }
    }
}
