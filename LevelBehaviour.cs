﻿using UnityEngine;
using System.Collections;

// This class only exists so we can use MonoBehaviour
public class LevelBehaviour : MonoBehaviour
{
    public void fallingEvent()
    {
        StartCoroutine(checkFalling());
    }

    public void timedEvent(Objective_Timed t)
    {
        StartCoroutine(timerRunning(t));
    }

    public void startNewLevel(float wait, int level)
    {
        StartCoroutine(startLevelWait(wait, level));
    }

    public void setLevelStatusAfterWaitingFor(int status, float seconds)
    {
        StartCoroutine(waitStatus(status, seconds));
    }

    public void doUPText(int x, int y, int points, string word)
    {
        StartCoroutine(UPText(x, y, points, word));
    }

    public void doREWARDText(int x, int y, int points)
    {
        StartCoroutine(RewardText(x, y, points));
    }

    public void doMoveCamera(int cameraMovement)
    {
        switch (cameraMovement)
        {
            case CAMERA_MOVEMENT.BEGIN: StartCoroutine(moveCameraParent(true)); break;
            case CAMERA_MOVEMENT.EXIT_FROM_LEVEL: StartCoroutine(moveCameraParent(false)); break;
            case CAMERA_MOVEMENT.LEVEL_PROGRESSION: StartCoroutine(moveCameraProgression()); break;
        }
    }

    public void moveAndDeactivateSparky(float fromx, float fromy, float tox, float toy)
    {
        StartCoroutine(moveAndDeactivateSpark1(fromx, fromy, tox, toy));
        StartCoroutine(moveAndDeactivateSpark2(tox, toy, fromx, fromy));
    }

    public void menyMove(bool enter)
    {
        StartCoroutine(moveMeny(enter));
    }

    public void menyMoveUpDown()
    {
        StartCoroutine(moveMenyUpAndDown());
    }

    public void bounce(GameObject o)
    {
        StartCoroutine(bounceObject(o));
    }

    private IEnumerator bounceObject(GameObject o)
    {
        int moves = 0;
        Vector3 pos1 = new Vector3(o.transform.localPosition.x, -0.85f, o.transform.localPosition.z);
        Vector3 pos2 = new Vector3(o.transform.localPosition.x + 0.5f, -0.85f, o.transform.localPosition.z);
        Vector3 startPos;
        Vector3 endPos;
        float startTime = 0;

        while (moves < 8 && o.activeSelf)
        {
            if(moves % 2 == 0)
            {
                startPos = pos1;
                endPos = pos2;
            }
            else
            {
                startPos = pos2;
                endPos = pos1;
            }

            startTime = Time.time;
            while (Time.time - startTime <= 1.1f)
            {
                o.transform.localPosition = Vector3.Lerp(startPos, endPos, Time.time - startTime);
                startTime -= 0.01f;
                yield return null;
            }
            moves++;
        }
        o.SetActive(false);

    }

    private IEnumerator moveMenyUpAndDown()
    {
        yield return new WaitForSeconds(0.2f);

        // Move from GAME to Main menu
        Vector3 endPos = new Vector3(0, 7, 8);
        Vector3 startPos = new Vector3(0, 0, 8);

        GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition = startPos;

        float startTime = Time.time;

        while (Time.time - startTime <= 1.1f)
        {
            GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition = Vector3.Lerp(startPos, endPos, Time.time - startTime);
            startTime -= 0.02f;
            yield return null;
        }

        yield return new WaitForSeconds(0.3f);

        // Now change menu
        GAMEGLOBAL.LVL.m_Menu.GetComponent<MenuLogic>().mainMenu();
        startTime = Time.time;
        endPos = new Vector3(0, 0, 8);
        startPos = GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition;

        // Move down
        while (Time.time - startTime <= 1.2f)
        {
            GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition = Vector3.Lerp(startPos, endPos, Time.time - startTime);
            startTime -= 0.01f;
            yield return null;
        }
    }

    private IEnumerator moveMeny(bool enter)
    {
        Vector3 endPos;
        Vector3 startPos;

        if (enter)
        {
            // Enter from above (appear)
            GAMEGLOBAL.LVL.m_Menu.SetActive(true);
            endPos = new Vector3(0, 0, 8);
            startPos = new Vector3(0, 7, 8);
        }
        else
        {
            // Move to above (disappear)
            yield return new WaitForSeconds(0.2f);
            endPos = new Vector3(0, 7, 8);
            startPos = new Vector3(0, 0, 8);
        }

        GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition = startPos;

        float startTime = Time.time;
        while (Time.time - startTime <= 1.1f)
        {
            GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition = Vector3.Lerp(startPos, endPos, Time.time - startTime);
            startTime -= 0.02f;
            yield return null;
        }

        if (!enter)
        {
            // Deactivate menu object
            GAMEGLOBAL.LVL.m_Menu.SetActive(false);
        }
    }



    private IEnumerator moveAndDeactivateSpark1(float fromx, float fromy, float tox, float toy)
    {
        GAMEGLOBAL.LVL.turnSpark1OnOff(true);

        Vector3 endPos = new Vector3(tox, toy, -2f);
        Vector3 startPos = GAMEGLOBAL.LVL.m_Spark1.gameObject.transform.position;
        float startTime = Time.time;

        // Move right
        while (Time.time - startTime <= 1.1f)
        {
            GAMEGLOBAL.LVL.m_Spark1.gameObject.transform.position = Vector3.Lerp(startPos, endPos, Time.time - startTime);
            startTime -= 0.03f;
            yield return null;
        }
        GAMEGLOBAL.LVL.turnSpark1OnOff(false);
    }


    private IEnumerator moveAndDeactivateSpark2(float fromx, float fromy, float tox, float toy)
    {
        GAMEGLOBAL.LVL.turnSpark2OnOff(true);

        Vector3 endPos = new Vector3(tox, toy, -2f);
        Vector3 startPos = GAMEGLOBAL.LVL.m_Spark2.gameObject.transform.position;
        float startTime = Time.time;

        // Move right
        while (Time.time - startTime <= 1.1f)
        {
            GAMEGLOBAL.LVL.m_Spark2.gameObject.transform.position = Vector3.Lerp(startPos, endPos, Time.time - startTime);
            startTime -= 0.02f;
            yield return null;
        }
        GAMEGLOBAL.LVL.turnSpark2OnOff(false);
    }

    private IEnumerator moveCameraProgression()
    {
        yield return new WaitForSeconds(0.5f);

        Vector3 endPos = new Vector3(20, GAMEGLOBAL.LVL.m_CameraParent.transform.position.y, GAMEGLOBAL.LVL.m_CameraParent.transform.position.z);
        Vector3 startPos = GAMEGLOBAL.LVL.m_CameraParent.transform.position;
        float startTime = Time.time; 

        // Move right
        while (Time.time - startTime <= 1.1f)
        {
            GAMEGLOBAL.LVL.m_CameraParent.transform.position = Vector3.Lerp(startPos, endPos, Time.time - startTime);
            startTime -= 0.01f;
            yield return null;
        }

        // Teleport back to -10
        GAMEGLOBAL.LVL.m_CameraParent.transform.position = new Vector3(-10, GAMEGLOBAL.LVL.m_CameraParent.transform.position.y, GAMEGLOBAL.LVL.m_CameraParent.transform.position.z);
        endPos = new Vector3(4.25f, GAMEGLOBAL.LVL.m_CameraParent.transform.position.y, GAMEGLOBAL.LVL.m_CameraParent.transform.position.z);
        startPos = GAMEGLOBAL.LVL.m_CameraParent.transform.position;
        startTime = Time.time;

        // Move right again
        while (Time.time - startTime <= 1.1f)
        {
            GAMEGLOBAL.LVL.m_CameraParent.transform.position = Vector3.Lerp(startPos, endPos, Time.time - startTime);
            startTime -= 0.01f;
            yield return null;
        }
        yield return null;
    }

    private IEnumerator moveCameraParent(bool right)
    {
        Vector3 endPos;
        Vector3 startPos = GAMEGLOBAL.LVL.m_CameraParent.transform.position;
        if (right)
        {
            yield return new WaitForSeconds(0.4f);
            endPos = new Vector3(4.25f, GAMEGLOBAL.LVL.m_CameraParent.transform.position.y, GAMEGLOBAL.LVL.m_CameraParent.transform.position.z);
        }
        else
            endPos = new Vector3(-10, GAMEGLOBAL.LVL.m_CameraParent.transform.position.y, GAMEGLOBAL.LVL.m_CameraParent.transform.position.z);

        float startTime = Time.time; 

        while (Time.time - startTime <= 1.1f)
        {
            GAMEGLOBAL.LVL.m_CameraParent.transform.position = Vector3.Lerp(startPos, endPos, Time.time - startTime);
            startTime -= 0.01f;
            yield return null;
        }
        yield return null;
    }

    private IEnumerator UPText(int x, int y, int points, string word)
    {
        GAMEGLOBAL.LVL.m_UPText.setText("<c2>" + word + "<c/>");
        Vector3 pos = GAMEGLOBAL.LVL.m_UPText.transform.position;
        pos.x = x;
        pos.y = y + 0.5f;
        int moves = 0;

        GAMEGLOBAL.LVL.m_UPText.gameObject.SetActive(true);
        while (moves < 50)
        {
            moves++;
            pos.y += 0.005f;
            GAMEGLOBAL.LVL.m_UPText.transform.position = pos;
            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(0.5f);
        GAMEGLOBAL.LVL.m_UPText.gameObject.SetActive(false);
    }

    private IEnumerator RewardText(int x, int y, int coins)
    {
        GAMEGLOBAL.LVL.m_RewardPopup.setText("<c2>+" + coins + "<c/>$");
        Vector3 pos = GAMEGLOBAL.LVL.m_RewardPopup.transform.localPosition;
        pos.x = x;
        pos.y = y + 0.5f;
        int moves = 0;

        GAMEGLOBAL.LVL.m_RewardPopup.gameObject.SetActive(true);
        while (moves < 50)
        {
            moves++;
            pos.y += 0.005f;
            GAMEGLOBAL.LVL.m_RewardPopup.transform.localPosition = pos;
            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(0.5f);
        GAMEGLOBAL.LVL.m_RewardPopup.gameObject.SetActive(false);
    }

    private IEnumerator waitStatus(int status, float seconds)
    {
        GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = LEVEL_STATUS.WAITING;
        yield return new WaitForSeconds(seconds);

        if(status == LEVEL_STATUS.SUCCESS)
            GAMEGLOBAL.LVL.m_Behaviour.doREWARDText(0, 0, 25);

        GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = status;
    }

    private IEnumerator startLevelWait(float seconds, int level)
    {
        GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = LEVEL_STATUS.WAITING;
        yield return new WaitForSeconds(seconds);
        GAMEGLOBAL.LVL.startLevel(level);
    }

    private IEnumerator timerRunning(Objective_Timed t)
    {
        // Give some time in the start for the falling event to take place...
        yield return new WaitForSeconds(2f);

        while (!t.m_Failed && !t.m_Complete)
        {
            t.m_checkTime = true;
            yield return new WaitForSeconds(1.1f);

            // Paused? Just continue until not..
            if (GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus == LEVEL_STATUS.PAUSED)
                continue;

            // Check again just before so we dont failed or succeeded before the check occured
            if (t.m_Failed || t.m_Complete)
                break;

            t.checkObjective();
            t.m_checkTime = false;
        }
    }

    private IEnumerator checkFalling()
    {
        // All objects must have falled down to their position before we can resume
        bool tryAgain = true;
        while (tryAgain)
        {
            tryAgain = false;
            yield return new WaitForSeconds(0.5f);
            for (int i = 0; i < GAMEGLOBAL.LVL.m_Boxes.Count; i++)
            {
                if (GAMEGLOBAL.LVL.m_Boxes[i].m_Script.gameObject.activeSelf && GAMEGLOBAL.LVL.m_Boxes[i].m_Gravity)
                {
                    if (GAMEGLOBAL.LVL.m_Boxes[i].arrPosX != -1 && GAMEGLOBAL.LVL.m_Boxes[i].arrPosY != -1)
                    {
                        Rigidbody r = GAMEGLOBAL.LVL.m_Boxes[i].m_Script.GetComponent<Rigidbody>();
                        
                        if (r.velocity.y < -1)
                        {
                            tryAgain = true;
                            break;
                        }
                    }
                    else
                    {
                        tryAgain = true;
                        break;
                    }
                }
            }
        }

        // Check objectives once more here, just star-objectives, after everything has falled down.
        GAMEGLOBAL.LVL.m_currentLevel.checkAfterFallObjectives();

        GAMEGLOBAL.LVL.m_isEvent = false;
        yield return null;
    }
}
