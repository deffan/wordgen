﻿using UnityEngine;
using System.Collections;
using System;

public class dailyQuestScript : MonoBehaviour
{
    private Mesh m_flat = null;
    public Objective m_Objective;
    private textScript m_TextScriptQuest;
    private textScript m_TextScriptReward;
    private bool initiated = false;
    private GameObject m_Button;
    public int m_CoinReward;
    public string m_QuestText;
    public bool m_Active;
    public int m_ID;
    public int m_AdType;
    public dailyQuestButtonScript m_ButtonScript;

    public void init()
    {
        if (!initiated)
        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/textFab");
            temp = GameObject.Instantiate(temp);
            temp.transform.SetParent(gameObject.transform);
            temp.transform.localPosition = new Vector3(-3.9f, -0.03f, 0);
            m_TextScriptQuest = temp.GetComponent<textScript>();

            temp = GameObject.Instantiate(temp);
            temp.transform.SetParent(gameObject.transform);
            temp.transform.localPosition = new Vector3(4.1f, -0.03f, 0);
            m_TextScriptReward = temp.GetComponent<textScript>();

            temp = (GameObject)Resources.Load("Prefabs/flat");
            m_flat = temp.GetComponent<MeshFilter>().sharedMesh;

            m_flat = gameObject.transform.GetChild(0).GetComponent<MeshFilter>().mesh;

            initiated = true;
            m_TextScriptQuest.init(0.4f);
            m_TextScriptReward.init(0.4f);
            m_Button = transform.GetChild(1).gameObject;
            m_ButtonScript = transform.GetChild(1).GetComponent<dailyQuestButtonScript>();
            m_ButtonScript.m_Quest = this;
        }
    }

    public void setQuest(int adtype, int id, string text, string reward, int coins)
    {
        m_ID = id;
        m_AdType = adtype;

        // Check if available
        if (GAMEGLOBAL.LVL.isQuestReady(m_ID))
        {
            m_QuestText = text;
            setText(m_QuestText);
            setReward(reward, coins);
            setReady();
        }
        else
            setDone();
    }

    public void setDone()
    {
        setGray();
        GAMEGLOBAL.LVL.setQuestDone(m_ID);
        setText("<c1>Quest Completed!</c> Come back later...");
        GAMEGLOBAL.LVL.setUVs(m_Button.GetComponent<MeshFilter>().mesh, 113, 35, 2048, 299, 1298);
        m_Active = false;
    }

    public void setReady()
    {
        setGreen();
        GAMEGLOBAL.LVL.setUVs(m_Button.GetComponent<MeshFilter>().mesh, 113, 35, 2048, 299, 1260);
        m_Active = true;
    }

    public void setReward(string txt, int coins)
    {
        m_TextScriptReward.setText(txt);
        m_CoinReward = coins;
    }

    public void setText(string txt)
    {
        m_TextScriptQuest.setText(txt);
    }

    public void addText(string txt)
    {
        m_TextScriptQuest.addText(txt);
    }

    public void setGray()
    {
        GAMEGLOBAL.LVL.setUVs(m_flat, 614, 35, 2048, 0, 1118);
    }

    public void setGreen()
    {
        GAMEGLOBAL.LVL.setUVs(m_flat, 614, 35, 2048, 0, 1156);
    }

}
