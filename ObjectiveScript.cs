﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectiveScript : MonoBehaviour
{
    private Mesh m_flat = null;
    private ParticleSystem m_ParticleSystem = null;
    public Objective m_Objective;
    public hintScript m_Hint;
    private textScript m_TextScript;
    private bool initiated = false;

    public void init()
    {
        if (!initiated)
        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/textFab");
            temp = GameObject.Instantiate(temp);
            temp.transform.SetParent(gameObject.transform);
            temp.transform.localPosition = new Vector3(-3.55f, -0.03f, 0);
            m_TextScript = temp.GetComponent<textScript>();

            temp = (GameObject)Resources.Load("Prefabs/flat");
            m_flat = temp.GetComponent<MeshFilter>().sharedMesh;

            m_ParticleSystem = gameObject.transform.GetChild(0).GetChild(0).GetComponent<ParticleSystem>();
            m_flat = gameObject.transform.GetChild(0).GetComponent<MeshFilter>().mesh;
            m_Hint = gameObject.transform.GetChild(1).GetComponent<hintScript>();
            m_Hint.init();
            initiated = true;
            m_TextScript.init(0.4f);
        }
        
        setGray();
    }

    public void setText(string txt)
    {
        m_TextScript.setText(txt);
    }

    public void addText(string txt)
    {
        m_TextScript.addText(txt);
    }

    public void setHintStatus(int status)
    {
        m_Hint.setHintStatus(status);
    }

    public void setHint(string txt, bool cost)
    {
        m_Hint.setHint(txt, cost);
    }

    public void setGray()
    {
        setUVs(614, 35, 2048, 0, 1400);
    }

    public void setGreen()
    {
        m_ParticleSystem.Play();
        setUVs(614, 35, 2048, 0, 1437);
        setHintStatus(hintScript.STATUS.HIDDEN);
    }

    public void setRed()
    {
        setUVs(614, 35, 2048, 0, 1474);
        setHintStatus(hintScript.STATUS.HIDDEN);
    }

    public void setUVs(float tileSize_x, float tileSize_y, float textureSize, float textureCoordinate_x, float textureCoordinate_y)
    {
        // To get top-left based coordinates...
        textureCoordinate_y = textureSize - tileSize_y - textureCoordinate_y;

        float offsetX = tileSize_x / textureSize;
        float offsetY = tileSize_y / textureSize;
        float x = textureCoordinate_x / textureSize;
        float y = textureCoordinate_y / textureSize;

        Vector2[] frontUVS = m_flat.uv;

        frontUVS[0].x = x;
        frontUVS[0].y = y;

        frontUVS[1].x = x + offsetX;
        frontUVS[1].y = y;

        frontUVS[2].x = x + offsetX;
        frontUVS[2].y = y + offsetY;

        frontUVS[3].x = x;
        frontUVS[3].y = y + offsetY;

        m_flat.uv = frontUVS;
    }
}
