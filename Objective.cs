﻿using System.Collections.Generic;

public class Objective 
{
    public class TYPE
    {
        public const int FIND_WORD = 0;
        public const int FIND_STARTING_ENDING = 1;
        public const int FIND_X_WORDS = 2;
        public const int FINISH_WORD = 3;
        public const int PALINDROME = 4;
        public const int CONSECUTIVE = 5;
        public const int TWICE = 6;
        public const int FIND_X_LETTERS = 7;
        public const int CONTAINING = 8;
        public const int ATTEMPTS = 9;
        public const int STAR = 10;
        public const int INCREASING_LENGTH = 11;
        public const int TIMED = 12;
        public const int ALPHABET = 13;
        public const int CATEGORY = 14;
        public const int MANUAL = 15;
    }

    public int m_Type;
    public bool m_Complete;
    public bool m_Failed;
    public string m_Description;
    public ObjectiveScript m_ObjectiveScript;

    protected string word;
    protected List<string> allWords;
    protected int attempts;
    protected bool wordFound;
    protected List<Objective> allObjectives;

    // Basklass
    public Objective()
    {
        m_Description = "";
        m_Type = -1;
        m_Complete = false;
        m_Failed = false;


        for (int i = 0; i < GAMEGLOBAL.LVL.m_Objectives.Count; i++)
        {
            if (!GAMEGLOBAL.LVL.m_Objectives[i].transform.gameObject.activeSelf)
            {
                m_ObjectiveScript = GAMEGLOBAL.LVL.m_Objectives[i];
                m_ObjectiveScript.m_Objective = this;
                GAMEGLOBAL.LVL.m_Objectives[i].transform.gameObject.SetActive(true);
                break;
            }
        }

        m_ObjectiveScript.init();
    }



    public void setData(string w, List<string> aw, int a, bool found, List<Objective> allObj)
    {
        word = w;
        allWords = aw;
        attempts = a;
        wordFound = found;
        allObjectives = allObj;
    }

    public virtual bool checkObjective()
    {
        return m_Complete;
    }


    // Palindrom bör ha "min length"
    // "increasing" bör inte användas? Är oklar och kanske tråkig.
    // "twice" är Ok men lite trist.
    // 

    // Fixa till "ANY" så att man FÖRST kollar igenom existerande ord, och kollar om det matchar.
    // Så objective blir fädigt. SEN om inte detta blev match, tar man första bästa i databasen.
    // Och då är det bara specifika ord som gäller, tex om man specifikt vill hitta "Find This"

    // KAn vara random från första start.
    // Ord länge än 5 är mkt svårt.. så vida man inte kan skapa order eftersom på nåt vis?
    // Detta kanske är ett annat objective?
    // Build the word... typ som hangman funktionalitet. Måste vara tydlig hur detta fungerar och skiljer sig från de andra då.
    // "Complete the word: *****" within X attempts
    // Direkt man hittar matchande bokstav så fylls den i.
    // Här ska "ANY" matcha första bästa?

    // level 1-10 ska ha ordlängd 3
    // 10-20 ska ha 3-4
    // 20-30 ska ha 4-5


    // Sahar vill ha poäng. Känns bättre. Men till vad?


    // Ok,
    // På höger sida ska det finnas en stapel med bokstäver man kan använda sig av.
    // Dessa bokstäver går klicka på, och sen sätta ut på valfri plats på spelplanen.
    // Denna bokstav ersätter då den man klickar på.
    // Man tjänar in bokstäver beroende på antalet ord man rensar. Kortare ord ger lägre poäng osv.
    // i vissa fall kanske man startar med lite bokstäver redan
    // bokstäverna som fås ska vara relevanta till det man antagligen behöver! (borde kunna vara bomb och annat också)

    // Hur ska spelaren fatta detta då?
    // Skapa ett "help bubble" första gången något uppträder?
    // Typ, en box som bara berättar vad det är.
    // Med en knapp [Ok]
    // går inte avbryta i detta läget.

    // [ICON] This is the "Any" block. It matches any letter.
    // [Ok]

    // Earn free blocks by making words. Longer words will earn you free blocks faster.
    // Click on a free block and then click again on the location on the board to use it.
    // [Ok]




    // Use all letters in the alphabet: ABCDEFGHIJKLMNOPQRSTUVW (omgivna av <color> och varje bokstav {n})
    // Har en stringbuilder i sig.
    // OBS! Behöver enbart kolla "current word" inte loopa alla ord varje gång.

    // Spelplanen görs mkt bredare, med utrymme för stapel.
    // Kanske ett snäpp högre också?
    // Den enda "limit" är ju egentligen att det inte blir för svårt att selecta på mobilen.


    // Objectet man får ska falla ner med glitter! Helst regnbågsglitter... kanske flera particlesystems? Prefab. Som automatiskt tas bort vid kollision ned?
    // Samt en text som UPText fast som säger: "Earned a Switch Block!"

    // Nya specialblock som enbart kan existera som rewards:
    // Crush/Hammer Block - Bra för att rensa stenar också. Stenar och Star ska ej gå ta bort med vanlig replace.
    // Rewrite Block
    // Advance Block (A->B->C)
    
    // Varje replace bör ha en "POFF" liknande effekt på plats (framför blocket)

    // Switch blocken på sidan bör dock byta varje gång så man kanske hittar nåt bra match

    // Men oftast får man bara vanliga bokstäver som man mest troligt behöver.
    // Kan bestämmas manuellt i Begin_1()





    // Det kanske inte är så lätt att veta vad som finns i tex solsystemet eller speciellt periodiska systemet.
    // Behöver nån mekanism som ger hjälp efter ett tag?
    // Kanske samla poäng, och poäng ger möjlighet till "clue" ?
    // Svårt att ge ledtråd när det finns så många olika att välja mellan...

    // Nåja, vill avsluta detta spelet så snabbt som möjligt nu ärligt talat.
    //
    // 1. Inför random hämting av ord och skapande av level i nån form av stigande svårighetsgrad till en viss nivå.
    // 2. Fixa databasen.
    // 3. Lägg till Ads.
    // 4. Se till så att progress sparas och hämtas från databasen.

    // BONUS:
    // Palindrome bonus +1 vanlig random bokstav
    // 4 word bonus +1 vanlig random bokstav
    // 5+ word bonus +1 switcher eller bomb



    // Varje kategori har egen tabell i DB, klass: Objective_Category(int cat, int amount)

    // Kategorier:
    //
    // Numbers (upto 20)
    // Countries
    // Fruits
    // Names
    // Animals
    // Colors
    // 


    // Other "x" include: abc, abcd...

    // På nåt vis bör man få veta/tips om andra saker i db.
    // 


    // Objective_Math() - "5 + 1 = ?"
    // What is X + Y
    // What is X - Y
    // Etc.
    // Answer? The number in letters





    // Find X words with a minimum length of Y

    // Find a word for each letter in the alfabet
    // Här borde det finnas en mindre font som visar progress för varje bokstav som då ändras till grön.
    // Detta borde vara det enda objektivet för denna level.

    // Tidsbaserat objective...
    // 1. Måste uppdateras oberoende av alla andra objectives.
    // 2. Måste kunna faila leveln 
    // 3. Måste kunna uppdateras.
    //
    // Lösning? Skapa en coroutine som kör varje sekund. Ganska enkel kontroll.
    // Sen kan den sätta level-status till failed?
    //
    // Complete the level in: x seconds, stringbuilder här.

    // Use 

    // Borde fungera bra på Tetris-nofill mode:
    // Clear one row once
    // Clear one column once

    // Klass -> Objective_Box
    // Make the box reach the bottom
    // Do not let the box reach the bottom 
    // Do not let any boxes reach the bottom
    // Make all boxes reach the bottom

    // Find 3 words in which the first letter creates: "CAR"

    // Avoid these characters: A,C,H,E,J <--- OBS! Då får inga uppenbara mismatches med andra objectives ske....
    // Detta objective måste skapas sist, och hämta objectives listan från LVL objektet och gå igenom denna.
    // Fel? -> FAILED.

    // Find in this order: 'Ben', 'Car', 'Sat'
    // Fel ordning? -> FAILED.

    // Find "XXX" within Y seconds (Y seconds left).    <-- Kräver ytterligare timer-hantering.
    // Fel? -> FAILED.
    // Kanske ha en/flera event i levelbehavior som kör varje sekund, men bara om den startats.
    // Och då kan det vara en speciell Objective som har timer.

    // Complete all objectives within X seconds (X seconds left)

    // Prefix / Suffix-klass 
    /*

        -s third person singular present
        -ed past tense
        -t past tense
        -ing progressive/continuous
        -en past participle
        -s plural
        -en plural (irregular)
        -er comparative
        -est superlative
        -n't negative

    https://en.wikipedia.org/wiki/Prefix
    https://en.wikipedia.org/wiki/Suffix
    */


    // Find a country - Kräver tabell i DB med alla länder på engelska
    // Find a fruit - Samma, med frukt.
    // Find a name - Samma...
    // Find a ... number, animal, planet(kräver utplacering)

    // Sen synonymer... skulle kräva enorm databas?

    // Sen verb, etc?



}
