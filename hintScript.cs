﻿using UnityEngine;
using System.Collections;

public class hintScript : MonoBehaviour
{
    public class STATUS
    {
        public const int HIDDEN = 0;
        public const int GRAYED = 1;
        public const int SHOW = 2;
    }

    private Mesh m_flat = null;
    public int m_status;
    public string m_Text;
    public bool m_Cost;

    public void init()
    {
        if (m_flat == null)
            m_flat = gameObject.transform.GetComponent<MeshFilter>().mesh;
        m_Text = "";
        m_status = STATUS.SHOW;
        gameObject.SetActive(false);
    }

    public void deactivate()
    {
        gameObject.SetActive(false);
        m_Text = "";
    }

    public void setHint(string txt, bool cost)
    {
        m_Cost = cost;
        m_Text = txt;
        gameObject.SetActive(true);
    }

    public void setHintStatus(int status)
    {
        switch (status)
        {
            case STATUS.HIDDEN: gameObject.SetActive(false); break;
            case STATUS.GRAYED:
                if (m_Cost)
                    setUVs(59, 35, 2048, 122, 1354);
                break;
            case STATUS.SHOW:
                if (m_Cost)
                    setUVs(59, 35, 2048, 61, 1354);
                else
                    setUVs(59, 35, 2048, 0, 1354);
                break;
            default: break;
        }
    }

    public void setUVs(float tileSize_x, float tileSize_y, float textureSize, float textureCoordinate_x, float textureCoordinate_y)
    {
        // To get top-left based coordinates...
        textureCoordinate_y = textureSize - tileSize_y - textureCoordinate_y;

        float offsetX = tileSize_x / textureSize;
        float offsetY = tileSize_y / textureSize;
        float x = textureCoordinate_x / textureSize;
        float y = textureCoordinate_y / textureSize;

        Vector2[] frontUVS = m_flat.uv;

        frontUVS[0].x = x;
        frontUVS[0].y = y;

        frontUVS[1].x = x + offsetX;
        frontUVS[1].y = y;

        frontUVS[2].x = x + offsetX;
        frontUVS[2].y = y + offsetY;

        frontUVS[3].x = x;
        frontUVS[3].y = y + offsetY;

        m_flat.uv = frontUVS;
    }

}
