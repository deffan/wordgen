﻿using UnityEngine;
using System.Collections;

public class BlockScript : MonoBehaviour
{
    public class BLOCKLOCATION
    {
        public const int GAME = 0;
        public const int SIDE = 1;
        public const int BUYMENU = 2;
    }

    public Mesh m_FrontMesh;
    public Mesh m_BackgroundMesh;
    public Block m_Block;
    public int m_Location;

    public void init(Mesh frontMesh, Mesh backgroundMesh)
    {
        m_BackgroundMesh = backgroundMesh;
        m_FrontMesh = frontMesh;
    }

    public void setFrontUVs(float x, float y)
    {
        // The UVs are apparently upsidedown in the Y-direction, so to get a zero-based coordinate system we do this:
        int reverse = (int)(2048f / 64f) - 1;
        y = reverse - y;

        Vector2[] frontUVS = m_FrontMesh.uv;

        frontUVS[0].x = (x * GAMEGLOBAL.LVL.m_UVOffset);
        frontUVS[0].y = (y * GAMEGLOBAL.LVL.m_UVOffset);

        frontUVS[1].x = GAMEGLOBAL.LVL.m_UVOffset + (x * GAMEGLOBAL.LVL.m_UVOffset);
        frontUVS[1].y = (y * GAMEGLOBAL.LVL.m_UVOffset);

        frontUVS[2].x = GAMEGLOBAL.LVL.m_UVOffset + (x * GAMEGLOBAL.LVL.m_UVOffset);
        frontUVS[2].y = GAMEGLOBAL.LVL.m_UVOffset + (y * GAMEGLOBAL.LVL.m_UVOffset);

        frontUVS[3].x = (x * GAMEGLOBAL.LVL.m_UVOffset);
        frontUVS[3].y = GAMEGLOBAL.LVL.m_UVOffset + (y * GAMEGLOBAL.LVL.m_UVOffset);

        m_FrontMesh.uv = frontUVS;
    }

    public void setBackUVs(float x, float y)
    {
        // The UVs are apparently upsidedown in the Y-direction, so to get a zero-based coordinate system we do this:
        int reverse = (int)(2048f / 64f) - 1;
        y = reverse - y;

        Vector2[] backgroundUVS = m_BackgroundMesh.uv;

        backgroundUVS[0].x = (x * GAMEGLOBAL.LVL.m_UVOffset);
        backgroundUVS[0].y = (y * GAMEGLOBAL.LVL.m_UVOffset);

        backgroundUVS[1].x = GAMEGLOBAL.LVL.m_UVOffset + (x * GAMEGLOBAL.LVL.m_UVOffset);
        backgroundUVS[1].y = (y * GAMEGLOBAL.LVL.m_UVOffset);

        backgroundUVS[2].x = GAMEGLOBAL.LVL.m_UVOffset + (x * GAMEGLOBAL.LVL.m_UVOffset);
        backgroundUVS[2].y = GAMEGLOBAL.LVL.m_UVOffset + (y * GAMEGLOBAL.LVL.m_UVOffset);

        backgroundUVS[3].x = (x * GAMEGLOBAL.LVL.m_UVOffset);
        backgroundUVS[3].y = GAMEGLOBAL.LVL.m_UVOffset + (y * GAMEGLOBAL.LVL.m_UVOffset);

        m_BackgroundMesh.uv = backgroundUVS;
    }

    public void addBombSpark()
    {
        if(m_Block.m_Type == Block.TYPE.BOMB && GetComponent<ParticleSystem>() == null)
        {
            GameObject o = (GameObject)Resources.Load("Prefabs/SparkFab");
            o = GameObject.Instantiate(o);
            o.transform.SetParent(transform);
            o.transform.localPosition = new Vector3(-0.5f, 0.25f, -1);
            o.transform.localScale = new Vector3(0.2f, 1, 0.2f);
        }
    }

    public void addGoldSpark()
    {
        if (m_Block.m_Type == Block.TYPE.GOLD && GetComponent<ParticleSystem>() == null)
        {
            GameObject o = (GameObject)Resources.Load("Prefabs/GoldFab");
            o = GameObject.Instantiate(o);
            o.transform.SetParent(transform);
            o.transform.localPosition = new Vector3(0, 0, -1);
            o.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void nextNumber()
    {
        if(m_Block.m_Type == Block.TYPE.BOMB)
            StartCoroutine(nextNumberLogic());
    }

    private IEnumerator nextNumberLogic()
    {
        float step = 0.0333f;
        float steps = 0;
        int s = 0;

        while(s < 30)
        {
            s++;
            steps += step;
            setFrontUVs(((BombBlock)m_Block).m_Count + 1 + 0.07f - steps, 2.9f);
            yield return new WaitForSeconds(0.0333f);
        }
        yield return null;
    }

    public void nextCharacter(int oldCharacter)
    {
        StartCoroutine(nextCharacterLogic(oldCharacter));
    }

    private IEnumerator nextCharacterLogic(int old)
    {
        // Distance to move
        int dist = Mathf.Abs(old - m_Block.m_LetterValue);

        // Move back or forward?
        int pos = 1;
        if (old > m_Block.m_LetterValue)
            pos = -1;
    
        float step = dist/30f;
        float steps = 0;
        int s = 0;

        while (s < 30)
        {
            s++;
            steps += step;
            setFrontUVs(old + (pos*steps), 0);
            yield return new WaitForSeconds(0.0333f);
        }
        yield return null;
    }
}
