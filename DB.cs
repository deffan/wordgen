﻿using Mono.Data.Sqlite;
using System.Data;
using System;
using UnityEngine;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class DB
{
    string m_ConnectionString;
    StringBuilder m_SB;
    int[] m_wordLength;

    public DB()
    {
        m_SB = new StringBuilder();

        // The amount of words per length in the "words" table
        m_wordLength = new int[15];
        m_wordLength[0] = 0;
        m_wordLength[1] = 0;
        m_wordLength[2] = 450;
        m_wordLength[3] = 2067;
        m_wordLength[4] = 6927;
        m_wordLength[5] = 15182;
        m_wordLength[6] = 28521;
        m_wordLength[7] = 40217;
        m_wordLength[8] = 49561;
        m_wordLength[9] = 51197;
        m_wordLength[10] = 43889;
        m_wordLength[11] = 35950;
        m_wordLength[12] = 27872;
        m_wordLength[13] = 20080;
        m_wordLength[14] = 13636;

        bool editor = false;
        string filepath = "";

        #if UNITY_EDITOR
            editor = true; 
        #endif

        // Load from datapath if Editor
        if(editor)
        {
            filepath = Application.dataPath + "/words.s3db";
            Debug.Log("Editor mode.");
        }
        else
        {
            // Else load database from persistent path in Android
            filepath = Application.persistentDataPath + "wordsTESTx.s3db";

            // If the database does not exist there, we move it there from StreamingAssets
            if (!File.Exists(filepath))
            {
                Debug.Log("Database do NOT exist.");
                WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/words.s3db");

                // This locks/blocks the thread while it happens, but it does not matter too much as it only happens once.
                // And in the very start of the program, before anything is shown.
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                while (!loadDB.isDone)
                {
                    // 20 seconds is too much, if this happens we throw exeption to get out
                    if (sw.ElapsedMilliseconds > 20000)
                        throw new Exception("WWW load of Database timed out.");
                }

                // Errors?
                if (!string.IsNullOrEmpty(loadDB.error))
                    throw new Exception(loadDB.error);

                // save to Application.persistentDataPath
                File.WriteAllBytes(Application.persistentDataPath + "wordsTESTx.s3db", loadDB.bytes);

                Debug.Log("Database should now be there....");
            }
            else
                Debug.Log("Database does exist there.");
        }

        m_ConnectionString = "URI=file:" + filepath;

        Debug.Log(m_ConnectionString);
    }

    public void Dispose()
    {
        m_SB = null;
    }

    public bool isWord(string word)
    {
        bool result = false;

        using (SqliteConnection connection = new SqliteConnection(m_ConnectionString))
        {
            try
            {
                m_SB.AppendFormat("select * from words where word = '{0}' and length = {1}", word.ToLower(), word.Length);

                connection.Open();
                SqliteCommand command = new SqliteCommand(m_SB.ToString(), connection);
                m_SB.Length = 0;

                SqliteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                    result = true;

            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        return result;
    }

    // When using the "ANY" block, we must try matching with "LIKE"
    public string isAnyWord(string word)
    {
        string w = "";

        using (SqliteConnection connection = new SqliteConnection(m_ConnectionString))
        {
            try
            {
                m_SB.AppendFormat("select word from words where word LIKE '{0}' LIMIT 1", word.ToLower());

                connection.Open();
                SqliteCommand command = new SqliteCommand(m_SB.ToString(), connection);
                m_SB.Length = 0;

                SqliteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                    w = reader.GetString(0);

            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        return w;
    }

    public List<string> getWordsMatching(string from, string word, int length)
    {
        if (length > 14)
            throw new Exception("YOU DID SOMETHING STUPID NOW! GetRandomWord length > 14");

        List<string> result = new List<string>();

        using (SqliteConnection connection = new SqliteConnection(m_ConnectionString))
        {
            try
            {
                m_SB.AppendFormat("select word from {0} where word LIKE '{1}' and length = {2} LIMIT 25", from, word.ToLower(), length);

                connection.Open();
                SqliteCommand command = new SqliteCommand(m_SB.ToString(), connection);
                m_SB.Length = 0;

                SqliteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                    result.Add(reader.GetString(0));

            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        return result;
    }

        // ****** OBS *******
        // NUVARANDE DATABAS INNEHÅLLER / ' TECKEN OSV. Måste rensas bort. Annars kommer "ANY" blocket hitta dessa.
        // Samt att getRandomWord kan hitta dem, så det blir omöjligt att klara leveln .


        // TODO:
        //
        // Manuellt fixa till kategorierna igen.
        // Fixa så att de har lengthorder kolumn.
        // Skapa fler kategorier.

        // Skapa fler levlar.
        // Fler objectives.

        // Skapa BUY swipen.
        // Den ska fungera enkelt. Block som inte är upplåsta ännu ska kanske öppna Hint fönstret med ett meddelande:
        // This block is avaiable at level 5,10,15 ...

        // LJUD!



    public List<string> getCategoryWordsMatching(string from)
    {

        // Låt oss även skicka in ett ID, som kan matcha ett element i en array, där vissa kategori-tabeller har en egen ordning?
        // 0 - sport
        // 1 - colors
        // etc.

        // Kategori-tabellerna har ingen ordning baserad på längd, utan bara antalet ord där.

        List<string> result = new List<string>();

        using (SqliteConnection connection = new SqliteConnection(m_ConnectionString))
        {
            try
            {
                m_SB.AppendFormat("select name from {0} ORDER BY RANDOM() LIMIT 25", from);

                connection.Open();
                SqliteCommand command = new SqliteCommand(m_SB.ToString(), connection);
                m_SB.Length = 0;

                SqliteDataReader reader = command.ExecuteReader();

                while(reader.Read())
                    result.Add(reader.GetString(0));

            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        return result;
    }

    // Vänta på svar? Surfa mer?
    // Prova nyare version av Sqlite om det finns? 

    // Skapa fler levlar.
    // Fixa buy.
    // Ta bort google-ads.
    // Thats it.


    public bool findWhatFrom(string from, string what)
    {
        bool result = false;
        using (SqliteConnection connection = new SqliteConnection(m_ConnectionString))
        {
            try
            {
                m_SB.AppendFormat("select name from {0} where name = '{1}' limit 1", from, what.ToLower());

                connection.Open();
                SqliteCommand command = new SqliteCommand(m_SB.ToString(), connection);
                m_SB.Length = 0;

                SqliteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                    result = true;

            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        return result;
    }

    public string getRandomWord(int length)
    {
        if (length > 14)
            throw new Exception("YOU DID SOMETHING STUPID NOW! GetRandomWord length > 14");

        string result = "";
        using (SqliteConnection connection = new SqliteConnection(m_ConnectionString))
        {
            try
            {
                m_SB.AppendFormat("select word from words where length = {0} and lengthorder >= {1} LIMIT 1", length, GAMEGLOBAL.LVL.m_Random.Next(0, m_wordLength[length] - 1));

                connection.Open();
                SqliteCommand command = new SqliteCommand(m_SB.ToString(), connection);
                m_SB.Length = 0;

                SqliteDataReader reader = command.ExecuteReader();

                if (reader.Read())
                    result = reader.GetString(0).ToUpper();

            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        return result;
    }

    public void insertCategory(string textFilePath, string category)
    {
        IDbConnection m_Connection = new SqliteConnection(m_ConnectionString);
        IDbCommand m_Command = m_Connection.CreateCommand();
        m_Connection.Open();

        using (var transaction = m_Connection.BeginTransaction())
        {
            var lines = File.ReadAllLines(textFilePath);
            foreach (var line in lines)
            {
                m_Command.CommandText = string.Format("Insert into {0} (NAME) VALUES ('{1}')", category, line);
                m_Command.ExecuteNonQuery();
            }

            transaction.Commit();
        }

        m_Connection.Close();
        m_Command.Dispose();
        m_Connection.Dispose();
    }

    public void insertWords()
    {
        IDbConnection m_Connection = new SqliteConnection(m_ConnectionString);
        IDbCommand m_Command = m_Connection.CreateCommand();

        try
        {
            m_Connection.Open();

            int[] lengths = new int[15];
            using (var transaction = m_Connection.BeginTransaction())
            {
                var lines = File.ReadAllLines("words.txt");
                foreach (var line in lines)
                {
                    if (line.Length < 15)    // Words longer than this is pretty much meaningless to have anyway
                    {
                        m_Command.CommandText = string.Format("Insert into words (WORD, LENGTH, LENGTHORDER) VALUES ('{0}', {1}, {2})", line, line.Length, lengths[line.Length]);
                        m_Command.ExecuteNonQuery();
                        lengths[line.Length]++;
                    }
                }
                transaction.Commit();
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
        }

        m_Connection.Close();
        m_Command.Dispose();
        m_Connection.Dispose();
    }


}
