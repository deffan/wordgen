﻿using System.Collections;

public class Block 
{
    public class TYPE
    {
        public const int NULL = -1;
        public const int LETTER = 0;
        public const int STONE = 1;
        public const int ANY = 2;
        public const int BOMB = 3;
        public const int SWITCH = 4;
        public const int GOLD = 5;
        public const int STAR = 6;
        public const int SWAP = 7;
    }

    public bool m_selected;
    public int m_ID;
    public int arrPosX;
    public int arrPosY;
    public int m_LetterValue;
    public char m_LetterChar;
    public bool m_Gravity;
    public bool m_Destroyable;
    public bool m_Selectable;
    public BlockScript m_Script;
    public int m_Type;
    public UnityEngine.Color m_BlockColor;

    public int background_x_coord;
    public int background_y_coord;
    public int character_y_coord;

    public Block(int id)
    {
        m_ID = id;
        arrPosX = -1;
        arrPosY = -1;
        m_Type = -1;
        background_x_coord = 0;
        background_y_coord = 0;
        character_y_coord = 0;
        m_Gravity = true;
        m_Destroyable = true;
        m_Selectable = true;
        m_BlockColor = UnityEngine.Color.white;
        m_LetterValue = -1;
    }

    public void setCharacter(char character, int charvalue)
    {
        m_LetterValue = charvalue;
        m_LetterChar = character;
        m_Script.setFrontUVs(m_LetterValue, character_y_coord);
    }

    public void setRandomCharacter()
    {
        int charValue = GAMEGLOBAL.LVL.getRandomFrequency();
        setCharacter(GAMEGLOBAL.LVL.getCharacter(charValue), charValue);
    }

    public void setGravity(bool ON)
    {
        m_Gravity = ON;
        UnityEngine.Rigidbody r = m_Script.GetComponent<UnityEngine.Rigidbody>();
        r.isKinematic = !ON;
    }

    public virtual void logic()
    {
        // Logic, such as bomb ticking down, bomb exploding, switcher changing character, etc
    }

    public virtual void init()
    {
        // Set uvs etc.
    }

    public virtual void select()
    {
        m_selected = true;
    }

    public virtual void deselect()
    {
        m_selected = false;
    }

    public void reset()
    {
        arrPosX = -1;
        arrPosY = -1;
        m_Script.setBackUVs(0, 0);
        m_Script.setFrontUVs(0, 0);
    }

}
