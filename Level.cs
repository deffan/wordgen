﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level
{
    public int m_Level;
    private RaycastHit m_Hit;
    public List<Objective> m_Objectives;
    public int m_LevelStatus;
    protected List<string> allWords;
    protected int m_ObjectivesComplete;
    protected int m_Attempts;
    protected int[] m_colObjectCount;
    private Block m_PreviouslySelectedBlock;
    protected int[] m_LetterExistance;
    protected int m_BlockFallCounter;

    // Startinglist
    protected List<Block> m_StartList;

    // Drops, how many of each level, defauly zero and only normal letters
    protected int droprate_ANY = 0;
    protected int droprate_SWITCH = 0;
    protected int droprate_STONE = 0;
    protected int droprate_BOMB = 0;
    protected int droprate_GOLD = 0;
    protected int droprate = 20;

    // Poins
    protected int m_currentPoints;

    public Level(int lvl)
    {
        m_BlockFallCounter = 0;
        m_currentPoints = 0;
        m_Level = lvl;
        m_Attempts = 0;
        m_ObjectivesComplete = 0;
        allWords = new List<string>();
        m_Objectives = new List<Objective>();
        m_StartList = new List<Block>();
        m_LevelStatus = LEVEL_STATUS.NOTSTARTED;
        m_colObjectCount = new int[GAMEGLOBAL.LVL.GAME_WIDTH];
        GAMEGLOBAL.LVL.m_isEvent = true;
        m_PreviouslySelectedBlock = null;
        m_LetterExistance = new int[26];
    }

    // Begin_1
    public virtual void Begin_1()
    {
        
    }

    // Begin_2s - THIS MUST BE OVERRIDDEN IF YOU WANT TO START YOUR OWN TETRIS EVENT!!!
    public virtual void Begin_2()
    {
        firstTetrisEvent(null);
    }

    // Begin_3
    public void Begin_3()
    {
       
    }

    public void setDrop(int type, int amount)
    {
        switch(type)
        {
            case Block.TYPE.ANY: droprate_ANY = amount; break;
            case Block.TYPE.SWITCH: droprate_SWITCH = amount; break;
            case Block.TYPE.STONE: droprate_STONE = amount; break;
            case Block.TYPE.BOMB: droprate_BOMB = amount; break;
            case Block.TYPE.GOLD: droprate_GOLD = amount; break;
            default: break;
        }
    }

    // This is the "local gameloop"
    public void Run ()
    {
        if (GAMEGLOBAL.LVL.m_isEvent || m_LevelStatus == LEVEL_STATUS.FAILED || m_LevelStatus == LEVEL_STATUS.SUCCESS)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            if (!Physics.Raycast(GAMEGLOBAL.LVL.m_Camera.ScreenPointToRay(Input.mousePosition), out m_Hit, 100))
                return;

            // We need to hit something (with a collider on it)
            if (!m_Hit.transform)
                return;

            BlockScript bs = m_Hit.collider.gameObject.GetComponent<BlockScript>();

            // Turn off hint
            if (GAMEGLOBAL.LVL.m_HintStatus)
                GAMEGLOBAL.LVL.setHint(false, "");

            // Get marked object
            if (bs != null)
            {
                Block marked = bs.m_Block;

                // Sidebar or normal object?
                if(marked.m_Script.m_Location == BlockScript.BLOCKLOCATION.SIDE)
                {
                    // If "SWAP BLOCK" has been placed, dont allow any selection on sidebar
                    if (m_PreviouslySelectedBlock != null && m_PreviouslySelectedBlock.m_Type == Block.TYPE.SWAP && ((SwapBlock)m_PreviouslySelectedBlock).m_Activated)
                        return;

                    // Select/Deselect for sidebar object
                    if (marked.m_selected)
                    {
                        marked.deselect();

                        // Deselect previous if any
                        if (m_PreviouslySelectedBlock != null)
                            m_PreviouslySelectedBlock.deselect();
                        m_PreviouslySelectedBlock = null;

                        GAMEGLOBAL.LVL.turnPoofOnOff(false);
                        return;
                    }
                    else
                    {
                        marked.select();

                        // Deselect previous if any
                        if (m_PreviouslySelectedBlock != null)
                            m_PreviouslySelectedBlock.deselect();

                        // POOF
                        GAMEGLOBAL.LVL.m_Poof.gameObject.transform.position = new Vector3(
                        marked.m_Script.transform.position.x,
                        marked.m_Script.transform.position.y,
                        0.5f);
                        GAMEGLOBAL.LVL.turnPoofOnOff(true);

                        m_PreviouslySelectedBlock = marked;
                        return;
                    }
                }
                else
                {
                    // If no sidebar object was clicked on previously just return, otherwise do logic for this.
                    if (m_PreviouslySelectedBlock == null)
                    {
                        return;
                    }
                    else
                    {
                        if(m_PreviouslySelectedBlock.m_Type == Block.TYPE.SWAP && ((SwapBlock)m_PreviouslySelectedBlock).m_Activated)
                        {
                            // Deactivate SWAP-block
                            m_PreviouslySelectedBlock.m_Script.gameObject.SetActive(false);

                            // Create a new block
                            Block swapped = ((SwapBlock)m_PreviouslySelectedBlock).m_Swapped;

                            // Create 2 fader explosions on each location
                            ParticleSystem f = GAMEGLOBAL.LVL.getInactiveFader();
                            f.startColor = marked.m_BlockColor;
                            f.gameObject.transform.position = new Vector3(
                            marked.m_Script.transform.position.x,
                            marked.m_Script.transform.position.y,
                            0);
                            f.gameObject.SetActive(true);
                            f.Play();

                            ParticleSystem f1 = GAMEGLOBAL.LVL.getInactiveFader();
                            f1.startColor = marked.m_BlockColor;
                            f1.gameObject.transform.position = new Vector3(
                            m_PreviouslySelectedBlock.m_Script.transform.position.x,
                            m_PreviouslySelectedBlock.m_Script.transform.position.y,
                            0);
                            f1.gameObject.SetActive(true);
                            f1.Play();

                            // Now switch positions of the blocks, and destroy the SWAP-block.
                            swapped.m_Script.gameObject.transform.position = new Vector3(
                            marked.m_Script.transform.position.x,
                            marked.m_Script.transform.position.y,
                            marked.m_Script.transform.position.z);
                            
                            marked.m_Script.gameObject.transform.position = new Vector3(
                            m_PreviouslySelectedBlock.m_Script.transform.position.x,
                            m_PreviouslySelectedBlock.m_Script.transform.position.y,
                            m_PreviouslySelectedBlock.m_Script.transform.position.z);

                            // Activate the new swapped object
                            swapped.m_Script.gameObject.SetActive(true);

                            // Activate spark 1/2
                            GAMEGLOBAL.LVL.m_Spark1.gameObject.transform.position = new Vector3(
                            marked.m_Script.transform.position.x,
                            marked.m_Script.transform.position.y,
                            -2f);

                            GAMEGLOBAL.LVL.m_Spark2.gameObject.transform.position = new Vector3(
                            swapped.m_Script.transform.position.x,
                            swapped.m_Script.transform.position.y,
                            -2f);

                            GAMEGLOBAL.LVL.m_Behaviour.moveAndDeactivateSparky(marked.m_Script.transform.position.x, marked.m_Script.transform.position.y, swapped.m_Script.transform.position.x, swapped.m_Script.transform.position.y);

                            // Turn off stuff
                            m_PreviouslySelectedBlock.deselect();
                            marked.deselect();
                            m_PreviouslySelectedBlock = null;
                            return;
                        }

                        int res = GAMEGLOBAL.LVL.CompleteSideBarTransfer(m_PreviouslySelectedBlock.arrPosY, marked.arrPosX, marked.arrPosY);
                        if (res == 1)
                        {
                            m_PreviouslySelectedBlock.deselect();
                            m_PreviouslySelectedBlock = null;
                        }
                        return;
                    }
                }
            }
            else
            {
                hintScript hnt = m_Hit.collider.gameObject.GetComponent<hintScript>();
                if (hnt != null)
                {
                    if (!hnt.m_Cost)
                    {
                        // This is a free hint, just display it
                        GAMEGLOBAL.LVL.setHint(true, hnt.m_Text);
                    }
                    else
                    {
                        // Can we afford to show?
                        if (GAMEGLOBAL.LVL.m_Coins >= 100)
                        {
                            // Use hint.
                            GAMEGLOBAL.LVL.setHint(true, hnt.m_Text);

                            // Hint is now free
                            hnt.m_Cost = false;

                            // This cost 100 coins!
                            GAMEGLOBAL.LVL.m_Coins -= 100;    // UPDATE THIS SO IT PERSIST.....

                            // Update info text
                            string wrdtmp = "";
                            if (allWords.Count > 0)
                                wrdtmp = allWords[allWords.Count - 1];
                            GAMEGLOBAL.LVL.updateInfoText(GAMEGLOBAL.LVL.m_Coins, GAMEGLOBAL.LVL.m_Points, wrdtmp);

                            // Update hints
                            GAMEGLOBAL.LVL.updateHints();
                        }
                        else
                        {
                            // Not enough coins
                            GAMEGLOBAL.LVL.setHint(true, "<b><color=#ffa500ff>How to earn coins:</color></b>\n- Watching Video Ads rewards a lot of coins.\n- Completing a level rewards coins.\n- Including a Gold Block in a sentence will reward coins equal to its length.");
                        }
                    }
                    return;
                }
                else
                {
                    smallButtonScript smb = m_Hit.collider.gameObject.GetComponent<smallButtonScript>();
                    if (smb != null)
                    {
                        if (smb.m_TYPE == smallButtonScript.BUTTONTYPE.BUY)
                            m_LevelStatus = LEVEL_STATUS.BUYBLOCKMENU;
                        return;
                    }
                }
            }

        }
        else if (Input.GetMouseButton(0))
        {
            if (m_PreviouslySelectedBlock != null)
                return;

            if (!Physics.Raycast(GAMEGLOBAL.LVL.m_Camera.ScreenPointToRay(Input.mousePosition), out m_Hit, 100))
                return;

            // We need to hit something (with a collider on it)
            if (!m_Hit.transform)
                return;

            BlockScript bs = m_Hit.collider.gameObject.GetComponent<BlockScript>();

            // Get marked object
            if (bs != null)
            {
                Block marked = bs.m_Block;

                // Not selectable?
                if (!marked.m_Selectable || marked.m_Script.m_Location == BlockScript.BLOCKLOCATION.SIDE)
                    return;

                // Check if this is the first
                if (GAMEGLOBAL.LVL.m_Selected.Count == 0)
                {
                    marked.select();
                    GAMEGLOBAL.LVL.m_Selected.Add(marked);
                    assembleWorld();
                }
                else
                {
                    // if marked is the same object as currently selected, do nothing
                    if (GAMEGLOBAL.LVL.m_Selected[GAMEGLOBAL.LVL.m_Selected.Count - 1].m_ID == marked.m_ID)
                        return;

                    // if marked is previously selected object, deselect current one and remove from list (we took a step back)
                    if (GAMEGLOBAL.LVL.m_Selected.Count > 1 && GAMEGLOBAL.LVL.m_Selected[GAMEGLOBAL.LVL.m_Selected.Count - 2].m_ID == marked.m_ID)
                    {
                        GAMEGLOBAL.LVL.m_Selected[GAMEGLOBAL.LVL.m_Selected.Count - 1].deselect();
                        GAMEGLOBAL.LVL.m_Selected.RemoveAt(GAMEGLOBAL.LVL.m_Selected.Count - 1);
                        assembleWorld();
                        return;
                    }

                    // If marked is already selected, do nothing (to avoid going across already selected letter)
                    if (marked.m_selected)
                        return;

                    // Check wether marked one is within reach. Can only select the nearest 8. (to avoid selecting things too far away)
                    if (Mathf.Abs(GAMEGLOBAL.LVL.m_Selected[GAMEGLOBAL.LVL.m_Selected.Count - 1].arrPosX - marked.arrPosX) > 1 ||
                        Mathf.Abs(GAMEGLOBAL.LVL.m_Selected[GAMEGLOBAL.LVL.m_Selected.Count - 1].arrPosY - marked.arrPosY) > 1)
                        return;

                    // Otherwise select and add to list (OK, new letter selected)
                    marked.select();
                    GAMEGLOBAL.LVL.m_Selected.Add(marked);
                    assembleWorld();
                }
            }

        }
        else if (Input.GetMouseButtonUp(0))
        {
            deselectAll();
            GAMEGLOBAL.LVL.m_Text = "";
        }

    }

    void assembleWorld()  
    {
        string word = "";
        for (int i = 0; i < GAMEGLOBAL.LVL.m_Selected.Count; i++)
        {
            // Assemble word
            word += GAMEGLOBAL.LVL.m_Selected[i].m_LetterChar;

            // Mark that we selected "any" char
            if (GAMEGLOBAL.LVL.m_Selected[i].m_LetterChar == '_')
                GAMEGLOBAL.LVL.m_SelectedAny = true;
        }
        
        // Save
        GAMEGLOBAL.LVL.m_Text = word;
    }

    void deselectAll()
    {
        // Deselect
        bool wordComplete = true;

        if (GAMEGLOBAL.LVL.m_Selected.Count <= 1)
        {
            wordComplete = false;
        }
        else
        {
            m_Attempts++;

            // Check word against database
            if (GAMEGLOBAL.LVL.m_SelectedAny)
            {
                // Check if we have selected "any" word, which has '_' wildcard character(s)
                string anyWord = GAMEGLOBAL.LVL.m_DB.isAnyWord(GAMEGLOBAL.LVL.m_Text);
                if (anyWord.Length > 0)
                {
                    GAMEGLOBAL.LVL.m_Text = anyWord.ToUpper();
                    allWords.Add(GAMEGLOBAL.LVL.m_Text);
                }
                else
                {
                    wordComplete = false;
                }  
            }
            else if (GAMEGLOBAL.LVL.m_DB.isWord(GAMEGLOBAL.LVL.m_Text))
            {
                // Check for word
                allWords.Add(GAMEGLOBAL.LVL.m_Text);
            }
            else
                wordComplete = false;

            // Check objectives
            for(int i = 0; i < m_Objectives.Count; i++)
            {
                if(!m_Objectives[i].m_Complete && m_Objectives[i].m_Type != Objective.TYPE.STAR)
                {
                    // Set word data
                    m_Objectives[i].setData(GAMEGLOBAL.LVL.m_Text, allWords, m_Attempts, wordComplete, m_Objectives);

                    // Check objective
                    if (m_Objectives[i].checkObjective())
                    {
                        m_ObjectivesComplete++;

                        // We made (another) successful database check for another match
                        if (m_Objectives[i].m_Type == Objective.TYPE.FINISH_WORD || m_Objectives[i].m_Type == Objective.TYPE.CATEGORY)
                            wordComplete = true;
                    }
                    else if (m_Objectives[i].m_Type == Objective.TYPE.ATTEMPTS && m_Objectives[i].m_Failed)
                    {
                        GAMEGLOBAL.LVL.m_isEvent = true;
                        levelFailed();
                    }
                }
            }

        }

        int x = 0;
        int y = 0;
        for (int i = 0; i < GAMEGLOBAL.LVL.m_Selected.Count; i++)
        {
            GAMEGLOBAL.LVL.m_Selected[i].deselect();
            x = (int)GAMEGLOBAL.LVL.m_Selected[i].m_Script.gameObject.transform.position.x;
            y = (int)GAMEGLOBAL.LVL.m_Selected[i].m_Script.gameObject.transform.position.y;

            if (wordComplete)
            {
                 GAMEGLOBAL.LVL.destroy(GAMEGLOBAL.LVL.m_Selected[i].arrPosX, GAMEGLOBAL.LVL.m_Selected[i].arrPosY, true, false);
            }     
        }

        if (wordComplete)
        {
            GAMEGLOBAL.LVL.updateInfoText(GAMEGLOBAL.LVL.m_Coins, GAMEGLOBAL.LVL.m_Points, GAMEGLOBAL.LVL.m_Text);

            // Do "Up" text, visually displaying the word found/created
            if (allWords.Count > 0)
            {
                addPoints(allWords[allWords.Count - 1], x, y);
            }

            // Run block logic
            for (int i = 0; i < GAMEGLOBAL.LVL.GAME_WIDTH; i++)
                for (int j = 0; j < GAMEGLOBAL.LVL.GAME_HEIGHT; j++)
                    if (GAMEGLOBAL.LVL.m_Arr[i, j] != null)
                        GAMEGLOBAL.LVL.m_Arr[i, j].logic();

            // Run block logic for sidebar, but only for switcher
            for (int j = 0; j < GAMEGLOBAL.LVL.GAME_HEIGHT; j++)
                if (GAMEGLOBAL.LVL.m_SideArr[0, j] != null && GAMEGLOBAL.LVL.m_SideArr[0, j].m_Type == Block.TYPE.SWITCH)
                    GAMEGLOBAL.LVL.m_SideArr[0, j].logic();

            // Tetris time
            tetrisEvent();
        }

        GAMEGLOBAL.LVL.m_Selected.Clear();
        GAMEGLOBAL.LVL.m_SelectedAny = false;
        GAMEGLOBAL.LVL.m_SelectedGold = false;
    }

    public virtual void tetrisEvent()
    {
        GAMEGLOBAL.LVL.m_isEvent = true;

        // Count how many we should replace
        for (int i = 0; i < GAMEGLOBAL.LVL.GAME_WIDTH; i++)
        {
            m_colObjectCount[i] = 0;
            for (int j = 0; j < GAMEGLOBAL.LVL.GAME_HEIGHT; j++)
            {
                if (GAMEGLOBAL.LVL.m_Arr[i, j] == null)
                    m_colObjectCount[i]++;
                else if (!GAMEGLOBAL.LVL.m_Arr[i, j].m_Gravity)
                {
                    // Gravity disabled on this block, this is now seen as "floor"
                    m_colObjectCount[i] = 0;
                }
            }
        }

        // Add new
        addNew();

        GAMEGLOBAL.LVL.m_Behaviour.fallingEvent();
        Debug.Log("m_Blocks.count = " + GAMEGLOBAL.LVL.m_Boxes.Count);
    }

    // Add new blocks. This is the default non-overridden version
    public virtual void addNew()
    {
        // Add new
        for (int i = 0; i < GAMEGLOBAL.LVL.GAME_WIDTH; i++)
        {
            float yPos = -2f;
            for (int n = 0; n < m_colObjectCount[i]; n++)
            {
                Block b = null;

                // Check if we are dropping normal letter or special
                if (GAMEGLOBAL.LVL.m_Random.Next(0, 100) < droprate)
                {
                    int secondRoll = GAMEGLOBAL.LVL.m_Random.Next(0, 7);

                    switch (secondRoll)
                    {
                        case 0:
                        {
                            if(droprate_ANY > 0)
                            {
                                b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.ANY);
                                droprate_ANY--;
                                break;
                            }
                            else
                            {
                                b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                                b.setRandomCharacter();
                                m_LetterExistance[b.m_LetterValue]++;
                                m_BlockFallCounter++;
                                break;
                            }
                        }

                        case 1:
                        {
                            if (droprate_SWITCH > 0)
                            {
                                b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.SWITCH);
                                droprate_SWITCH--;
                                break;
                            }
                            else
                            {
                                b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                                b.setRandomCharacter();
                                m_LetterExistance[b.m_LetterValue]++;
                                m_BlockFallCounter++;
                                break;
                            }
                        }

                        case 2:
                        {
                            if (droprate_STONE > 0)
                            {
                                b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.STONE);
                                droprate_STONE--;
                                break;
                            }
                            else
                            {
                                b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                                b.setRandomCharacter();
                                m_LetterExistance[b.m_LetterValue]++;
                                m_BlockFallCounter++;
                                break;
                            }
                        }

                        case 3:
                        {
                            if (droprate_BOMB > 0)
                            {
                                b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.BOMB);
                                ((BombBlock)b).setBombTime(GAMEGLOBAL.LVL.m_Random.Next(3, 8));
                                droprate_BOMB--;
                                break;
                            }
                            else
                            {
                                b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                                b.setRandomCharacter();
                                m_LetterExistance[b.m_LetterValue]++;
                                m_BlockFallCounter++;
                                break;
                            }
                        }

                        default:
                            b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                            b.setRandomCharacter();
                            m_LetterExistance[b.m_LetterValue]++;
                            m_BlockFallCounter++;
                            break;
                    }
                }
                else
                {
                    // Every 10 block falling will trigger a "non-random" letter
                    m_BlockFallCounter++;
                    if (m_BlockFallCounter % 5 == 0)
                    {
                        b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                        int charVal = getLowestExistingLetter();
                        b.setCharacter(GAMEGLOBAL.LVL.getCharacter(charVal), charVal);
                        m_LetterExistance[charVal]++;
                    }
                    else
                    {
                        b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                        b.setRandomCharacter();
                        m_LetterExistance[b.m_LetterValue]++;
                    }
                }

                yPos += 1.5f;
                b.m_Script.transform.position = new Vector3(i, yPos, 0);
                b.m_Script.gameObject.SetActive(true);

                Rigidbody r = b.m_Script.GetComponent<Rigidbody>();
                r.isKinematic = false;
            }
        }
    }

    public virtual void defaultRandomStart()
    {
        // Placera objekten i arrayen där det är null.
        for (int i = 0; i < GAMEGLOBAL.LVL.GAME_WIDTH; i++)
            for (int j = 0; j < GAMEGLOBAL.LVL.GAME_HEIGHT; j++)
                if (GAMEGLOBAL.LVL.m_Arr[i, j] == null)
                {
                    GAMEGLOBAL.LVL.m_Arr[i, j] = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                    int charValue = GAMEGLOBAL.LVL.getRandomFrequency();
                    GAMEGLOBAL.LVL.m_Arr[i, j].setCharacter(GAMEGLOBAL.LVL.getCharacter(charValue), charValue);
                    GAMEGLOBAL.LVL.m_Arr[i, j].m_Script.transform.position = new Vector3(i, j - 9, 0);
                    GAMEGLOBAL.LVL.m_Arr[i, j].m_Script.gameObject.SetActive(true);
                }
        GAMEGLOBAL.LVL.m_isEvent = false;
    }

    public Block getStaticBlock(int blocktype, int h, int w)
    {
        // H: 0 = BOTTOM, 4 = TOP
        // W: 0 = LEFT, 7 = RIGHT
        Block t = GAMEGLOBAL.LVL.getBlock(blocktype);
        t.m_Script.transform.position = new Vector3(w, -9 + h, 0);
        t.arrPosX = w;
        t.arrPosY = h;
        t.m_Script.gameObject.SetActive(true);
        t.setGravity(true);
        return t;
    }

    public void checkAfterFallObjectives()
    {
        // Check star objectives
        for (int i = 0; i < m_Objectives.Count; i++)
        {
            if (!m_Objectives[i].m_Complete && m_Objectives[i].m_Type == Objective.TYPE.STAR || m_Objectives[i].m_Type == Objective.TYPE.TIMED)
            {
                m_Objectives[i].setData("", null, 0, false, m_Objectives);
                if(m_Objectives[i].checkObjective())
                    m_ObjectivesComplete++;
            }
        }

        // Check if level failed first
        for (int i = 0; i < m_Objectives.Count; i++)
            if(m_Objectives[i].m_Failed)
            {
                levelFailed();
                break;
            }

        // Level completed?
        checkLevelComplete();
    }

    public void checkLevelComplete()
    {
        if (m_LevelStatus != LEVEL_STATUS.FAILED && m_ObjectivesComplete == m_Objectives.Count)
            levelComplete();
    }

    public void levelComplete()
    {
        GAMEGLOBAL.LVL.m_isEvent = true;
        GAMEGLOBAL.LVL.m_Behaviour.setLevelStatusAfterWaitingFor(LEVEL_STATUS.SUCCESS, 1);

        GAMEGLOBAL.LVL.m_Coins += 25;
        GAMEGLOBAL.LVL.SavePlayerData(GAMEGLOBAL.LVL.m_Coins, GAMEGLOBAL.LVL.m_currentLevel.m_Level + 1);
    }

    public void levelFailed()
    {
        GAMEGLOBAL.LVL.m_isEvent = true;
        GAMEGLOBAL.LVL.m_Behaviour.setLevelStatusAfterWaitingFor(LEVEL_STATUS.FAILED, 1);
    }

    public void start_addStarObjectiveAndBlock(bool good)
    {
        StarBlock sb = (StarBlock)GAMEGLOBAL.LVL.getBlock(Block.TYPE.STAR);
        sb.setGoodBad(good);
        sb.m_Script.gameObject.SetActive(true);
        Objective_Stars objStar = new Objective_Stars(sb);
        m_Objectives.Add(objStar);
        m_StartList.Add(sb);
    }

    public virtual void firstTetrisEvent(List<Block> staticObjects)
    {
        Block[,] world = new Block[GAMEGLOBAL.LVL.GAME_WIDTH, GAMEGLOBAL.LVL.GAME_HEIGHT];

        if (staticObjects != null)
        {
            Block placeholder = new Block(0);
            for(int i = 0; i < staticObjects.Count; i++)
            {
                world[staticObjects[i].arrPosX, staticObjects[i].arrPosY] = staticObjects[i];

                // Mark that everything below is taken by setting it to default-block baseclass.
                for (int n = staticObjects[i].arrPosY - 1; n >= 0; n--)
                    world[staticObjects[i].arrPosX, n] = placeholder;
            }
        }

        // Loop start-list and add blocks
        for(int i = 0; i < m_StartList.Count; i++)
        {
            int max = 0;
            while(max < 10)
            {
                int rx = GAMEGLOBAL.LVL.m_Random.Next(0, GAMEGLOBAL.LVL.GAME_WIDTH);
                int ry = GAMEGLOBAL.LVL.m_Random.Next(1, GAMEGLOBAL.LVL.GAME_HEIGHT);

                if (world[rx, ry] == null)
                {
                    if(m_StartList[i].m_Type == Block.TYPE.STAR)
                    {
                        bool placementOk = true;

                        // Check up
                        for (int j = ry + 1; j < GAMEGLOBAL.LVL.GAME_HEIGHT; j++)
                        {
                            // Other star blocks not allowed above
                            if (world[rx, j] != null && world[rx, j].m_Type == Block.TYPE.STAR)
                            {
                                placementOk = false;
                                break;
                            }
                        }
                            
                        // Check down
                        if(placementOk)
                        {
                            for (int j = ry - 1; j > 0 ; j--)
                            {
                                // Other stars, stone and blocks with gravity turned off not allowed below
                                if (world[rx, j] != null && (world[rx, j].m_Type == Block.TYPE.STAR || world[rx, j].m_Type == Block.TYPE.STONE))
                                {
                                    placementOk = false;
                                    break;
                                }
                                else if(world[rx, j] != null && !world[rx, j].m_Gravity)
                                {
                                    placementOk = false;
                                    break;
                                }
                            }
                        }

                        // Placement is Ok
                        if (placementOk)
                        {
                            world[rx, ry] = m_StartList[i];
                            break;
                        }
                    }
                    else if(m_StartList[i].m_Type == Block.TYPE.STONE)
                    {
                        bool placementOk = true;

                        // Check up
                        for (int j = ry; j < GAMEGLOBAL.LVL.GAME_HEIGHT; j++)
                        {
                            // Other star blocks not allowed above
                            if (world[rx, j] != null && world[rx, j].m_Type == Block.TYPE.STAR)
                            {
                                placementOk = false;
                                break;
                            }
                        }

                        // Placement is Ok
                        if (placementOk)
                        {
                            world[rx, ry] = m_StartList[i];
                            break;
                        }
                    }
                    else
                    {
                        world[rx, ry] = m_StartList[i];
                        break;
                    }
                }
                max++;
            }
        }


        for (int i = 0; i < GAMEGLOBAL.LVL.GAME_WIDTH; i++)
        {
            float yPos = -2f;
            for (int j = 0; j < GAMEGLOBAL.LVL.GAME_HEIGHT; j++)
            {
                if(world[i, j] == null)
                {
                    // Get a letter block
                    Block o = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                    int charValue = GAMEGLOBAL.LVL.getRandomFrequency();
                    o.setCharacter(GAMEGLOBAL.LVL.getCharacter(charValue), charValue);
                    m_LetterExistance[charValue]++;


                    // Set new position for falling
                    yPos += 1.5f;
                    o.m_Script.transform.position = new Vector3(i, yPos, 0);
                    o.m_Script.gameObject.SetActive(true);

                    // Set gravity
                    Rigidbody r = o.m_Script.GetComponent<Rigidbody>();
                    r.isKinematic = false;
                }
                else
                {
                    if (world[i, j].m_Type == -1)
                        continue;

                    if(world[i, j].m_Gravity)
                    {
                        yPos += 1.5f;
                        world[i, j].m_Script.transform.position = new Vector3(i, yPos, 0);
                        world[i, j].m_Script.gameObject.SetActive(true);

                        // Set gravity
                        Rigidbody r = world[i, j].m_Script.GetComponent<Rigidbody>();
                        r.isKinematic = false;
                    }
                }

            }
        }

        GAMEGLOBAL.LVL.m_Behaviour.fallingEvent();
    }

    private void addPoints(string word, int x, int y)
    {
        int points = 0;
        switch(word.Length)
        {
            case 1: points++; break;
            case 2: points++; break;
            case 3: points = 3; break;
            case 4: points = 8; break;
            case 5: points = 20; break;
            default: points = word.Length * 2 * 5; break;
        }

        GAMEGLOBAL.LVL.m_Behaviour.doUPText(x, y, points, word);

        m_currentPoints += points;
        checkSideBlock(points);
    }

    private void checkSideBlock(int points)
    {
        GAMEGLOBAL.LVL.m_Points += points;
        GAMEGLOBAL.LVL.updateInfoText(GAMEGLOBAL.LVL.m_Coins, GAMEGLOBAL.LVL.m_Points, GAMEGLOBAL.LVL.m_Text);

        if (m_currentPoints >= GAMEGLOBAL.LVL.m_PointsLimit)
        {
            if(GAMEGLOBAL.LVL.getFreeSideBarSpace() > 0)
            {
                // Remove points
                m_currentPoints -= GAMEGLOBAL.LVL.m_PointsLimit;

                addNewToSideBar(-1);
            }
        }
    }

    public void addNewToSideBar(int specific)
    {
        Block b = null;
        if(specific == -1)
        {
            // Get random
            int type = GAMEGLOBAL.LVL.m_Random.Next(0, 5);

            switch(type)
            {
                case 0:
                {
                    b = getRandomLetterBlock();
                    break;
                }
                case 1:
                {
                    if(m_Level >= 15)
                    {
                        b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.BOMB);
                        ((BombBlock)b).setBombTime(10);
                    }
                    else
                    {
                        b = getRandomLetterBlock();
                    }
                    break;
                }
                case 2:
                {
                    if (m_Level >= 5)
                    {
                        b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.SWITCH);
                    }
                    else
                    {
                        b = getRandomLetterBlock();
                    }
                    break;
                }
                case 3:
                {
                    if (m_Level >= 10)
                    {
                        b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.ANY);
                    }
                    else
                    {
                        b = getRandomLetterBlock();
                    }
                    break;
                }
                case 4:
                {
                    if (m_Level >= 20)
                    {
                        b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.SWAP);
                        b.init();
                    }
                    else
                    {
                        b = getRandomLetterBlock();
                    }
                    break;
                }
                default:
                {
                    b = getRandomLetterBlock();
                    break;
                }
            }
        }
        else
        {
            b = GAMEGLOBAL.LVL.getBlock(specific);
            b.init();
        }

        // Add to world
        b.m_Script.transform.position = new Vector3(GAMEGLOBAL.LVL.GAME_WIDTH + 0.5f, -2f, 0);
        b.m_Script.gameObject.SetActive(true);
        Rigidbody r = b.m_Script.GetComponent<Rigidbody>();
        r.isKinematic = false;
    }

    private Block getRandomLetterBlock()
    {
        Block b = GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
        int charValue = GAMEGLOBAL.LVL.getRandomFrequency();
        b.setCharacter(GAMEGLOBAL.LVL.getCharacter(charValue), charValue);
        return b;
    }

    private int getLowestExistingLetter()
    {
        int res = 0;
        int lowest = 1000;

        for (int i = 0; i < 26; i++)
            if (m_LetterExistance[i] < lowest)
            {
                lowest = m_LetterExistance[i];
                res = i;
            }

        return res;
    }

}


// BUGGAR:
//

// Skapa ikon för Buy Blocks, men ha den utgråad och gör den uppdateringen senare/sist.
// Databasen, samt övriga manuella tabeller med kategorier.
// Sparande av data, nån annanstans än databasen.


    // 1. Kolla vad/hur för att spara data "persistance" för android.
    // 2. Gör så detta fungerar.
    // 3. Manuellt snygga till/rätta till kategorierna.
    // 4. Börja skapa de första levlarna.
    // 
    // När jag har några levlar, prova på mobilen.
    // Skapa "new game" knapp som bara resettar allt.



    // Vid start:
    // Läs från sparningsfilen. (Om den inte existerar, skapar vi den).
    // Hämta:
    // Progress (nuvarande level)
    // Coins

    // Varje gång coins blir earned (efter level, efter video) skall det direkt sparas.
    // Varje gång coins används likaså.






// Borde finnas någon liten knapp där man kan sätta "New game"
// Ska säga "Are you sure? Starting a new game will reset all saved progress and all coins will be lost." OK/Cancel.


// NAMN:
// Blockable
// 

// Testa med telefonen - ta reda på device-id så jag kan testa där.