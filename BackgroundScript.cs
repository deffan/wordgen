﻿using UnityEngine;
using System.Collections;

public class BackgroundScript : MonoBehaviour
{
    public class BKColor
    {
        public const int BLUE = 0;
        public const int GREEN = 1;
        public const int ORANGE = 2;
        public const int PURPLE = 3;
        public const int BLACK = 4;
    }

    private Rigidbody m_Rbody;
    private Mesh m_Mesh;
    private int m_sizeX;
    private int m_sizeY;

    public void init()
    {
        // Mesh
        m_Mesh = gameObject.GetComponent<MeshFilter>().mesh;

        // Size etc
        m_sizeX = GAMEGLOBAL.LVL.m_Random.Next(10, 17);
        m_sizeY = GAMEGLOBAL.LVL.m_Random.Next(10, 17);
        gameObject.transform.localScale = new Vector3(m_sizeX, m_sizeY, 1f);

        // Ridgidbody
        m_Rbody = gameObject.AddComponent<Rigidbody>();
        m_Rbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationZ;
        m_Rbody.useGravity = false;

        // Random rotation
        m_Rbody.AddTorque(new Vector3(
            GAMEGLOBAL.LVL.m_Random.Next(0, 2), 
            GAMEGLOBAL.LVL.m_Random.Next(0, 2), 
            GAMEGLOBAL.LVL.m_Random.Next(0, 2)));
    }

    public void setColor(int color)
    {
        switch(color)
        {
            case BKColor.BLUE: setUVS(GAMEGLOBAL.LVL.m_Random.Next(17, 21), 6); break;
            case BKColor.GREEN: setUVS(GAMEGLOBAL.LVL.m_Random.Next(17, 21), 5); break;
            case BKColor.ORANGE: setUVS(GAMEGLOBAL.LVL.m_Random.Next(21, 26), 5); break;
            case BKColor.PURPLE: setUVS(GAMEGLOBAL.LVL.m_Random.Next(21, 26), 6); break;
            default: break;
        }
    }

    private void setUVS(int x, int y)
    {
        // The UVs are apparently upsidedown in the Y-direction, so to get a zero-based coordinate system we do this:
        int reverse = (int)(2048f / 64f) - 1;
        y = reverse - y;

        Vector2[] frontUVS = m_Mesh.uv;

        frontUVS[0].x = (x * GAMEGLOBAL.LVL.m_UVOffset);
        frontUVS[0].y = (y * GAMEGLOBAL.LVL.m_UVOffset);

        frontUVS[1].x = GAMEGLOBAL.LVL.m_UVOffset + (x * GAMEGLOBAL.LVL.m_UVOffset);
        frontUVS[1].y = (y * GAMEGLOBAL.LVL.m_UVOffset);

        frontUVS[2].x = GAMEGLOBAL.LVL.m_UVOffset + (x * GAMEGLOBAL.LVL.m_UVOffset);
        frontUVS[2].y = GAMEGLOBAL.LVL.m_UVOffset + (y * GAMEGLOBAL.LVL.m_UVOffset);

        frontUVS[3].x = (x * GAMEGLOBAL.LVL.m_UVOffset);
        frontUVS[3].y = GAMEGLOBAL.LVL.m_UVOffset + (y * GAMEGLOBAL.LVL.m_UVOffset);

        m_Mesh.uv = frontUVS;
    }

    void Update ()
    {
        // Istället för new, kan jag ha alla dessa Vector3 färdiga som medlemmar...

        /*
        if (gameObject.transform.position.x + m_sizeX < -8)
            m_Rbody.AddForce(new Vector3(5, 0, 0));
        if (gameObject.transform.position.x - m_sizeX > 8)
            m_Rbody.AddForce(new Vector3(-5, 0, 0));
        if (gameObject.transform.position.y + m_sizeY < -6)
            m_Rbody.AddForce(new Vector3(0, 5, 0));
        if (gameObject.transform.position.y - m_sizeY > 5)
            m_Rbody.AddForce(new Vector3(0, -5, 0));
        */

        // Kanske räcker med att bara ha dem positionerade slumpvis vid start sen stå stilla och rotera?
        // Testa med 50% transparens

        // x +/- 8 är max
        // y: +5 och -6

        // Undra om det går sätta så att man applicerar en random force?
        // åt motsatt håll eller nåt.
    }
}
