﻿using UnityEngine;
using System.Collections;

public class fadeScript : MonoBehaviour
{
    public Renderer m_Renderer;
    public Color m_Color;

    public void start()
    {
        gameObject.SetActive(true);
        StartCoroutine(fadeOut());
    }

    private IEnumerator fadeOut()
    {
        while (m_Color.a >= 0)
        {
            m_Color.a -= 0.01f;
           // m_Pos.z += 0.1f;
          //  transform.position = m_Pos;
            m_Renderer.material.color = m_Color;
            yield return null;
        }
        gameObject.SetActive(false);
        m_Color.a = 1;
    }

}
