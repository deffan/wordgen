﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour
{
    public class TYPE
    {
        public const int PLAY = 0;
        public const int NEXT = 2;
        public const int RETRY = 3;
        public const int EXIT = 4;
        public const int RESUME = 5;
        public const int DAILYQUEST = 6;
        public const int WATCHVIDEO = 7;
        public const int BUY = 8;
    }

    public Mesh m_Mesh;
    public int m_ButtonType;
    private int m_xCoord;
    public bool m_Active;

	void Start ()
    {
	
	}
    
    public void init(Mesh m, int type)
    {
        m_Active = true;

        if (m != null)
            m_Mesh = m;
        m_ButtonType = type;

        // set uvs
        switch(m_ButtonType)
        {
            case TYPE.PLAY: m_xCoord = 0; break;
            case TYPE.NEXT: m_xCoord = 684; break;
            case TYPE.RETRY: m_xCoord = 912; break;
            case TYPE.RESUME: m_xCoord = 456; break;
            case TYPE.EXIT: m_xCoord = 228; break;
            case TYPE.DAILYQUEST: m_xCoord = 1140; break;
            case TYPE.WATCHVIDEO: m_xCoord = 1368; break;
            case TYPE.BUY: m_xCoord = 1596; break;
        }
        GAMEGLOBAL.LVL.setUVs(m_Mesh, 225, 50, 2048, m_xCoord, 650);
        transform.gameObject.SetActive(true);
    }
	
    public void click()
    {
        GAMEGLOBAL.LVL.setUVs(m_Mesh, 225, 50, 2048, m_xCoord, 703);
    }

    public void unclick()
    {
        GAMEGLOBAL.LVL.setUVs(m_Mesh, 225, 50, 2048, m_xCoord, 650);
    }
}
