﻿using UnityEngine;
using System.Collections;

public class level_RAND : Level
{
    public level_RAND(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(GAMEGLOBAL.LVL.m_Random.Next(0, 8));

        int numOfObjectives = 3;


        // Palindrome ska inte gå ha flera gånger.
        // Find with with 'A' and 'A' ska inte gå heller såklart...
        // Find a word starting/ending är alldeles för lätta. Vore bättre att merga och göra så att man måste ha både start/slut.
        // Attempts ska inte räknas ner om man inte hittar ett ord.

        // Failed level borde starta om exakt samma level.
        // Pause -> Exit -> Play borde också starta om exakt samma level.
        // Pause -> Retry kanske ska finnas också?

        // Leta bättre databas, med mer "logiska" eller "enkla" ord som man kanske har hört talas om i varje fall...

        // "ANY"-Block känns rätt meningslös. Nån form av swap hade varit 100ggr bättre.
        // Eller ett icke-random switcher-block som går från A->B->C->... 
        // Med ikonen -> under blocket så man ser skillnaden.

        // Igen kanske man bör fundera över att droppa saker man potentiellt behöver för ett ord?
        // Om man behöver "P" men det aldrig aldrig över 30 attempts droppar är det bara frustrerande...

        // SWAP Block.
        // Placeras ut på spelplanen normalt och via sidebar.
        // Har statisk bokstav placerad på sig, men specialbakrund med <- -> över/under osv.
        // Eller runtom blocket med pilar
        // KAn ingå i vanligt ord som vanligt... MEN
        // Om man clickar på det som vanligt, (OnMouseUp) så aktiveras det med "POOF".
        // Nästa click på objekt genomför en switch, och switchobjektet förvandlas till vanlig bokstav med samma värde.

        // Botten på ramen runt spelplanen kanske ska tas bort och där ska en liten text finnas:
        // Points to next free: xxxx        Last word was: aaaaaaa
        // 
        // Då kan jag ändra UPWord till poäng istället för ord.




        // Only one per level allowed
        bool category = true;
        bool palindrome = true;
        bool twice = true;

        for (int i = 0; i < numOfObjectives; i++)
        {
            int objective = GAMEGLOBAL.LVL.m_Random.Next(0, 11);
            switch(objective)
            {
                case 0: m_Objectives.Add(new Objective_Find_This(GAMEGLOBAL.LVL.m_DB.getRandomWord(GAMEGLOBAL.LVL.m_Random.Next(3, 7)))); break;
                case 1: m_Objectives.Add(new Objective_Find_Word_StartingEnding(GAMEGLOBAL.LVL.m_Random.Next(3, 6), true, GAMEGLOBAL.LVL.getCharacter(GAMEGLOBAL.LVL.getRandomFrequency()))); break;
                case 2: m_Objectives.Add(new Objective_Find_Word_StartingEnding(GAMEGLOBAL.LVL.m_Random.Next(3, 6), false, GAMEGLOBAL.LVL.getCharacter(GAMEGLOBAL.LVL.getRandomFrequency()))); break;
                case 3: m_Objectives.Add(new Objective_Finish_The_Word(GAMEGLOBAL.LVL.m_DB.getRandomWord(GAMEGLOBAL.LVL.m_Random.Next(4, 9)))); break;
                case 4:
                    {
                        int len = GAMEGLOBAL.LVL.m_Random.Next(3, 6);
                        m_Objectives.Add(new Objective_Attempts(GAMEGLOBAL.LVL.m_DB.getRandomWord(len), len*8));
                    }
                    break;
                case 5: m_Objectives.Add(new Objective_Find_X_Words(GAMEGLOBAL.LVL.m_Random.Next(3, 5), GAMEGLOBAL.LVL.m_Random.Next(2, 6), GAMEGLOBAL.LVL.m_Random.Next(0, 2) == 0)); break;
                case 6:
                    {
                        int num = GAMEGLOBAL.LVL.m_Random.Next(3, 6);
                        int size = GAMEGLOBAL.LVL.m_Random.Next(0, 4);

                        if (size == 0)
                            m_Objectives.Add(new Objective_Find_X_Letters(true, false, false, num));
                        else if (size == 1)
                            m_Objectives.Add(new Objective_Find_X_Letters(false, true, false, num));
                        else
                            m_Objectives.Add(new Objective_Find_X_Letters(false, false, true, num));
                    }
                    break;
                case 7:
                    if (category)
                    {
                      //  m_Objectives.Add(new Objective_Category(GAMEGLOBAL.LVL.m_Random.Next(0, 9)));
                        category = false;
                    }
                    else
                        numOfObjectives++;
                    break;
                case 8: m_Objectives.Add(new Objective_Containing(GAMEGLOBAL.LVL.m_DB.getRandomWord(GAMEGLOBAL.LVL.m_Random.Next(10, 12)), GAMEGLOBAL.LVL.m_Random.Next(2, 4))); break;
                case 9:
                    if(palindrome)
                    {
                        m_Objectives.Add(new Objective_Find_Twice());
                        palindrome = false;
                    }
                    else
                        numOfObjectives++;
                    break;
                case 10:
                    if (twice)
                    {
                        m_Objectives.Add(new Objective_Palindrome(GAMEGLOBAL.LVL.m_Random.Next(3, 5)));
                        twice = false;
                    }
                    else
                        numOfObjectives++;
                    break;
                default: break;
            }
        }

        if(m_Objectives.Count > 4)
        {
            Debug.Log("More than 4 objectives were created...");
        }


        // Ha en "crazyness" faktor, som gör att man kanske har 5-10% chans att få en level fylld med bomber osv

        // Random drops
        setDrop(Block.TYPE.STONE, GAMEGLOBAL.LVL.m_Random.Next(0, 3));
        setDrop(Block.TYPE.SWITCH, GAMEGLOBAL.LVL.m_Random.Next(0, 3));
        setDrop(Block.TYPE.BOMB, GAMEGLOBAL.LVL.m_Random.Next(2, 5));
        setDrop(Block.TYPE.ANY, GAMEGLOBAL.LVL.m_Random.Next(0, 3));

    }

}


/*







    Färdiga levlar... som kan placeras på lämplig nivå...

-----------------------------------------------------------------------------------------------------

List<string> m_Ans = new List<string>();
m_Ans.Add("MONDAY");
m_Ans.Add("TUESDAY");
m_Ans.Add("WEDNESDAY");
m_Ans.Add("THURSDAY");
m_Ans.Add("FRIDAY");
m_Ans.Add("SATURDAY");
m_Ans.Add("SUNDAY");
m_Objectives.Add(new Objective_Manual("Name a day", m_Ans, m_Ans, true));

List<string> m_Ans2 = new List<string>();
m_Ans2.Add("seven");
m_Objectives.Add(new Objective_Manual("There are _____ days in a week", m_Ans2, m_Ans2, false));

List<string> m_Ans3 = new List<string>();
m_Ans3.Add("thor");
m_Objectives.Add(new Objective_Manual("Thursday, Norse God of Thunder ____", m_Ans3, m_Ans3, true));

-----------------------------------------------------------------------------------------------------

List<string> m_Ans = new List<string>();
m_Ans.Add("WATER");
m_Objectives.Add(new Objective_Manual("Fish swims in _____", m_Ans, m_Ans, false));
m_Objectives.Add(new Objective_Find_X_Words(5, 4, true));
m_Objectives.Add(new Objective_Find_This("CRAB"));

----------------------------------------------------------------------------------------------------



*/
