﻿using UnityEngine;
using System.Collections;

public class level_10 : Level
{
    public level_10(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(1);

        m_Objectives.Add(new Objective_Finish_The_Word("SNOW"));
        m_Objectives.Add(new Objective_Finish_The_Word("BASH"));
        m_Objectives.Add(new Objective_Finish_The_Word("SLOT"));
  
    }

}
