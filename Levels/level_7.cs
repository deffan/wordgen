﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class level_7 : Level
{
    public level_7(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(6);

        List<string> m_Ans = new List<string>();
        m_Ans.Add("SKY");
        m_Objectives.Add(new Objective_Manual("Birds fly in the ___", m_Ans, m_Ans, true));

        List<string> m_Ans1 = new List<string>();
        m_Ans1.Add("SIX");
        m_Objectives.Add(new Objective_Manual("4 + 2 : ?", m_Ans1, null, false));

        m_Objectives.Add(new Objective_Find_Twice());

        setDrop(Block.TYPE.SWITCH, 3);
    }

}
