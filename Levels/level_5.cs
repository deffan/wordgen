﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class level_5 : Level
{
    public level_5(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(6);

        m_Objectives.Add(new Objective_Attempts("DOG", 20));
        m_Objectives.Add(new Objective_Finish_The_Word("ROOF"));
        
        List<string> m_Ans = new List<string>();
        m_Ans.Add("ONE");
        m_Ans.Add("TWO");
        m_Ans.Add("THREE");
        m_Ans.Add("FOUR");
        m_Ans.Add("FIVE");
        m_Ans.Add("SIX");
        m_Ans.Add("SEVEN");
        m_Ans.Add("EIGHT");
        m_Ans.Add("NINE");
        m_Ans.Add("TEN");
        m_Objectives.Add(new Objective_Manual("Number between <c1>1</c> and <c1>10</c>", m_Ans, m_Ans, true));

        setDrop(Block.TYPE.SWITCH, 3);
    }

}
