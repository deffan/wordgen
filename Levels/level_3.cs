﻿using UnityEngine;
using System.Collections;

public class level_3 : Level
{
    public level_3(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(1);

        m_Objectives.Add(new Objective_Find_This("JOB"));
        m_Objectives.Add(new Objective_Finish_The_Word("LEAF"));
        m_Objectives.Add(new Objective_Palindrome(3));
    }

}
