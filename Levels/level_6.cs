﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class level_6 : Level
{
    public level_6(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(7);

        m_Objectives.Add(new Objective_Find_Word_StartingEnding(4, true, 'T'));
        m_Objectives.Add(new Objective_Find_X_Words(5, 4, true));
        m_Objectives.Add(new Objective_Find_This("CRAB"));

        setDrop(Block.TYPE.SWITCH, 3);
    }

}
