﻿using UnityEngine;
using System.Collections;

public class level_8 : Level
{
    public level_8(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(6);

        m_Objectives.Add(new Objective_Find_X_Words(3, 4, true));
        m_Objectives.Add(new Objective_Finish_The_Word("STONE"));
        m_Objectives.Add(new Objective_Finish_The_Word("BOMB"));

        setDrop(Block.TYPE.STONE, 3);
        setDrop(Block.TYPE.BOMB, 3);
    }

}
