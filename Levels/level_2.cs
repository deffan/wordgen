﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class level_2 : Level
{
    public level_2(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        GAMEGLOBAL.LVL.setLetterBlockColor(4);

       
        m_Objectives.Add(new Objective_Finish_The_Word("RAT"));
        m_Objectives.Add(new Objective_Find_Twice());
        m_Objectives.Add(new Objective_Find_Word_StartingEnding(3, true, 'R'));

    }

}
