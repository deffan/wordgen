﻿using UnityEngine;
using System.Collections;

public class level_4 : Level
{
    public level_4(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(7);

        m_Objectives.Add(new Objective_Find_This("TAN"));
        m_Objectives.Add(new Objective_Finish_The_Word("TREE"));
        m_Objectives.Add(new Objective_Attempts("BALL", 50));

    }

}
