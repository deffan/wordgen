﻿using UnityEngine;
using System.Collections;

public class level_9 : Level
{
    public level_9(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(14);

        m_Objectives.Add(new Objective_Finish_The_Word("DARLING"));
        m_Objectives.Add(new Objective_Find_This("DONE"));
        m_Objectives.Add(new Objective_Timed(180));
    }

}
