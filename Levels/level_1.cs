﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class level_1 : Level
{
    public level_1(int lvl) : base(lvl)
    {

    }

    public override void Begin_1()
    {
        // Set color and style for blocks and background
        GAMEGLOBAL.LVL.setLetterBlockColor(0);

        m_Objectives.Add(new Objective_Find_This("CAT"));
        m_Objectives.Add(new Objective_Find_X_Words(5, 0, false));


        /*
        start_addStarObjectiveAndBlock(true);
        start_addStarObjectiveAndBlock(false);
        start_addStarObjectiveAndBlock(false);

        m_Objectives.Add(new Objective_Timed(55));

        // Add other things to startlist
        m_StartList.Add(GAMEGLOBAL.LVL.getBlock(Block.TYPE.ANY));
        m_StartList.Add(GAMEGLOBAL.LVL.getBlock(Block.TYPE.SWITCH));

        // Add a static stone object with gravity turned off
        Block s1 = getStaticBlock(Block.TYPE.STONE, 3, 4);
        s1.setGravity(false);
        Block s2 = getStaticBlock(Block.TYPE.STONE, 1, 3);
        s2.setGravity(false);

        // Construct a list with static objects and call firstTetrisEvent
        List<Block> statics = new List<Block>();
        statics.Add(s1);
        statics.Add(s2);
        firstTetrisEvent(statics);
        */
    }

}
