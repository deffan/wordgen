﻿using UnityEngine;
using System.Collections;

public class Objective_IncreasingLength : Objective
{
    private int m_Words;
    private int m_LastLength;
    private int m_Count;

    public Objective_IncreasingLength(int words)
    {
        m_Type = TYPE.INCREASING_LENGTH;
        m_Words = words;
        m_Count = 0;
        m_LastLength = 0;
        m_Description = "Find {0} words with <c1>increasing length each time</c>";
        m_ObjectiveScript.setText(string.Format(m_Description, m_Words));
    }

    public override bool checkObjective()
    {
        if (!wordFound)
            return false;

        if(word.Length > m_LastLength)
        {
            m_LastLength = word.Length;
            m_Count++;
            if(m_Count == m_Words)
            {
                m_Complete = true;
                m_ObjectiveScript.setGreen();
            }
        }
        else
        {
            m_Failed = true;
            m_ObjectiveScript.setRed();
        }

        return m_Complete;
    }
}
