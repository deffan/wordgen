﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Objective_Category : Objective
{
    public class CATEGORY
    {
        public const int FRUITS = 0;
        public const int CAPITALS = 1;
        public const int ANIMALS = 2;
        public const int COLORS = 3;
        public const int COUNTRIES = 4;
        public const int ELEMENTS = 5;
        public const int SOLARSYSTEM = 6;
        public const int VEGETABLES = 7;
        public const int SPORT = 8;
    }
        public int m_Category;

    public Objective_Category(int cat)
    {
        m_Category = cat;
        m_Type = TYPE.CATEGORY;

        switch (m_Category)
        {
            case CATEGORY.FRUITS: m_Description = "Fruit"; setHint("fruits"); break;
            case CATEGORY.CAPITALS: m_Description = "Capital city"; setHint("capitals"); break;
            case CATEGORY.ANIMALS: m_Description = "Animal"; setHint("animals"); break;
            case CATEGORY.COLORS: m_Description = "Color"; setHint("colors"); break;
            case CATEGORY.COUNTRIES: m_Description = "Country"; setHint("countries"); break;
            case CATEGORY.ELEMENTS: m_Description = "Periodic table element"; setHint("elements"); break;
            case CATEGORY.SOLARSYSTEM: m_Description = "Object in the solarsystem"; setHint("solarsystem"); break;
            case CATEGORY.VEGETABLES: m_Description = "Vegetable"; setHint("VEGETABLES"); break;
            case CATEGORY.SPORT: m_Description = "Sport"; setHint("sports"); break;
        }
        
        m_ObjectiveScript.setText(m_Description);
    }

    private void setHint(string from)
    {
        // Add hint
        List<string> matchingWords = GAMEGLOBAL.LVL.m_DB.getCategoryWordsMatching(from);
        if (matchingWords.Count > 0)
        {
            string hinttext = "<b><color=#ffa500ff>Examples of matching words:</color></b>\n";
            int currentLineCount = 0;
            for (int i = 0; i < matchingWords.Count; i++)
            {
                if (currentLineCount + matchingWords[i].Length > 70)
                {
                    hinttext += "\n";
                    currentLineCount = 0;
                }
                else
                    currentLineCount += matchingWords[i].Length + 2;

                hinttext += matchingWords[i].ToLower();
                if (i < matchingWords.Count - 1)
                    hinttext += ", ";
            }
            m_ObjectiveScript.setHint(hinttext, true);
        }
    }

    public override bool checkObjective()
    {
        bool result = false;
        switch (m_Category)
        {
            case CATEGORY.FRUITS: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("fruits", word); break;
            case CATEGORY.CAPITALS: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("capitals", word); break;
            case CATEGORY.ANIMALS: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("animals", word); break;
            case CATEGORY.COLORS: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("colors", word); break;
            case CATEGORY.COUNTRIES: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("countries", word); break;
            case CATEGORY.ELEMENTS: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("elements", word); break;
            case CATEGORY.SOLARSYSTEM: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("solarsystem", word); break;
            case CATEGORY.VEGETABLES: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("VEGETABLES", word); break;
            case CATEGORY.SPORT: result = GAMEGLOBAL.LVL.m_DB.findWhatFrom("sports", word); break;
        }

        if(result)
        {
            m_Complete = true;
            m_ObjectiveScript.addText(" (" + word + ")");
            m_ObjectiveScript.setGreen();
        }

        return m_Complete;
    }
}
