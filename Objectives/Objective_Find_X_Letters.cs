﻿using System.Collections.Generic;

public class Objective_Find_X_Letters : Objective
{
    private bool m_Bigger;
    private bool m_Smaller;
    private bool m_Exactly;
    private int m_Count;

    public Objective_Find_X_Letters(bool bigger, bool smaller, bool exactly, int numOfLetters)
    {
        m_Type = TYPE.FIND_X_LETTERS;
        m_Description = "A word with";
        m_Bigger = bigger;
        m_Smaller = smaller;
        m_Exactly = exactly;
        m_Count = numOfLetters;

        if (bigger)
            m_Description += "<c1> more than " + numOfLetters + "</c> letters";
        else if (smaller)
            m_Description += "<c1> less than " + numOfLetters + "</c> letters";
        else if (exactly)
            m_Description += "<c1> exactly " + numOfLetters + "</c> letters";

        m_ObjectiveScript.setText(m_Description);
    }

    public override bool checkObjective()
    {
        if (!wordFound)
            return false;

        bool success = false;
        if (m_Bigger && word.Length > m_Count)
        {
            success = true;
        }
        else if (m_Smaller && word.Length < m_Count)
        {
            success = true;
        }
        else if (m_Exactly && word.Length == m_Count)
        {
            success = true;
        }

        if (success)
        {
            m_Complete = true;
            m_ObjectiveScript.addText(" (" + word + ")");
            m_ObjectiveScript.setGreen();
        }

        return m_Complete;
    }
}
