﻿using System.Collections.Generic;

public class Objective_Stars : Objective
{
    public StarBlock m_StarBlock;

    public Objective_Stars(StarBlock sb)
    {
        m_Type = TYPE.STAR;
        m_StarBlock = sb;

        if (m_StarBlock.m_Good)
            m_Description = "Make the <color=#ff9900>star</color> reach the bottom";
        else
            m_Description = "Do not let the <color=#e21717>star</color> reach the bottom";

        m_ObjectiveScript.setText(m_Description);
    }

    public override bool checkObjective()
    {
        // Must be active
        if (!m_StarBlock.m_Script.gameObject.activeSelf)
            return false;

        // Good or Bad star?
        if (m_StarBlock.m_Good)
        {
            // Check if it touched ground, then complete
            if(m_StarBlock.arrPosY == 0)
            {
                m_Complete = true;
                m_ObjectiveScript.addText(" <b>(COMPLETED)</b>");
                m_ObjectiveScript.setGreen();
            }
        }
        else
        {
            // Check if it touched ground, then fail
            if (m_StarBlock.arrPosY == 0)
            {
                m_Failed = true;
                m_ObjectiveScript.addText(" <b>(FAILED)</b>");
                m_ObjectiveScript.setRed();
            }
            else
            {
                // Check all other objectives
                bool allOK = true;
                for(int i = 0; i < allObjectives.Count; i++)
                {
                    // Check if all are completed, if not... 
                    if(!allObjectives[i].m_Complete)
                    {
                        // If this is also a BAD star, and it has failed... We do not set completed.
                        if(allObjectives[i].m_Type == TYPE.TIMED)
                        {
                            // Timed is always OK until its not.
                        }
                        else if (allObjectives[i].m_Type == TYPE.STAR && !((Objective_Stars)allObjectives[i]).m_StarBlock.m_Good)
                        {
                            if(allObjectives[i].m_Failed)
                            {
                                allOK = false;
                                break;
                            }
                        }
                        else
                        {
                            // This was another type of block that is not completed.
                            allOK = false;
                            break;
                        }
                    }
                }

                // Everything is completed
                if(allOK)
                {
                    m_Complete = true;
                    m_ObjectiveScript.addText(" <b>(COMPLETED)</b>");
                    m_ObjectiveScript.setGreen();
                }
            }
        }

        return m_Complete;
    }
}
