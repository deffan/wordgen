﻿using System.Collections.Generic;

public class Objective_Find_This : Objective
{
    private string m_Word;

    public Objective_Find_This(string word) 
    {
        // Bör vara ett ord som är min 2 och max 4 bokstäver långt för att vara rimligt att hitta slumpmässigt.
        m_Type = TYPE.FIND_WORD;
        m_Description = "Find <c1>" + word + "</c>";
        m_Word = word;
        m_ObjectiveScript.setText(m_Description);
    }

    public override bool checkObjective()
    {
        if(m_Word.Equals(word))
        {
            m_Complete = true;
            m_ObjectiveScript.setGreen();
        }
        return m_Complete;
    }
}
