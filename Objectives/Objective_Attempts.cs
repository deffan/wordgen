﻿using System.Collections.Generic;

public class Objective_Attempts : Objective
{
    private string m_Word;
    private int m_Attempts;

    public Objective_Attempts(string word, int tries)
    {
        m_Type = TYPE.ATTEMPTS;
        m_Word = word;
        m_Attempts = tries;
        m_Description = "Find <c1>" + word + "</c> within <c1>{0} attempts</c>";
        m_ObjectiveScript.setText(string.Format(m_Description, tries));
    }

    public override bool checkObjective()
    {
        if(m_Word.Equals(word))
        {
            m_Complete = true;
            m_ObjectiveScript.addText(" (" + word + ")");
            m_ObjectiveScript.setGreen();
        }
        else
        {
            m_ObjectiveScript.setText(string.Format(m_Description, m_Attempts - attempts));
            if(m_Attempts - attempts <= 0)
            {
                m_ObjectiveScript.setRed();
                m_Failed = true;
            }
        }

        return m_Complete;
    }
}
