﻿using System.Collections.Generic;

public class Objective_Manual : Objective
{
    private List<string> m_Answers;
    private List<string> m_Hints;
    // For example:
    // Description: In a ______ far far away...
    // Answer: galaxy
    // Hint: Famous first line from Star Wars. 
    //       Here is the answer in jumbled form: alagyx

    // A small step for ___ a giant leap for mankind.
    // One two _____ four five
    // thursday ______ saturday 
    // osv...

    // Or several answers:
    // A shape:
    // Answers: triangle, square, circle ...


    // Egentligen är detta bättre än databasen med kategorier....           ----- SKIPPA KATEGORIER I DATABASEN!
    // Kräver bara lite mer manuellt arbete, men är antagligen snabbare.
    // Problemet är när det är måånga ord.
    // ...
    // sånt med många ord, får bli kategori. Annars manuellt, tex veckodagar, nummer upp till 10, osv.

    // Shapes
    // Religions
    // Days
    // Numbers (upto 10)
    // 

    // "Frågor"
    // Fish swims in _ _ _ _ _ (water)
    // Largest object in the solarsystem? _ _ _ (sun)
    // 
    // Andromeda is a _ _ _ _ _ _ galaxy
    // At the center of our galaxy is a black _ _ _ _ (hole)
    // Albert Einstein won the Nobel _ _ _ _ _ (prize)



    public Objective_Manual(string description, List<string> answers, List<string> hints, bool hintCost)
    {
        m_Type = TYPE.MANUAL;
        m_Answers = answers;
        m_Hints = hints;

        m_Description = description;
        m_ObjectiveScript.setText(m_Description);

        // Hints
        if (m_Hints != null && m_Hints.Count > 0)
        {
            string hinttext = "<b><color=#ffa500ff>Hint:</color></b>\n";
            int currentLineCount = 0;
            for (int i = 0; i < m_Hints.Count; i++)
            {
                if (currentLineCount + m_Hints[i].Length > 70)
                {
                    hinttext += "\n";
                    currentLineCount = 0;
                }
                else
                    currentLineCount += m_Hints[i].Length + 2;

                hinttext += m_Hints[i].ToLower();
                if (i < m_Hints.Count - 1)
                    hinttext += ", ";
            }
            m_ObjectiveScript.setHint(hinttext, hintCost);
        }
    }

    public override bool checkObjective()
    {
        for (int i = 0; i < m_Answers.Count; i++)
        {
            if (word.Equals(m_Answers[i]))
            {
                m_Complete = true;
                m_ObjectiveScript.addText(" (" + word + ")");
                m_ObjectiveScript.setGreen();
                break;
            }
        }
        return m_Complete;
    }
}
