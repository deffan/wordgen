﻿using System.Collections.Generic;

public class Objective_Consecutive : Objective
{

    public Objective_Consecutive()
    {
        m_Type = TYPE.CONSECUTIVE;
        m_Description = "Find a word with <c1>identical consecutive letters</c>";
        m_ObjectiveScript.setText(m_Description);
    }

    public override bool checkObjective()
    {
        if (!wordFound)
            return false;

        for (int i = 0; i < word.Length - 1; i++)
        {
            if(word[i] == word[i + 1])
            {
                m_Complete = true;
                m_ObjectiveScript.addText(" (" + word + ")");
                m_ObjectiveScript.setGreen();
                break;
            }
        }

        return m_Complete;
    }
}
