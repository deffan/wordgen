﻿using System.Collections.Generic;

public class Objective_Find_Twice : Objective
{
    public Objective_Find_Twice()
    {
        m_Type = TYPE.TWICE;
        m_Description = "Find a word <c1>twice</c>";
        m_ObjectiveScript.setText(m_Description);
    }

    public override bool checkObjective()
    {
        int count = 0;
        for (int i = 0; i < allWords.Count; i++)
            if (allWords[i].Equals(word))
                count++;

        if (count >= 2)
        {
            m_Complete = true;
            m_ObjectiveScript.addText(" (" + word + ")");
            m_ObjectiveScript.setGreen();
        }

        return m_Complete;
    }
}
