﻿using System.Collections.Generic;

public class Objective_Find_Word_StartingEnding : Objective
{
    private char m_Char;
    private bool m_Starting;
    private int m_Length;

    public Objective_Find_Word_StartingEnding(int len, bool starting, char character)
    {
        m_Type = TYPE.FIND_STARTING_ENDING;
        m_Length = len;
        m_Description = "<c1>" + m_Length + "</c> letters ";
        string hintwrd = "";
        if (starting)
        {
            m_Description += "starting with <c1>" + character + "</c>";

            // Put together the hint matching word
            hintwrd += character;
            for (int i = 1; i < len; i++)
                hintwrd += "_";
        }
        else
        {
            m_Description += "ending with <c1>" + character + "</c>";

            // Put together the hint matching word
            for (int i = 0; i < len - 1; i++)
                hintwrd += "_";
            hintwrd += character;
        }

        m_Char = character;
        m_Starting = starting;
        m_ObjectiveScript.setText(m_Description);

        // Add hint
        List<string> matchingWords = GAMEGLOBAL.LVL.m_DB.getWordsMatching("words", hintwrd, len);
        if (matchingWords.Count > 0)
        {
            string hinttext = "<b><color=#ffa500ff>Examples of matching words:</color></b>\n";
            int currentLineCount = 0;
            for (int i = 0; i < matchingWords.Count; i++)
            {
                if (currentLineCount + matchingWords[i].Length + 2 >= 70)
                {
                    hinttext += "\n";
                    currentLineCount = 0;
                }
                else
                    currentLineCount += matchingWords[i].Length + 2;

                hinttext += matchingWords[i].ToLower();
                if (i < matchingWords.Count - 1)
                    hinttext += ", ";
            }
            m_ObjectiveScript.setHint(hinttext, true);
        }
    }

    public override bool checkObjective()
    {
        if (!wordFound)
            return false;

        if (word.Length != m_Length)
            return false;

        if (m_Starting)
        {
            if (word[0] == m_Char)
            {
                m_Complete = true;
                m_ObjectiveScript.addText(" (" + word + ")");
                m_ObjectiveScript.setGreen();
            }
        }
        else
        {
            if (word[word.Length - 1] == m_Char)
            {
                m_Complete = true;
                m_ObjectiveScript.addText(" (" + word + ")");
                m_ObjectiveScript.setGreen();
            }
        }

        return m_Complete;
    }

}
