﻿using System;
using System.Collections.Generic;

public class Objective_Finish_The_Word : Objective
{
    private string m_Word;
    private int m_Count;

    public Objective_Finish_The_Word(string word)
    {
        m_Type = TYPE.FINISH_WORD;
        m_Word = word;

        // Word must be larger than 3 letters.
        if (word.Length < 3)
            throw new Exception("Objective_Finish_The_Word - Is less than 3 letters long.");

        // The amount of letters to to mask/remove and where
        m_Count = GAMEGLOBAL.LVL.m_Random.Next(2, m_Word.Length - 1);               // Min 2 and maximum wordlength - 1
        if (m_Count > 3)
            m_Count = 3;

        int pos = GAMEGLOBAL.LVL.m_Random.Next(0, m_Word.Length - m_Count + 1);     // Position between 0 and wordlenght - count

        string newWord = "";
        int count = 0;
        for (int i = 0; i < m_Word.Length; i++)
        {
            if (i >= pos && count < m_Count)
            {
                newWord += "_";
                count++;
            }
            else
                newWord += m_Word[i];
        }
        m_Word = newWord;
        m_Description = "Fill in the blanks: <c1>" + m_Word + "</c>";
        m_ObjectiveScript.setText(m_Description);

        // Add hint
        List<string> matchingWords = GAMEGLOBAL.LVL.m_DB.getWordsMatching("words", m_Word, m_Word.Length);
        if (matchingWords.Count > 0)
        {
            string hinttext = "<b><color=#ffa500ff>Examples of matching words:</color></b>\n";
            int currentLineCount = 0;
            for (int i = 0; i < matchingWords.Count; i++)
            {
                if (currentLineCount + matchingWords[i].Length + 2 >= 70)
                {
                    hinttext += "\n";
                    currentLineCount = 0;
                }
                else
                    currentLineCount += matchingWords[i].Length + 2;

                hinttext += matchingWords[i].ToLower();
                if (i < matchingWords.Count - 1)
                    hinttext += ", ";
            }
            m_ObjectiveScript.setHint(hinttext, true);
        }
        else
            UnityEngine.Debug.Log("Did not find a matching word for: " + m_Word);

    }

    public override bool checkObjective()
    {
        // First check that its the same length
        if(m_Count == word.Length)
        {
            // Now add words string into m_Word string
            string wordTest = "";
            int count = 0;
            for (int i = 0; i < m_Word.Length; i++)
                if (m_Word[i] == '_')
                {
                    wordTest += word[count];
                    count++;
                }
                else
                    wordTest += m_Word[i];

            // Check database for a match
            if (GAMEGLOBAL.LVL.m_DB.isWord(wordTest))
            {
                m_Complete = true;
                m_ObjectiveScript.addText(" (" + wordTest + ")");
                m_ObjectiveScript.setGreen();
            }
        }

        return m_Complete;
    }
}
