﻿using System.Collections.Generic;

public class Objective_Containing : Objective
{
    private char[] m_Chars;

    public Objective_Containing(string word, int amount)
    {
        m_Type = TYPE.CONTAINING;
        m_Chars = new char[amount];

        m_Description = "Word containing: <c1>";
        for (int i = 0; i < amount; i++)
        {
            // Identical characters not allowed
            bool Ok = true;
            char newChar = 'A';
            while (Ok)
            {
                Ok = false;
                newChar = word[GAMEGLOBAL.LVL.m_Random.Next(0, word.Length)];   // Get characters from this random word
                for (int j = 0; j < amount; j++)
                {
                    if (m_Chars[j] == newChar)
                    {
                        Ok = true;
                        break;
                    }
                }
            }

            m_Chars[i] = newChar;

            if (i > 0 || i + 1 == amount)
                m_Description += ", ";
            m_Description += m_Chars[i];
        }
        m_Description += "</c>";

        m_ObjectiveScript.setText(m_Description);
    }

    public override bool checkObjective()
    {
        if (!wordFound)
            return false;

        int count = 0;
        for (int i = 0; i < m_Chars.Length; i++)
        {
            if (word.Contains(m_Chars[i] + ""))
                count++;
        }

        if (count == m_Chars.Length)
        {
            m_Complete = true;
            m_ObjectiveScript.addText(" (" + word + ")");
            m_ObjectiveScript.setGreen();
        }

        return m_Complete;
    }
}
