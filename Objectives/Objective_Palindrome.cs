﻿using System.Collections.Generic;

public class Objective_Palindrome : Objective
{
    private int m_MinLen;

    public Objective_Palindrome(int minLen)
    {
        m_Type = TYPE.PALINDROME;
        m_MinLen = minLen;
        m_Description = "<c1>Palindrome</c> minimum length <c1>" + m_MinLen + "</c>";
        m_ObjectiveScript.setText(m_Description);
        m_ObjectiveScript.setHint("<b><color=#ffa500ff>Hint:</color></b>\nA palindrome is a word that reads the same backwards as forwards.\n", false);
    }

    public override bool checkObjective()
    {
        if (!wordFound)
            return false;

        if (word.Length < m_MinLen)
            return false;

        string palindrome = "";
        for (int i = word.Length - 1; i >= 0; i--)
            palindrome += word[i];

        if (palindrome.Equals(word))
        {
            m_Complete = true;
            m_ObjectiveScript.addText(" (" + word + ")");
            m_ObjectiveScript.setGreen();
        }
        return m_Complete;
    }
}
