﻿using System.Collections.Generic;

public class Objective_Find_X_Words : Objective
{
    private int m_Num;
    private int m_Length;
    private bool m_MinLength;

    public Objective_Find_X_Words(int number, int Length, bool minimum)
    {
        m_MinLength = minimum;
        m_Length = Length;
        m_Type = TYPE.FIND_X_WORDS;
        if (number == 1)
            m_Description = "<c1>" + number + "</c> word ({0}/{1})";
        else
            m_Description = "<c1>" + number + "</c> words ({0}/{1})";

        if (m_Length > 0 && m_MinLength)
            m_Description += " with<c1> at least " + m_Length + "</c> letters";
        else if (m_Length > 0)
            m_Description += " with<c1> exactly " + m_Length + "</c> letters";

        m_Num = number;
        m_ObjectiveScript.setText(string.Format(m_Description, 0, m_Num));
    }

    public override bool checkObjective()
    {
        // Specific length?
        if (m_Length > 0)
        {
            int count = 0;
            for(int i = 0; i < allWords.Count; i++)
            {
                if(m_MinLength)
                {
                    if (allWords[i].Length >= m_Length)
                        count++;
                }
                else
                {
                    if (allWords[i].Length == m_Length)
                        count++;
                }
            }

            m_ObjectiveScript.setText(string.Format(m_Description, count, m_Num));
            if (count >= m_Num)
            {
                m_Complete = true;
                m_ObjectiveScript.setGreen();
            }
        }
        else // No specific length
        {
            m_ObjectiveScript.setText(string.Format(m_Description, allWords.Count, m_Num));
            if (allWords.Count >= m_Num)
            {
                m_Complete = true;
                m_ObjectiveScript.setGreen();
            }
        }
        return m_Complete;
    }

}
