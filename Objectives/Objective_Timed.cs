﻿using UnityEngine;
using System.Collections;

public class Objective_Timed : Objective
{
    public int m_Time;
    public bool m_checkTime;
    private System.Text.StringBuilder m_SB;

    public Objective_Timed(int time)
    {
        m_Type = TYPE.TIMED;
        setData("", null, 0, false, GAMEGLOBAL.LVL.m_currentLevel.m_Objectives);
        m_SB = new System.Text.StringBuilder();
        m_Time = time;
        m_checkTime = false;
        m_SB.AppendFormat("Complete the level within <color=#ffa500ff>{0}</color> seconds", m_Time);
        m_Description = m_SB.ToString();
        m_ObjectiveScript.setText(m_Description);
        GAMEGLOBAL.LVL.m_Behaviour.timedEvent(this);
    }

    public override bool checkObjective()
    {
        if (m_Failed)
            return false;

        // Every second this is true and checked
        if(m_checkTime)
        {
            m_Time--;
            if(m_Time <= 0)
            {
                // Kanske "flasha" mellan olika orange-röda färger varannan sekund sista 5 sekunderna?

                m_ObjectiveScript.addText(" <b>(FAILED)</b>");
                m_ObjectiveScript.setRed();
                m_Failed = true;
                GAMEGLOBAL.LVL.m_currentLevel.levelFailed();
            }
            else
            {
                m_SB.Length = 0;
                m_SB.AppendFormat("Complete the level within <color=#ffa500ff>{0}</color> seconds", m_Time);
                m_Description = m_SB.ToString();
                m_ObjectiveScript.setText(m_Description);
            }
        }

        // Check all other objectives... this objective MUST be last in the objectives list.
        bool allOK = true;
        for (int i = 0; i < allObjectives.Count; i++)
        {
            if (!allObjectives[i].m_Complete && allObjectives[i].m_Type != TYPE.TIMED)
            {
                allOK = false;
                break;
            }
        }

        if(allOK)
        {
            m_Complete = true;
            m_ObjectiveScript.addText(" <b>(COMPLETED)</b>");
            m_ObjectiveScript.setGreen();
        }

        return m_Complete;
    }
}
