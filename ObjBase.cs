﻿using UnityEngine;
using System.Collections;

public class ObjBase : MonoBehaviour
{
    public Mesh m_MeshLetter;
    public Mesh m_MeshBackground;
    public bool m_selected;
    public int m_ID;
    public int arrPosX;
    public int arrPosY;
    public int m_LetterValue;
    public char m_LetterChar;
    Vector2[] newUVsBackground;
    Vector2[] defaultUVS;

    public void init(Mesh mL, Mesh mB, char C, int Cval, int id)
    {
        m_LetterValue = Cval;
        m_LetterChar = C;
        m_ID = id;
        arrPosX = -1;
        arrPosY = -1;

        m_MeshBackground = mB;
        newUVsBackground = m_MeshBackground.uv;
        setMeshBackgroundUV();

        m_MeshLetter = mL;
        defaultUVS = m_MeshLetter.uv;
        setMeshLetterUV(Cval);
    }

    public void select()
    {
        m_selected = true;
        changeMesh(true);
    }

    public void deselect()
    {
        m_selected = false;
        changeMesh(false);
    }

    public void reset()
    {
        arrPosX = -1;
        arrPosY = -1;
        m_MeshLetter.uv = defaultUVS;
    }

    public void refresh()
    {
        setMeshLetterUV(m_LetterValue);
    }

    void setMeshLetterUV(int letterValue)
    {
        Vector2[] newUVs = m_MeshLetter.uv;
        for (int b = 0; b < m_MeshLetter.uv.Length; b++)
            newUVs[b].x += GAMEGLOBAL.LVL.m_UVOffset * letterValue;
        m_MeshLetter.uv = newUVs;
    }

    // Go down one step in UVs to get to "unselected" default
    void setMeshBackgroundUV()
    {
        newUVsBackground = m_MeshBackground.uv;
        for (int b = 0; b < m_MeshBackground.uv.Length; b++)
            newUVsBackground[b].y -= GAMEGLOBAL.LVL.m_UVOffset;
        m_MeshBackground.uv = newUVsBackground;
    }

    void changeMesh(bool select)
    {
        newUVsBackground = m_MeshBackground.uv;
        if (select)
        {
            // Shift UV coordinates down
            for (int b = 0; b < m_MeshBackground.uv.Length; b++)
                newUVsBackground[b].y -= GAMEGLOBAL.LVL.m_UVOffset;
        }
        else
        {
            // Shift UV coordinates up
            for (int b = 0; b < m_MeshBackground.uv.Length; b++)
                newUVsBackground[b].y += GAMEGLOBAL.LVL.m_UVOffset;
        }
        m_MeshBackground.uv = newUVsBackground;
    }

    private Texture getTexture(string path)
    {
        return (Texture)Resources.Load(path);
    }

}
