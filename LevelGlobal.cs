﻿using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LevelGlobal 
{
    public Level m_currentLevel;
    public Block[,] m_Arr;
    public Block[,] m_SideArr;
    public int GAME_WIDTH = 8;
    public int GAME_HEIGHT = 5;
    public List<Block> m_Boxes;
 //   public List<Block> m_SwipeBoxes;
    public List<ParticleSystem> m_Faders;
    public List<ParticleSystem> m_BombExplosions;
    public GameObject m_Poof;
    public GameObject m_Spark1;
    public GameObject m_Spark2;
    public List<ObjectiveScript> m_Objectives;
    public textScript m_RewardPopup;

//    public List<Mesh> m_BackgroundMeshesFront;
    public List<Mesh> m_BackgroundMeshesBack;
    public Camera m_Camera;
    public GameObject m_CameraParent;
    public string m_Text;
    public string m_LastSetText;
    public DB m_DB;
    public bool m_isEvent;
    public LevelBehaviour m_Behaviour;
    public System.Random m_Random;
    public float m_UVOffset = 64f / 2048f;
    public GameObject m_BoxesObject;
    public GameObject m_ParticleSystemsObject;
    public GameObject m_LevelGameObject;
    public int m_IDCount;
    public textScript m_UPText;
    public TextMesh m_PointsText;
    public TextMesh m_LastWordText;
    public int m_PointsLimit;
    public int m_Points;
    public int m_Coins;
    public smallButtonScript m_BuyButton;
    public smallButtonScript m_SettingsButton;
    public List<Block> m_Selected;
    public bool m_SelectedAny;
    public bool m_SelectedGold;
    public GameObject m_HintPanel;
    public TextMesh m_HintText;
    public bool m_HintStatus;
    public GameObject m_Menu;

    public textScript m_TopText;
    public PlayerData m_PlayerData;

    private Material m_material;
    private Mesh m_flat;

    public bool m_ShutDownGame;

    private System.DateTime m_Quest1;
    private System.DateTime m_Quest2;
    private System.DateTime m_Quest3;
    private System.DateTime m_Quest4;
    private System.DateTime m_Quest5;


    // Colors
    public Color32 m_Magenta = new Color32(188, 0, 204, 255);
    public Color32 m_Green = new Color32(0, 200, 25, 255);
    public Color32 m_Yellow = Color.yellow;
    public Color32 m_Orange = new Color32(255, 160, 0, 255);
    public Color32 m_DampBlue = new Color32(21, 126, 126, 255);
    public Color32 m_Blue = new Color32(0, 95, 187, 255);
    public Color32 m_DarkRed = new Color32(160, 55, 55, 255);
    public Color32 m_LightBrown = new Color32(166, 140, 0, 255);
    public Color32 m_White = Color.white;

    private AdManager m_AdManager;
    public dailyQuestButtonScript m_WatchedQuest;

    public void Init ()
    {
        m_PlayerData = new PlayerData();

        m_AdManager = GameObject.Find("Logic").GetComponent<AdManager>();

        m_RewardPopup = GameObject.Find("RewardPopup").GetComponent<textScript>();
        m_RewardPopup.init(0.7f);
        m_RewardPopup.gameObject.SetActive(false);



        m_Quest1 = System.DateTime.Now;
        m_Quest2 = System.DateTime.Now;
        m_Quest3 = System.DateTime.Now;
        m_Quest4 = System.DateTime.Now;
        m_Quest5 = System.DateTime.Now;

        m_Coins = 0;
        m_currentLevel = null;
        m_LevelGameObject = GameObject.Find("Level");
        m_UPText = GameObject.Find("UPText").GetComponent<textScript>(); 
        m_TopText = GameObject.Find("TOPTEXT").GetComponent<textScript>();
        m_TopText.init(0.4f);
        m_UPText.init(0.6f);

        m_HintPanel = GameObject.Find("HintPanel");
        m_HintText = GameObject.Find("HintText").GetComponent<TextMesh>();
        setUVs(m_HintPanel.GetComponent<MeshFilter>().mesh, 666, 109, 2048, 1220, 325);
        m_HintPanel.SetActive(false);
        m_HintText.gameObject.SetActive(false);
        m_HintStatus = false;

        m_Menu = GameObject.Find("Menu");

        m_LastSetText = "";
        m_UPText.gameObject.SetActive(false);
        m_ShutDownGame = false;
        m_SelectedAny = false;
        m_SelectedGold = false;
        m_Random = new System.Random();
        m_isEvent = false;
        m_Arr = new Block[GAME_WIDTH, GAME_HEIGHT];
        m_SideArr = new Block[1, GAME_HEIGHT];
        m_DB = new DB();
        m_CameraParent = GameObject.Find("CAMERA");
        m_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        m_Text = "";
        m_PointsLimit = 16;
        m_Points = 0;

        m_ParticleSystemsObject = new GameObject("PARTICLESYSTEMS");

        GameObject temp = (GameObject)Resources.Load("Prefabs/flat");
        m_material = temp.GetComponent<MeshRenderer>().sharedMaterial;
        m_flat = temp.GetComponent<MeshFilter>().sharedMesh;
        
        m_Selected = new List<Block>();
        m_Boxes = new List<Block>();
     //   m_SwipeBoxes = new List<Block>();
        m_Faders = new List<ParticleSystem>();

        // POOF object (selected green puffiness)
        GameObject ox2 = (GameObject)Resources.Load("Prefabs/PoofFab");
        m_Poof = GameObject.Instantiate(ox2);
        m_Poof.name = "THE_POOF";
        turnPoofOnOff(false);

        GameObject ox3 = (GameObject)Resources.Load("Prefabs/tinySparkFab");
        m_Spark1 = GameObject.Instantiate(ox3);
        m_Spark1.name = "THE_SPARK_1";
        m_Spark2 = GameObject.Instantiate(ox3);
        m_Spark2.name = "THE_SPARK_2";
        turnSpark1OnOff(false);
        turnSpark2OnOff(false);

        // m_BoxesRigidbodys = new List<Rigidbody>();
        m_Objectives = new List<ObjectiveScript>();
   //     m_BackgroundMeshesFront = new List<Mesh>();
        m_BackgroundMeshesBack = new List<Mesh>();
        m_BombExplosions = new List<ParticleSystem>();

        GameObject faders = GameObject.Find("FADERS");

        GameObject triggers = GameObject.Find("TRIGGERS");
        m_BoxesObject = GameObject.Find("BOXES");
        GameObject objectives = GameObject.Find("OBJECTIVE_PANEL");
    //    GameObject swipe = GameObject.Find("SWIPE");

        m_IDCount = 0;


        // Objective prefabs
        GameObject o = (GameObject)Resources.Load("Prefabs/ObjectiveFab");
        for (int i = 0; i < 4; i++)
        {
            GameObject no = GameObject.Instantiate(o);
            no.transform.SetParent(objectives.transform);
            no.name = "OBJECTIVE_" + i;
            ObjectiveScript os = no.GetComponent<ObjectiveScript>();
            os.init();
            m_Objectives.Add(os);
            no.transform.localPosition = new Vector3(0, i * -0.55f, -1);
            no.SetActive(false);
        }

        // Fader objects
        GameObject o1 = (GameObject)Resources.Load("Prefabs/FaderFab");
        for (int i = 0; i < GAME_WIDTH * (GAME_HEIGHT + 2); i++)
        {
            GameObject no1 = GameObject.Instantiate(o1);
            no1.transform.SetParent(faders.transform);
            no1.name = "FADER_" + i;
            no1.gameObject.SetActive(false);
            m_Faders.Add(no1.GetComponent<ParticleSystem>());
        }

        // Create trigger in the middle of each position
        float hPos = 0;
        for (int i = 0; i < GAME_WIDTH; i++)
        {
            hPos = -10;
            for (int j = 0; j < GAME_HEIGHT; j++)
            {
                // With a "arrayPositioner" script which automatically sets the array value
                GameObject trigger = new GameObject("TRIGGER_" + m_IDCount);
                trigger.isStatic = true;
                trigger.transform.SetParent(triggers.transform);
                hPos++;
                trigger.transform.position = new Vector3(i, hPos, 0);          // 9.25 är baserat på var "floor" ligger...
                trigger.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                BoxCollider b0 = trigger.AddComponent<BoxCollider>();
                b0.isTrigger = true;
                arrayPositioner arrPos = trigger.AddComponent<arrayPositioner>();
                arrPos.init(m_Arr, i, j, BlockScript.BLOCKLOCATION.GAME);

                // Create a full playfield of letter boxes/blocks
                Block b = getNewBlock(Block.TYPE.LETTER);
                m_Boxes.Add(b);
            }
        }

        // Sidebar
        hPos = -10;
        for (int j = 0; j < GAME_HEIGHT; j++)
        {
            // With a "arrayPositioner" script which automatically sets the array value
            GameObject trigger = new GameObject("TRIGGER_" + m_IDCount);
            trigger.isStatic = true;
            trigger.transform.SetParent(triggers.transform);
            hPos++;
            trigger.transform.position = new Vector3(GAME_WIDTH + 0.5f, hPos, 0); 
            trigger.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            BoxCollider b0 = trigger.AddComponent<BoxCollider>();
            b0.isTrigger = true;
            arrayPositioner arrPos = trigger.AddComponent<arrayPositioner>();
            arrPos.init(m_SideArr, 0, j, BlockScript.BLOCKLOCATION.SIDE);

            // Add more to cover the sidebar too
            Block b = getNewBlock(Block.TYPE.LETTER);
            m_Boxes.Add(b);
        }
        createLevelBackground();

        // Create the Buy and Settings buttons
        GameObject bf = (GameObject)Resources.Load("Prefabs/smallButtonFab");
        {
            GameObject no1 = GameObject.Instantiate(bf);
            no1.transform.SetParent(m_LevelGameObject.transform);
            no1.transform.position = new Vector3(GAME_WIDTH + 1f, -10.43f, -1);
            no1.isStatic = true;
            no1.name = "BUYBUTTON";
            m_BuyButton = no1.GetComponent<smallButtonScript>();
            m_BuyButton.init();
            m_BuyButton.setType(smallButtonScript.BUTTONTYPE.BUY);
            m_BuyButton.setButtonStatus(smallButtonScript.STATUS.SHOW);
        }

        {
            GameObject no1 = GameObject.Instantiate(bf);
            no1.transform.SetParent(m_LevelGameObject.transform);
            no1.transform.position = new Vector3(GAME_WIDTH + 1f, -11.3f, -1);
            no1.isStatic = true;
            no1.name = "SETTINGSBUTTON";
            m_SettingsButton = no1.GetComponent<smallButtonScript>();
            m_SettingsButton.init();
            m_SettingsButton.setType(smallButtonScript.BUTTONTYPE.SETTINGS);
            m_SettingsButton.setButtonStatus(smallButtonScript.STATUS.SHOW);
        }

        // Add some more just in case!
        for(int i = 0; i < 5; i++)
        {
            Block blkb = getNewBlock(Block.TYPE.LETTER);
            m_Boxes.Add(blkb);
        }
    }

    public bool isQuestReady(int id)
    {
        switch(id)
        {
            case 1:
                if (m_Quest1 < System.DateTime.Now)
                    return true;
                break;
            case 2:
                if (m_Quest2 < System.DateTime.Now)
                    return true;
                break;
            case 3:
                if (m_Quest3 < System.DateTime.Now)
                    return true;
                break;
            case 4:
                if (m_Quest4 < System.DateTime.Now)
                    return true;
                break;
            case 5:
                if (m_Quest5 < System.DateTime.Now)
                    return true;
                break;
        }
        return false;
    }

    public void setQuestDone(int id)
    {
        switch (id)
        {
            case 1: m_Quest1.AddHours(3); break;
            case 2: m_Quest2.AddHours(3); break;
            case 3: m_Quest3.AddHours(3); break;
            case 4: m_Quest4.AddHours(3); break;
            case 5: m_Quest5.AddHours(3); break;
        }
    }

    public void startVideo(dailyQuestButtonScript quest)
    {
        m_WatchedQuest = quest; // If null, we watched a "non daily video"
        m_AdManager.ShowAd();
    }


    /*
    public void createSwipe(Transform parent)
    {
        // Create the Buy Button
        GameObject bf = (GameObject)Resources.Load("Prefabs/smallButtonFab");
        {
            GameObject no1 = GameObject.Instantiate(bf);
            no1.transform.SetParent(parent);
            no1.transform.position = new Vector3(GAME_WIDTH + 2.35f, -9.7f, -1);
            no1.isStatic = true;
            no1.name = "BUYBUTTON";
            m_BuyButton = no1.GetComponent<smallButtonScript>();
            m_BuyButton.init();
            m_BuyButton.setButtonStatus(2);
        }

        // Fyll "SWIPEBLOCKS" med alla block, utan gravitation exakt 1x1 mellanrum.
        Transform swipeblocks = parent.GetChild(0).transform;
        for (int i = 0; i < 25; i++)
        {
            LetterBlock b = (LetterBlock)getNewBlock(Block.TYPE.LETTER);
            b.setCharacter(getCharacter(i), i);
            m_SwipeBoxes.Add(b);

            Rigidbody r = b.m_Script.GetComponent<Rigidbody>();
            r.isKinematic = true;
            b.m_Script.transform.position = new Vector3(GAME_WIDTH + 2, 20 - i, 0);
            b.m_Script.transform.SetParent(swipeblocks);
            b.m_Script.m_Location = BlockScript.BLOCKLOCATION.SWIPE;
            setUVs(b.m_Script.m_BackgroundMesh, 8, 5);
            b.background_x_coord = 8;
            b.m_Script.gameObject.SetActive(true);
        }

        swipeblocks.position = new Vector3(0.3f, -15, 1);

        GameObject top_pipe = parent.GetChild(1).gameObject;
        GameObject bottom_pipe = parent.GetChild(2).gameObject;

        setUVs(top_pipe.GetComponent<MeshFilter>().mesh, 87, 238, 2048, 0, 1106);
        setUVs(bottom_pipe.GetComponent<MeshFilter>().mesh, 87, 238, 2048, 0, 1106);

        // hämta meshfilter->mesh
        // setUVs(87, 238, 2048, 0, 1106);

        // kom ihåg att matcha storleken på objekten med uvs så det ser rätt ut.

        // TOP_PIPE och BOTTOM_PIPE ska sättas UVs på, identiska. Sen bara rotera och placera rätt.
    }
    */

    public void setHint(bool ON, string txt)
    {
        if(ON)
        {
            m_HintStatus = true;
            m_HintText.text = txt;
            m_HintPanel.SetActive(true);
            m_HintText.gameObject.SetActive(true);
            //m_HintPanel.GetComponent<ParticleSystem>().Play();
        }
        else
        {
            m_HintStatus = false;
            m_HintPanel.SetActive(false);
            m_HintText.gameObject.SetActive(false);
        }
    }

    public void updateHints()
    {
        for (int i = 0; i < m_Objectives.Count; i++)
        {
            if (m_Objectives[i].gameObject.activeSelf && 
                m_Objectives[i].m_Hint.m_status != hintScript.STATUS.HIDDEN &&
                m_Objectives[i].m_Hint.m_Text.Length > 0)
            {
                if(m_Objectives[i].m_Hint.m_Cost)
                {
                    if (m_Coins < 100)
                        m_Objectives[i].setHintStatus(hintScript.STATUS.GRAYED);
                    else
                        m_Objectives[i].setHintStatus(hintScript.STATUS.SHOW);
                }
                else
                {
                    m_Objectives[i].setHintStatus(hintScript.STATUS.SHOW);
                }
            }
        }
    }

    public void startLevel(int level)
    {
        m_Selected.Clear();
        m_SelectedAny = false;
        m_SelectedGold = false;
        turnPoofOnOff(false);
        m_Points = 0;

        m_BuyButton.init();
        m_BuyButton.setButtonStatus(2);


        // Måste designa om detta.... av någon bisarr och oförklaring anledning.

        // Det jag är lite rädd över nu är att NewBlock kanske KONSTANT genererar nya block när det inte ska ske?
        // Men bara på android då....
        // BRA KOLL MAN HAR MED UNITY ... NOT
        //
        // 



        // Jag har 4st Objective Prefabs. Dessa är instansierade, men jag har bara sparat ObjectiveScriptsen i en lista.
        // Därifrån kan jag ändå nå gameobjecten såklart.

        // Dessa vill jag återanvända, så varje OBJECTIVE sen kan bara bytas ut.
        // Och objective använder -> objectivescript.


        // Nån form av gissning här...
        // resultatet blir att alla objectives fylls med samma objective
        // men samtidigt blir de/är null ?!
        //
        // varför alla blir aktiva vet jag Ej, det är ungefär som att alla gameobjects länkar till exakt samma?!
        // vilket också makes-no-sense då det inte är så i editorn

        // Prova detta kanske.... men det kommer ha noll effekt om detta har med Level konstuktorn att göra.
        //
        // AND: Sätt igång rand_level igen, och se till så att alla objekt är inblandade i testerna....
        // Allt skall fungera...
        //
        // iterera alla objectives.gameobject.name och skriv ut name och se så de är olika...
        // om det är olika så är detta inte problemet.
        //
        // Annars vet jag inte.

        // Potentiellt prova UTAN objectives, bara för att testa annat...


        for (int i = 0; i < m_Objectives.Count; i++)
        {
            if(m_Objectives[i].m_Objective != null)
                m_Objectives[i].m_Objective.m_Failed = true;
            if(m_Objectives[i].m_Hint != null)
                m_Objectives[i].m_Hint.deactivate();
            m_Objectives[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < m_Boxes.Count; i++)
            m_Boxes[i].m_Script.gameObject.SetActive(false);

        m_currentLevel = getLevel(level);

        m_TopText.setText("<c1>Level</c> " + level + "    $ " + m_Coins + "    <c1>Score</c> 0    <c1>Last word:</c>");
    }

    public void destroyAll()
    {
        for (int i = 0; i < GAME_WIDTH; i++)
            for (int j = 0; j < GAME_HEIGHT; j++)
                destroy(i, j, true, true);

        clearSideBar();
    }

    public void updateInfoText(int coins, int score, string word)
    {
        m_TopText.setText("<c1>Level</c> " + m_currentLevel.m_Level + "    $ " + coins + "    <c1>Score</c> " + score + "    <c1>Last word:</c> " + word);
    }

    public void exitGame()
    {
        m_ShutDownGame = true;
    }

    public void Dispose()
    {
        m_DB.Dispose();
    }

    private void createLevelBackground()
    {
        GameObject walls = GameObject.Find("WALLS");
        GameObject bk = new GameObject("BACKGROUND_MESHES");
        bk.transform.SetParent(GameObject.Find("Level").transform);
        int c = 0;

        // Create background meshes.
        // Covering a +/- 5 squares around the game field.
        // Placed in the very back.
        for (int i = -8; i < GAME_WIDTH; i++)
        {
            for (int j = 0; j < GAME_HEIGHT + 6; j++)
            {
                GameObject meshObject = new GameObject("MESH_" + c);
                MeshRenderer rnd = meshObject.AddComponent<MeshRenderer>();
                MeshFilter filter = meshObject.AddComponent<MeshFilter>();

                meshObject.transform.SetParent(m_CameraParent.transform);
                meshObject.transform.localPosition = new Vector3(i, j - 4, 15);
                

                filter.sharedMesh = GameObject.Instantiate(m_flat);
                rnd.material = m_material;
                rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                rnd.receiveShadows = false;
                meshObject.isStatic = true;
                m_BackgroundMeshesBack.Add(filter.mesh);
                c++;
            }
        }
        setLevelBackground();



        // Now create the "walls"
        int h = -10;
        for (int i = 0; i < GAME_WIDTH + 2; i++)
        {
            for (int j = 1; j < GAME_HEIGHT + 1; j++)
            {
                if (i == 0 || i + 1 == GAME_WIDTH + 2)
                {
                    GameObject meshObject = new GameObject("MESH_" + c);
                    meshObject.transform.SetParent(walls.transform);
                    MeshRenderer rnd = meshObject.AddComponent<MeshRenderer>();
                    MeshFilter filter = meshObject.AddComponent<MeshFilter>();

                    meshObject.transform.position = new Vector3(i - 1, h + j, 1);
                    meshObject.transform.SetParent(bk.transform);

                    filter.sharedMesh = GameObject.Instantiate(m_flat);
                    rnd.material = m_material;
                    rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    rnd.receiveShadows = false;
                    meshObject.isStatic = true;

                    if (j == GAME_HEIGHT)
                    {
                        // Top
                        setUVs(filter.sharedMesh, 2, 13);
                        if(i == 0)
                            meshObject.transform.rotation = Quaternion.Euler(0, 180, -90);
                        else
                            meshObject.transform.rotation = Quaternion.Euler(0, 0, -90);
                    }
                    else if(i == 0)
                    {
                        setUVs(filter.sharedMesh, 9, 13);   // Left side
                        meshObject.transform.rotation = Quaternion.Euler(0, 0, 90);
                    }
                    else if (i + 1 == GAME_WIDTH + 2)
                    {
                        setUVs(filter.sharedMesh, 8, 13);   // Right side
                        meshObject.transform.rotation = Quaternion.Euler(0, 0, 90);
                    }
                }

                c++;
            }
        }

        // Sidebar wall
        for (int i = GAME_WIDTH + 2; i < GAME_WIDTH + 4; i++)
        {
            for (int j = 1; j < GAME_HEIGHT + 1; j++)
            {
                if (i == GAME_WIDTH + 3)
                {
                    GameObject meshObject = new GameObject("MESH_" + c);
                    meshObject.transform.SetParent(walls.transform);
                    MeshRenderer rnd = meshObject.AddComponent<MeshRenderer>();
                    MeshFilter filter = meshObject.AddComponent<MeshFilter>();

                    meshObject.transform.position = new Vector3(i - 2f, h + j, 1);
                    meshObject.transform.SetParent(bk.transform);

                    filter.sharedMesh = GameObject.Instantiate(m_flat);
                    rnd.material = m_material;
                    rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    rnd.receiveShadows = false;
                    meshObject.isStatic = true;

                    if (j == GAME_HEIGHT)
                    {
                        setUVs(filter.sharedMesh, 2, 13);
                        meshObject.transform.rotation = Quaternion.Euler(0, 180, -90);
                    }
                    else
                    {
                        setUVs(filter.sharedMesh, 9, 13);
                        meshObject.transform.rotation = Quaternion.Euler(0, 0, 90);
                    }
                        
                }

                c++;
            }
        }

        // Now the bottom wall
        for (int i = 0; i < GAME_WIDTH + 3; i++)
        {
            GameObject meshObject = new GameObject("MESH_" + c);
            meshObject.transform.SetParent(walls.transform);
            MeshRenderer rnd = meshObject.AddComponent<MeshRenderer>();
            MeshFilter filter = meshObject.AddComponent<MeshFilter>();

            meshObject.transform.position = new Vector3(i - 1, h, 1);
            meshObject.transform.SetParent(bk.transform);

            filter.sharedMesh = GameObject.Instantiate(m_flat);
            rnd.material = m_material;
            rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            rnd.receiveShadows = false;
            meshObject.isStatic = true;
            
            if(i == 0)                          // LEFT corner
            {
                setUVs(filter.sharedMesh, 4, 13);
               // meshObject.transform.rotation = Quaternion.Euler(0, 0, 90);
            }
            else if(i == GAME_WIDTH + 1)        // SIDEBAR MIDDLE
            {
                setUVs(filter.sharedMesh, 0, 13);
                meshObject.transform.rotation = Quaternion.Euler(0, 180, -90);
            }
            else if(i + 1 == GAME_WIDTH + 3)    // RIGHT corner
            {
                setUVs(filter.sharedMesh, 5, 13);
                meshObject.transform.rotation = Quaternion.Euler(0, 0, 180);
                meshObject.transform.position = new Vector3(i - 0.47f, h, 1);
            }
            else                                // BOTH
            {
                setUVs(filter.sharedMesh, 11, 13);
                meshObject.transform.rotation = Quaternion.Euler(0, 0, 90);
            }

            c++;
        }

        // And one more special case between sidebarmiddle and right corner...
        {
            GameObject meshObject = new GameObject("MESH_" + c);
            meshObject.transform.SetParent(walls.transform);
            MeshRenderer rnd = meshObject.AddComponent<MeshRenderer>();
            MeshFilter filter = meshObject.AddComponent<MeshFilter>();

            meshObject.transform.position = new Vector3(GAME_WIDTH + 2 - 1.25f, h, 1.2f);
            meshObject.transform.SetParent(bk.transform);

            filter.sharedMesh = GameObject.Instantiate(m_flat);
            rnd.material = m_material;
            rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            rnd.receiveShadows = false;
            meshObject.isStatic = true;

            setUVs(filter.sharedMesh, 11, 13);
            meshObject.transform.rotation = Quaternion.Euler(0, 0, 90);
        }

    }

    public int getFreeSideBarSpace()
    {
        int count = 0;
        for (int i = 0; i < GAME_HEIGHT; i++)
            if (m_SideArr[0, i] == null)
                count++;
        return count;
    }

    public void clearSideBar()
    {
        for (int i = 0; i < GAME_HEIGHT; i++)
        {
            if(m_SideArr[0, i] != null)
            {
                ParticleSystem f = GAMEGLOBAL.LVL.getInactiveFader();
                f.startColor = m_SideArr[0, i].m_BlockColor;
                f.gameObject.transform.position = new Vector3(
                m_SideArr[0, i].m_Script.transform.position.x,
                m_SideArr[0, i].m_Script.transform.position.y,
                m_SideArr[0, i].m_Script.transform.position.z);
                f.gameObject.SetActive(true);
                f.Play();
                m_SideArr[0, i] = null;
            }
        }    
    }

    public void StartSideBarTransfer()
    {
        // Activate visual experience
    }

    public void AbortSideBarTransfer()
    {
        // Deactivate visual experience
    }

    public void turnPoofOnOff(bool ON)
    {
        ParticleSystem[] ps = m_Poof.gameObject.GetComponentsInChildren<ParticleSystem>();
        for(int i = 0; i < ps.Length; i++)
        {
            if (ON)
                ps[i].Play();
            else
                ps[i].Stop();
        }
    }

    public void turnSpark1OnOff(bool ON)
    {
        ParticleSystem[] ps = m_Spark1.gameObject.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < ps.Length; i++)
        {
            if (ON)
                ps[i].Play();
            else
                ps[i].Stop();
        }
    }

    public void turnSpark2OnOff(bool ON)
    {
        ParticleSystem[] ps = m_Spark2.gameObject.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < ps.Length; i++)
        {
            if (ON)
                ps[i].Play();
            else
                ps[i].Stop();
        }
    }

    public int CompleteSideBarTransfer(int fromSideBarY, int toX, int toY)
    {
        int result = 1;

        // Trying to place "swap block" ?
        if (m_SideArr[0, fromSideBarY].m_Type == Block.TYPE.SWAP)
        {
            // Activate swapblock
            ((SwapBlock)m_SideArr[0, fromSideBarY]).activate(m_Arr[toX, toY]);

            result = 2;
        }

        // Turn off "poof"
        turnPoofOnOff(false);

        // Physically move sidebar object to new location
        m_SideArr[0, fromSideBarY].m_Script.transform.position = new Vector3(m_Arr[toX, toY].m_Script.transform.position.x, m_Arr[toX, toY].m_Script.transform.position.y, 0);

        // Destroy or deactivate old object
        if(result == 1)
            destroy(toX, toY, true, true);
        else
            m_Arr[toX, toY].m_Script.gameObject.SetActive(false);

        // Set new reference
        m_Arr[toX, toY] = m_SideArr[0, fromSideBarY];
        m_Arr[toX, toY].m_Script.m_Location = BlockScript.BLOCKLOCATION.GAME;     // No longer sidebar
        m_Arr[toX, toY].arrPosX = toX;
        m_Arr[toX, toY].arrPosY = toY;

        // If bomb, explode instantly
        if (m_Arr[toX, toY].m_Type == Block.TYPE.BOMB)
        {
            ((BombBlock)m_Arr[toX, toY]).m_Count = 0;
            ((BombBlock)m_Arr[toX, toY]).logic();
        }

        // Tetris event
        m_currentLevel.tetrisEvent();

        // Set null in previous position
        m_SideArr[0, fromSideBarY] = null;
        return result;
    }


    public void setLevelBackground()
    {

        for (int i = 0; i < m_BackgroundMeshesBack.Count; i++)
            setUVs(m_BackgroundMeshesBack[i], 0, 16);

    }


    public void setLetterBlockColor(int value)
    {
        for (int i = 0; i < m_Boxes.Count; i++)
        {
            if (m_Boxes[i].m_Type == Block.TYPE.LETTER)
            {
                m_Boxes[i].background_x_coord = value;
                m_Boxes[i].m_BlockColor = getColor(value);
                m_Boxes[i].m_Script.setBackUVs(value, 5);
            }
        }
    }

    public Color getRandomColor(byte r, byte g, byte b)
    {
        return new Color32(r, g, b, 255);
    }

    public Color getColor(int value)
    {
        Color c;
        switch(value)
        {
            case 0:
            case 8: c = m_DampBlue; break; 

            case 1:
            case 9: c = m_LightBrown; break;

            case 2:
            case 10: c = m_Magenta; break;

            case 3:
            case 11: c = m_DarkRed; break; 

            case 4:
            case 12: c = m_Green; break; 

            case 5:
            case 13: c = m_Orange; break; 

            case 6:
            case 14: c = m_Orange; break;

            case 7:
            case 15: c = m_LightBrown; break;  

            default: c = m_White;  break;
        }
        return c;
    }


    public void setUVs(Mesh m, float x, float y)
    {
        // The UVs are apparently upsidedown in the Y-direction, so to get a zero-based coordinate system we do this:
        int reverse = (int)(2048f / 64f) - 1;
        y = reverse - y;

        Vector2[] newUVS = m.uv;

        newUVS[0].x = (x * GAMEGLOBAL.LVL.m_UVOffset);
        newUVS[0].y = (y * GAMEGLOBAL.LVL.m_UVOffset);

        newUVS[1].x = GAMEGLOBAL.LVL.m_UVOffset + (x * GAMEGLOBAL.LVL.m_UVOffset);
        newUVS[1].y = (y * GAMEGLOBAL.LVL.m_UVOffset);

        newUVS[2].x = GAMEGLOBAL.LVL.m_UVOffset + (x * GAMEGLOBAL.LVL.m_UVOffset);
        newUVS[2].y = GAMEGLOBAL.LVL.m_UVOffset + (y * GAMEGLOBAL.LVL.m_UVOffset);

        newUVS[3].x = (x * GAMEGLOBAL.LVL.m_UVOffset);
        newUVS[3].y = GAMEGLOBAL.LVL.m_UVOffset + (y * GAMEGLOBAL.LVL.m_UVOffset);

        m.uv = newUVS;
    }

    public void setUVs(Mesh m, float tileSize_x, float tileSize_y, float textureSize, float textureCoordinate_x, float textureCoordinate_y)
    {
        // To get top-left based coordinates...
        textureCoordinate_y = textureSize - tileSize_y - textureCoordinate_y;

        float offsetX = tileSize_x / textureSize;
        float offsetY = tileSize_y / textureSize;
        float x = textureCoordinate_x / textureSize;
        float y = textureCoordinate_y / textureSize;

        Vector2[] frontUVS = m.uv;

        frontUVS[0].x = x;
        frontUVS[0].y = y;

        frontUVS[1].x = x + offsetX;
        frontUVS[1].y = y;

        frontUVS[2].x = x + offsetX;
        frontUVS[2].y = y + offsetY;

        frontUVS[3].x = x;
        frontUVS[3].y = y + offsetY;

        m.uv = frontUVS;
    }

    public Block getNewBlock(int blocktype)
    {
        // Create the actual box gameobject
        GameObject box = new GameObject("BOX_" + m_IDCount);
        box.transform.SetParent(m_BoxesObject.transform);
        box.transform.position = new Vector3(0, 0, 0);
        box.transform.localScale = new Vector3(1, 1, 0.5f);
        box.transform.rotation = Quaternion.Euler(0, 0, -90);

        // Add default flat mesh 
        MeshRenderer rnd = box.AddComponent<MeshRenderer>();
        MeshFilter m = box.AddComponent<MeshFilter>();
        m.sharedMesh = GameObject.Instantiate(m_flat);
        rnd.material = m_material;
        rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        rnd.receiveShadows = false;

        // Collider
        SphereCollider b1 = box.AddComponent<SphereCollider>();
        b1.radius = 0.25f;

        BoxCollider b2 = box.AddComponent<BoxCollider>();
        b2.size = new Vector3(1, 0.2f, 1);

        // Physics
        Rigidbody r = box.AddComponent<Rigidbody>();
        r.constraints =
            RigidbodyConstraints.FreezePositionX |
            RigidbodyConstraints.FreezePositionZ |
            RigidbodyConstraints.FreezeRotationX |
            RigidbodyConstraints.FreezeRotationZ |
            RigidbodyConstraints.FreezeRotationY;

        // Create background object (that changes color when selecting)
        GameObject boxBackground = new GameObject("BOX_" + m_IDCount + "_BACKGROUND");
        boxBackground.transform.SetParent(box.transform);
        boxBackground.transform.localPosition = new Vector3(0, 0, 0.1f);
        boxBackground.transform.rotation = Quaternion.Euler(0, 0, -90);
        MeshRenderer rnd1 = boxBackground.AddComponent<MeshRenderer>();
        MeshFilter m1 = boxBackground.AddComponent<MeshFilter>();
        m1.sharedMesh = GameObject.Instantiate(m_flat);
        rnd1.material = m_material;
        rnd1.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        rnd1.receiveShadows = false;

        // Add blockscript
        BlockScript blockScript = box.AddComponent<BlockScript>();
        blockScript.init(m.sharedMesh, m1.sharedMesh);

        // Create correct blocktype
        Block b;
        switch(blocktype)
        {
            case Block.TYPE.LETTER: b = new LetterBlock(m_IDCount); break;
            case Block.TYPE.STONE: b = new StoneBlock(m_IDCount); break;
            case Block.TYPE.ANY: b = new AnyBlock(m_IDCount); break;
            case Block.TYPE.BOMB: b = new BombBlock(m_IDCount); break;
            case Block.TYPE.SWITCH: b = new SwitcherBlock(m_IDCount); break;
            case Block.TYPE.GOLD: b = new GoldBlock(m_IDCount); break;
            case Block.TYPE.STAR: b = new StarBlock(m_IDCount); break;
            case Block.TYPE.SWAP: b = new SwapBlock(m_IDCount); break;
            default: b = new LetterBlock(m_IDCount); Debug.Log("WARNING: Default Block Created in getNewBlock()"); break;
        }

        // Link Blockscript to Block and back
        b.m_Script = blockScript;
        b.m_Script.m_Block = b;

        // Init, usually set default UVS
        b.init();

        // Dectivate the box
        box.SetActive(false);

        // ID++
        m_IDCount++;

        // Return the block
        return b;
    }

    // This returns letters in a "frequency" that is "most common" in english.
   public int getRandomFrequency()
    {
        int res = Random.Range(0, 40);

        if (res <= 4)
        {
            res = Random.Range(0, 6);
            switch (res)
            {
                case 0: return 9;
                case 1: return 10;
                case 2: return 16;
                case 3: return 21;
                case 4: return 23;
                case 5: return 25;
            }
        }
        else if (res <= 8)
        {
            res = Random.Range(0, 9);
            switch (res)
            {
                case 0: return 2;
                case 1: return 5;
                case 2: return 6;
                case 3: return 12;
                case 4: return 20;
                case 5: return 22;
                case 6: return 15;
                case 7: return 1;
                case 8: return 24;

            }
        }
        else if (res <= 28)
        {
            res = Random.Range(0, 8);
            switch (res)
            {
                case 0: return 7;
                case 1: return 8;
                case 2: return 13;
                case 3: return 14;
                case 4: return 17;
                case 5: return 18;
                case 6: return 11;
                case 7: return 3;
            }
        }
        else
        {
            res = Random.Range(0, 4);
            switch (res)
            {
                case 0: return 0;
                case 1: return 4;
                case 2: return 19;
            }
        }

        return 0;
    }

    public Block getBlock(int blocktype)
    {
        // Iterate over all boxes/blocks and try to find an existing inactive one to reuse
        for (int i = 0; i < m_Boxes.Count; i++)
        {
            if (m_Boxes[i].m_Type == blocktype && !m_Boxes[i].m_Script.gameObject.activeSelf)
                return m_Boxes[i];
        }

        // Found nothing, create new
        Block newBlock = getNewBlock(blocktype);
        m_Boxes.Add(newBlock);
        return newBlock;
    }

    public ParticleSystem getInactiveFader()
    {
        for (int i = 0; i < m_Faders.Count; i++)
        {
            if (!m_Faders[i].gameObject.activeSelf)
                return m_Faders[i];
        }
        return null;
    }

    public ParticleSystem getBombExplosion()
    {
        for (int i = 0; i < m_BombExplosions.Count; i++)
        {
            if (!m_BombExplosions[i].gameObject.activeSelf)
                return m_BombExplosions[i];
        }
        return getNewBombExplosion();
    }
    
    private ParticleSystem getNewBombExplosion()
    {
        GameObject o = (GameObject)Resources.Load("Prefabs/ExplosionFab");
        o = GameObject.Instantiate(o);
        o.transform.SetParent(m_ParticleSystemsObject.transform);
        o.name = "BOMBEXP";
        o.gameObject.SetActive(false);
        ParticleSystem s = o.GetComponent<ParticleSystem>();
        m_BombExplosions.Add(s);
        return s;
    }

    public char getCharacter(int val)
    {
        switch (val)
        {
            case 0: return 'A';
            case 1: return 'B';
            case 2: return 'C';
            case 3: return 'D';
            case 4: return 'E';
            case 5: return 'F';
            case 6: return 'G';
            case 7: return 'H';
            case 8: return 'I';
            case 9: return 'J';
            case 10: return 'K';
            case 11: return 'L';
            case 12: return 'M';
            case 13: return 'N';
            case 14: return 'O';
            case 15: return 'P';
            case 16: return 'Q';
            case 17: return 'R';
            case 18: return 'S';
            case 19: return 'T';
            case 20: return 'U';
            case 21: return 'V';
            case 22: return 'W';
            case 23: return 'X';
            case 24: return 'Y';
            case 25: return 'Z';
            default: return ' ';
        }
    }

    public int getCharacterValue(char character)
    {
        switch (character)
        {
            case 'A': return 0;
            case 'B': return 1;
            case 'C': return 2;
            case 'D': return 3;
            case 'E': return 4;
            case 'F': return 5;
            case 'G': return 6;
            case 'H': return 7;
            case 'I': return 8;
            case 'J': return 9;
            case 'K': return 10;
            case 'L': return 11;
            case 'M': return 12;
            case 'N': return 13;
            case 'O': return 14;
            case 'P': return 15;
            case 'Q': return 16;
            case 'R': return 17;
            case 'S': return 18;
            case 'T': return 19;
            case 'U': return 20;
            case 'V': return 21;
            case 'W': return 22;
            case 'X': return 23;
            case 'Y': return 24;
            case 'Z': return 25;
            case ' ': return 26;
            case '.': return 26;
            case ',': return 27;
            case ':': return 28;
            case '(': return 29;
            case ')': return 30;
            case '+': return 31;
            case '_': return 32;
            case '!': return 33;
            case '?': return 34;
            case '/': return 35;
            case '0': return 36;
            case '1': return 37;
            case '2': return 38;
            case '3': return 39;
            case '4': return 40;
            case '5': return 41;
            case '6': return 42;
            case '7': return 43;
            case '8': return 44;
            case '9': return 45;
            case '$': return 46;
            default: return -1;
        }
    }

    public void destroy(int x, int y, bool explosion, bool destroyImmortal)
    {
        // If within bounds and not null...
        if(x >= 0 && x < GAMEGLOBAL.LVL.GAME_WIDTH && y >= 0 && y < GAMEGLOBAL.LVL.GAME_HEIGHT && GAMEGLOBAL.LVL.m_Arr[x, y] != null)
        {
            // Trying to destroy immortal block?
            if (!destroyImmortal && !GAMEGLOBAL.LVL.m_Arr[x, y].m_Destroyable)
                return;

            // Deactivate
            GAMEGLOBAL.LVL.m_Arr[x, y].m_Script.gameObject.SetActive(false);

            // Start particle system
            if(explosion)
            {
                ParticleSystem f = GAMEGLOBAL.LVL.getInactiveFader();
                f.startColor = GAMEGLOBAL.LVL.m_Arr[x, y].m_BlockColor;
                f.gameObject.transform.position = new Vector3(
                GAMEGLOBAL.LVL.m_Arr[x, y].m_Script.transform.position.x,
                GAMEGLOBAL.LVL.m_Arr[x, y].m_Script.transform.position.y,
                GAMEGLOBAL.LVL.m_Arr[x, y].m_Script.transform.position.z);
                f.gameObject.SetActive(true);
                f.Play();
            }

            // Mark as removed
            GAMEGLOBAL.LVL.m_Arr[x, y] = null;   
        }
    }

    public Level getLevel(int lvl)
    {
        switch (lvl)
        {
            case 1: return new level_1(lvl);
            case 2: return new level_2(lvl);
            case 3: return new level_3(lvl);
            case 4: return new level_4(lvl);
            case 5: return new level_5(lvl);
            case 6: return new level_6(lvl);
            case 7: return new level_7(lvl);
            case 8: return new level_8(lvl);
            case 9: return new level_9(lvl);
            case 10: return new level_10(lvl);
        }
        return new level_RAND(lvl); // Once levels are completed, a random level is generated...
    }

    public List<TEXTURECHAR> getCharacters(string txt)
    {
        List<TEXTURECHAR> t = new List<TEXTURECHAR>();


        for (int i = 0; i < txt.Length; i++)
        {
            if(txt[i] == '<')
            {
                // Get color value <C?
                int color = (int)System.Char.GetNumericValue(txt[i + 2]);

                // Get the end of colored text
                int endPos = txt.IndexOf("<", i + 1);
                if (endPos == -1)
                    throw new System.Exception("Did not find '<' ending character in getCharacters().");

                for (int j = i + 4; j < endPos; j++)
                    t.Add(getCharacter(txt[j], color));
                i = endPos + 3;
            }
            else
                t.Add(getCharacter(txt[i], 0));
        }
            
        return t;
    }

    public TEXTURECHAR getCharacter(char character, int color)
    {
        TEXTURECHAR t = new TEXTURECHAR();

        t.color = color * 80;

        t.uvy = 1552;
        t.uvx = getCharacterValue(char.ToUpper(character)) * 40;

        if (t.uvx < 0)
        {
            t.color = 0;
            t.uvx = 34 * 40;
            t.uvy = 1552;
            t.width = 17;
            return t;
        }
        
        switch (character)
        {
            case 'A': t.width = 26; t.uvy += 40; break;
            case 'B': t.width = 23; t.uvy += 40; break;
            case 'C': t.width = 22; t.uvy += 40; break;
            case 'D': t.width = 25; t.uvy += 40; break;
            case 'E': t.width = 17; t.uvy += 40; break;
            case 'F': t.width = 17; t.uvy += 40; break;
            case 'G': t.width = 24; t.uvy += 40; break;
            case 'H': t.width = 25; t.uvy += 40; break;
            case 'I': t.width = 11; t.uvy += 40; break;
            case 'J': t.width = 19; t.uvy += 40; break;
            case 'K': t.width = 25; t.uvy += 40; break;
            case 'L': t.width = 19; t.uvy += 40; break;
            case 'M': t.width = 32; t.uvy += 40; break;
            case 'N': t.width = 26; t.uvy += 40; break;
            case 'O': t.width = 26; t.uvy += 40; break;
            case 'P': t.width = 22; t.uvy += 40; break;
            case 'Q': t.width = 29; t.uvy += 40; break;
            case 'R': t.width = 21; t.uvy += 40; break;
            case 'S': t.width = 19; t.uvy += 40; break;
            case 'T': t.width = 22; t.uvy += 40; break;
            case 'U': t.width = 24; t.uvy += 40; break;
            case 'V': t.width = 24; t.uvy += 40; break;
            case 'W': t.width = 35; t.uvy += 40; break;
            case 'X': t.width = 23; t.uvy += 40; break;
            case 'Y': t.width = 23; t.uvy += 40; break;
            case 'Z': t.width = 23; t.uvy += 40; break;
            case ' ': t.width = 15; t.uvx = 1700; t.uvy = 40; break;
            case 'a': t.width = 19; break;
            case 'b': t.width = 22; break;
            case 'c': t.width = 18; break;
            case 'd': t.width = 22; break;
            case 'e': t.width = 20; break;
            case 'f': t.width = 18; break;
            case 'g': t.width = 22; break;
            case 'h': t.width = 21; break;
            case 'i': t.width = 11; break;
            case 'j': t.width = 14; break;
            case 'k': t.width = 20; break;
            case 'l': t.width = 11; break;
            case 'm': t.width = 31; break;
            case 'n': t.width = 21; break;
            case 'o': t.width = 23; break;
            case 'p': t.width = 22; break;
            case 'q': t.width = 22; break;
            case 'r': t.width = 16; break;
            case 's': t.width = 16; break;
            case 't': t.width = 16; break;
            case 'u': t.width = 22; break;
            case 'v': t.width = 21; break;
            case 'w': t.width = 29; break;
            case 'x': t.width = 22; break;
            case 'y': t.width = 21; break;
            case 'z': t.width = 10; break;
            case '.': t.width = 10; break;
            case ',': t.width = 10; break;
            case ':': t.width = 10; break;
            case '(': t.width = 13; break;
            case ')': t.width = 13; break;
            case '+': t.width = 19; break;
            case '_': t.width = 25; break;
            case '!': t.width = 11; break;
            case '?': t.width = 17; break;
            case '/': t.width = 19; break;
            case '0': t.width = 22; break;
            case '1': t.width = 17; break;
            case '2': t.width = 19; break;
            case '3': t.width = 19; break;
            case '4': t.width = 22; break;
            case '5': t.width = 20; break;
            case '6': t.width = 21; break;
            case '7': t.width = 21; break;
            case '8': t.width = 21; break;
            case '9': t.width = 21; break;
            case '$': t.width = 37; t.uvy = 1555; break;
            default: t.width = 10; break;
        }
        return t;
    }

    public void SavePlayerData(int coins, int level)
    {
        m_PlayerData.m_coins = coins;
        m_PlayerData.m_progress = level;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerData.dat");
        bf.Serialize(file, m_PlayerData);
        file.Close();
    }

    public PlayerData LoadPlayerData()
    {
        m_PlayerData.m_coins = 0;
        m_PlayerData.m_progress = 1;

        if (File.Exists(Application.persistentDataPath + "/playerData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerData.dat", FileMode.Open);
            m_PlayerData = (PlayerData)bf.Deserialize(file);
            file.Close();
            Debug.Log("LoadPlayerData: Loaded file with data: Level: " + m_PlayerData.m_progress + " Coins: " + m_PlayerData.m_coins);
        }
        else
            Debug.Log("LoadPlayerData: No file existed.");

        return m_PlayerData;
    }

}


public class LEVEL_STATUS
{
    public const int MAINMENU = -2;
    public const int NOTSTARTED = -1;
    public const int RUNNING = 0;
    public const int FAILED = 1;
    public const int PAUSED = 2;
    public const int SUCCESS = 3;
    public const int WAITING = 4;
    public const int BUYMENU = 5;
    public const int SETTINGSMENU = 6;
    public const int DAILYQUESTMENU = 7;
    public const int NEWBLOCKVISUAL = 8;
    public const int BUYBLOCKMENU = 9;
}

public class CAMERA_MOVEMENT
{
    public const int BEGIN = 0;                 // -> 
    public const int EXIT_FROM_LEVEL = 1;       // <-
    public const int LEVEL_PROGRESSION = 2;     // -> <-- ->
}


public class BOOLSTR
{
    public BOOLSTR(string str, bool b)
    {
        m_String = str;
        m_Bool = b;
    }
    public string m_String;
    public bool m_Bool;
}

public class TEXTURECHAR
{
    public float uvx = 0;
    public float uvy = 0;
    public float width = 0;
    public int color = 0;   // Offset för Y-led, ökas med 40 för varje "rad" så om texten är grön 2 rader ner, +80 då.
}

public class ADTYPE
{
    public const int UNITY_REWARD = 0;
    public const int GOOGLE_REWARD = 1;
    public const int GOOGLE_SKIPPABLE = 2;
    
}

[System.Serializable]
public class PlayerData
{
    public int m_coins;
    public int m_progress;
}