﻿using System.Collections;

public class StarBlock : Block
{
    public bool m_Good;

    public StarBlock(int id) : base(id)
    {
        m_Type = TYPE.STAR;
        m_Good = true;
        m_Selectable = false;
        m_Destroyable = false;
        background_x_coord = 5;
    }

    // Good or evil star?
    public void setGoodBad(bool good)
    {
        m_Good = good;
        if(good)
        {
            background_y_coord = 8;
        }
        else
        {
            background_y_coord = 9;
        }
        m_Script.setFrontUVs(background_x_coord, background_y_coord);
        m_Script.setBackUVs(29, 0);
    }

}