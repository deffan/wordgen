﻿using System.Collections;

public class LetterBlock : Block
{
    public LetterBlock(int id) : base(id)
    {
        m_Type = TYPE.LETTER;
    }

    public override void init()
    {
        background_y_coord = 5;
    }

    public override void select()
    {
        base.select();
        m_Script.setBackUVs(background_x_coord, background_y_coord + 1);
    }

    public override void deselect()
    {
        base.deselect();
        m_Script.setBackUVs(background_x_coord, background_y_coord);
    }
}
