﻿using System.Collections;

public class AnyBlock : Block
{
    public AnyBlock(int id) : base(id)
    {
        m_Type = TYPE.ANY;
        m_LetterValue = -1;
        m_LetterChar = '_';     // the underscore is wildcard for "any" in SQLite
    }

    public override void init()
    {
        background_x_coord = 1;
        m_Script.setBackUVs(29, 0);    // Invisible/Transparent tile
        m_Script.setFrontUVs(background_x_coord, 8);

    }

    public override void select()
    {
        base.select();
        m_Script.setFrontUVs(background_x_coord, 9);
    }

    public override void deselect()
    {
        base.deselect();
        m_Script.setFrontUVs(background_x_coord, 8);
    }
}
