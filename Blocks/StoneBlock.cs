﻿using System.Collections;

public class StoneBlock : Block
{
    public StoneBlock(int id) : base(id)
    {
        m_Type = TYPE.STONE;
        m_Selectable = false;       
        m_BlockColor = UnityEngine.Color.gray;
    }

    public override void init()
    {
        m_Script.setFrontUVs(0, 8);
    }

    public override void select()
    {
    }

    public override void deselect()
    {
    }
}
