﻿using System.Collections;

public class BombBlock : Block
{
    public int m_Count;

    public BombBlock(int id) : base(id)
    {
        m_Type = TYPE.BOMB;
        m_Count = 0;
        m_Selectable = false;
    }

    public override void init()
    {
        background_y_coord = 8;
        background_x_coord = 2;
        m_Script.setBackUVs(background_x_coord, background_y_coord); 
        m_Script.setFrontUVs(0.7f, 2.9f);
        m_Script.addBombSpark();
    }

    public override void select()
    {
        base.select();
        m_Script.setBackUVs(background_x_coord, background_y_coord + 1);
    }

    public override void deselect()
    {
        base.deselect();
        m_Script.setBackUVs(background_x_coord, background_y_coord);
    }

    public void setBombTime(int time)
    {
        m_Count = time;
        m_Script.setFrontUVs(time + 0.07f, 2.9f);
    }

    public override void logic()
    {
        m_Count--;
        if (m_Count <= 0)
        {
            // Explosion animation
            UnityEngine.ParticleSystem f = GAMEGLOBAL.LVL.getBombExplosion();
            f.gameObject.transform.position = new UnityEngine.Vector3(
            GAMEGLOBAL.LVL.m_Arr[arrPosX, arrPosY].m_Script.transform.position.x,
            GAMEGLOBAL.LVL.m_Arr[arrPosX, arrPosY].m_Script.transform.position.y,
            -2);
            f.gameObject.SetActive(true);
            f.Play();
        
            // Explode, destroying itself and everything around
            explode();
        }
        else
            m_Script.nextNumber();
    }

    public void explode()
    {
        // Remove bomb itself 
        m_Script.gameObject.SetActive(false);
        GAMEGLOBAL.LVL.m_Arr[arrPosX, arrPosY] = null;

        // Try to destroy all blocks around the bomb
        GAMEGLOBAL.LVL.destroy(arrPosX + 1, arrPosY, false, false);
        GAMEGLOBAL.LVL.destroy(arrPosX, arrPosY + 1, false, false);
        GAMEGLOBAL.LVL.destroy(arrPosX + 1, arrPosY + 1, false, false);
        GAMEGLOBAL.LVL.destroy(arrPosX - 1, arrPosY, false, false);
        GAMEGLOBAL.LVL.destroy(arrPosX, arrPosY - 1, false, false);
        GAMEGLOBAL.LVL.destroy(arrPosX - 1, arrPosY - 1, false, false);
        GAMEGLOBAL.LVL.destroy(arrPosX + 1, arrPosY - 1, false, false);
        GAMEGLOBAL.LVL.destroy(arrPosX - 1, arrPosY + 1, false, false);
    }
}
