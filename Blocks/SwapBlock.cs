﻿using System.Collections;

public class SwapBlock : Block
{
    public bool m_Activated;
    public Block m_Swapped;

    public SwapBlock(int id) : base(id)
    {
        m_Type = TYPE.SWAP;
        m_Activated = false;
    }

    public override void init()
    {
        m_Activated = false;
        m_Swapped = null;
        background_y_coord = 5;
        background_x_coord = 17;

        m_Script.setFrontUVs(28, 0);
        m_Script.setBackUVs(17, 5);
    }

    public void activate(Block swapped)
    {
        m_Activated = true;
        m_Swapped = swapped;

        if (swapped.m_Type == TYPE.LETTER || swapped.m_Type == TYPE.SWITCH || swapped.m_Type == TYPE.GOLD)
        {
            m_LetterValue = swapped.m_LetterValue;
            m_LetterChar = swapped.m_LetterChar;
            m_Script.setFrontUVs(m_LetterValue, 0);
        }
        else if(swapped.m_Type == TYPE.ANY)
        {
            m_Script.setFrontUVs(1, 8);
        }
        else if (swapped.m_Type == TYPE.BOMB)
        {
            m_Script.setFrontUVs(17, 5);
            m_Script.setBackUVs(2, 8);
        }
        else if (swapped.m_Type == TYPE.STONE)
        {
            m_Script.setBackUVs(0, 8);
            m_Script.setFrontUVs(17, 5);
        }
    }

    public override void select()
    {
        base.select();
        if (m_Activated)
        {
            m_Script.setBackUVs(background_x_coord, background_y_coord + 1);
        }
    }

    public override void deselect()
    {
        base.deselect();
        if (m_Activated)
        {
            m_Script.setBackUVs(background_x_coord, background_y_coord);
        }
    }
}
