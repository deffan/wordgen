﻿using System.Collections;

public class NullBlock : Block
{
    public NullBlock(int id) : base(id)
    {
        m_Type = TYPE.NULL;
        m_Selectable = false;
        m_Gravity = false;
        m_Destroyable = false;
    }

    public override void init()
    {
        // Invisible block, used in SideBar only to get things working the way its designed...
        m_Script.setFrontUVs(28, 0);
        m_Script.setBackUVs(28, 0);
    }

    public override void select()
    {
    }

    public override void deselect()
    {
    }
}
