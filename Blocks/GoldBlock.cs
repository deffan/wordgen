﻿using System.Collections;

public class GoldBlock : Block
{
    public GoldBlock(int id) : base(id)
    {
        m_Type = TYPE.GOLD;
    }

    public override void init()
    {
        background_y_coord = 8;
        background_x_coord = 4;
        character_y_coord = 1;

        m_Script.setBackUVs(background_x_coord, background_y_coord);
        int charValue = GAMEGLOBAL.LVL.getRandomFrequency();
        setCharacter(GAMEGLOBAL.LVL.getCharacter(charValue), charValue);

        m_Script.addGoldSpark();
    }

    public override void select()
    {
        base.select();
        m_Script.setBackUVs(background_x_coord, background_y_coord + 1);
    }

    public override void deselect()
    {
        base.deselect();
        m_Script.setBackUVs(background_x_coord, background_y_coord);
    }
}