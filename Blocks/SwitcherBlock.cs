﻿using System.Collections;

public class SwitcherBlock : Block
{
    public SwitcherBlock(int id) : base(id)
    {
        m_Type = TYPE.SWITCH;
    }

    public override void init()
    {
        background_y_coord = 8;
        background_x_coord = 3;
        m_Script.setBackUVs(background_x_coord, background_y_coord);
        int charValue = GAMEGLOBAL.LVL.getRandomFrequency();
        setCharacter(GAMEGLOBAL.LVL.getCharacter(charValue), charValue);
    }

    public override void logic()
    {
        // Move to next char value.
        int oldc = m_LetterValue;

        while(m_LetterValue == oldc)
            m_LetterValue = GAMEGLOBAL.LVL.getRandomFrequency();

        m_LetterChar = GAMEGLOBAL.LVL.getCharacter(m_LetterValue);
        m_Script.nextCharacter(oldc);
    }


    public override void select()
    {
        base.select();
        m_Script.setBackUVs(background_x_coord, background_y_coord + 1);
    }

    public override void deselect()
    {
        base.deselect();
        m_Script.setBackUVs(background_x_coord, background_y_coord);
    }
}
