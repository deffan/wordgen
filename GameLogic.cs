﻿using UnityEngine;
using System.Collections;
using System;

public static class GAMEGLOBAL
{
    public static LevelGlobal LVL;
    public static DateTime m_AntiPauseSpam;
    public static int m_Version = 1;
}

public class GameLogic : MonoBehaviour
{
    private MenuLogic m_Menu;
    
    
	void Start ()
    {
        GAMEGLOBAL.LVL = new LevelGlobal();
        GAMEGLOBAL.LVL.m_Behaviour = GameObject.Find("Logic").GetComponent<LevelBehaviour>();
        GAMEGLOBAL.LVL.Init();

        // Get player data
        PlayerData playerData = GAMEGLOBAL.LVL.LoadPlayerData();
        GAMEGLOBAL.LVL.m_Coins = playerData.m_coins;
        GAMEGLOBAL.LVL.m_currentLevel = GAMEGLOBAL.LVL.getLevel(playerData.m_progress);
        GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = LEVEL_STATUS.MAINMENU;

        // Check for update
        checkUpdate();

        GAMEGLOBAL.m_AntiPauseSpam = DateTime.Now;
        m_Menu = GameObject.Find("Menu").GetComponent<MenuLogic>();
        m_Menu.init();
    }

    private void checkUpdate()
    {
        // Compare compiled game-version with database version.
        // Update database structure if required and set DBversion = gameversion.

        // GAMEGLOBAL.LVL.m_DB.insertWords();
        //  GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/colors.txt", "colors");
        //  GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/animals.txt", "animals");
        //  GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/fruits.txt", "fruits");
        //  GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/countries.txt", "countries");
        //  GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/capitals.txt", "capitals");
        //  GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/solarsystem.txt", "solarsystem");
        //  GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/elements.txt", "elements");
      //  GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/vegetables.txt", "vegetables");
       // GAMEGLOBAL.LVL.m_DB.insertCategory(Application.dataPath + "/lists/sports.txt", "sports");
    }
	
    void OnApplicationQuit()
    {
        GAMEGLOBAL.LVL.Dispose();
    }

    void Update ()
    {
        if(GAMEGLOBAL.LVL.m_ShutDownGame)
        {
            Application.Quit();
            return;
        }

        // Waiting
        if (GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus == LEVEL_STATUS.WAITING)
            return;

        // Toggle pause
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus == LEVEL_STATUS.RUNNING && !m_Menu.m_Active && GAMEGLOBAL.m_AntiPauseSpam < DateTime.Now)
            {
                GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = LEVEL_STATUS.PAUSED;
                m_Menu.m_ToggleEscKey = true;
            }
        }

        // Playing a level
        if (GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus == LEVEL_STATUS.RUNNING)
        {
            GAMEGLOBAL.LVL.m_currentLevel.Run();
        }
        else if(GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus == LEVEL_STATUS.NOTSTARTED)
        {
            for (int i = 0; i < GAMEGLOBAL.LVL.m_Objectives.Count; i++)
            {
                GAMEGLOBAL.LVL.m_Objectives[i].transform.gameObject.SetActive(false);
            }

            GAMEGLOBAL.LVL.m_currentLevel.Begin_1();
            GAMEGLOBAL.LVL.m_currentLevel.Begin_2();
            GAMEGLOBAL.LVL.m_currentLevel.Begin_3();
            GAMEGLOBAL.LVL.updateHints();
            GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = LEVEL_STATUS.RUNNING;
        }
        else
        {
            // Menu logic
            m_Menu.Run();
        }
	}
}
