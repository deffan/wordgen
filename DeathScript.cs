﻿using UnityEngine;
using System.Collections;

public class DeathScript : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        other.gameObject.SetActive(false);
    }

}
