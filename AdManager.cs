﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;
//using GoogleMobileAds.Api;

public class AdManager : MonoBehaviour
{
    private string gameID = "1186379";
 //   private string googleID = "1186379";
 //   private string rewardedGoogleID = "1186379";
 //   private InterstitialAd googleAd;
  //  private RewardBasedVideoAd rewardedGoogleAd;
    private bool m_EditorTest;

    void Start ()
    {

        m_EditorTest = true;


        if (m_EditorTest)
        {
            Advertisement.Initialize(gameID, m_EditorTest);
       //     googleAd = new InterstitialAd("ca-app-pub-3940256099942544/1033173712");
        }
        else
        {
            Advertisement.Initialize(gameID, false);
        //    googleAd = new InterstitialAd(googleID);
        }

   //     rewardedGoogleAd = RewardBasedVideoAd.Instance;
  //      rewardedGoogleAd.OnAdRewarded += HandleRewardBasedVideoRewarded;

    //    newRewardedGoogleAdRequest(m_EditorTest);
        //newGoogleAdRequest(m_EditorTest);
    }

    public void ShowAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = AdCallbackhandler;
            Advertisement.Show("rewardedVideo", options);
        }  
    }

    // public void ShowBannerAd()
    // {
    //     BannerView bannerView = new BannerView("ca-app-pub-3940256099942544/6300978111", AdSize.Banner, AdPosition.Bottom);
    //     AdRequest request = new AdRequest.Builder().Build();
    //    bannerView.LoadAd(request);
    //  }

    //  public void ShowGoogleAd()
    // {
    //  if (googleAd.IsLoaded())
    //    {
    //   googleAd.Show();
    //   rewardPlayer();
    //    }
    //     newGoogleAdRequest(m_EditorTest);
    //  }

    /*
public void ShowRewardedGoogleAd()
{
    if (rewardedGoogleAd.IsLoaded())
    {
        rewardedGoogleAd.Show();
    }
    newRewardedGoogleAdRequest(m_EditorTest);
}
*/
    private void rewardPlayer()
    {
        Debug.Log("Ad Finished. Rewarding player...");
        if (GAMEGLOBAL.LVL.m_WatchedQuest == null)
        {
            GAMEGLOBAL.LVL.m_Coins += 50;
            GAMEGLOBAL.LVL.m_Behaviour.doREWARDText(0, 0, 50);
        }
        else
        {
            GAMEGLOBAL.LVL.m_WatchedQuest.m_Quest.setDone();
            GAMEGLOBAL.LVL.m_Coins += GAMEGLOBAL.LVL.m_WatchedQuest.m_Quest.m_CoinReward;
            GAMEGLOBAL.LVL.m_Behaviour.doREWARDText(0, 0, GAMEGLOBAL.LVL.m_WatchedQuest.m_Quest.m_CoinReward);
        }
        GAMEGLOBAL.LVL.SavePlayerData(GAMEGLOBAL.LVL.m_Coins, GAMEGLOBAL.LVL.m_currentLevel.m_Level);
    }

    void AdCallbackhandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                rewardPlayer();
                break;
            case ShowResult.Failed:
                Debug.Log("SHOWING AD FAILED");
                break;
        }
    }

    /*
    void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        rewardPlayer();
    }


    
    public void newGoogleAdRequest(bool test)
    {
        AdRequest request = null;
        if (test)
        {
            request = new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
            .AddTestDevice("2077ef9a63d2b398840261c8221a0c9b")  // My test device.
            .Build();
        }
        else
        {
            request = new AdRequest.Builder().Build();
        }

        // Load the interstitial with the request.
     //   googleAd.LoadAd(request);
    }

    public void newRewardedGoogleAdRequest(bool test)
    {
        AdRequest request = null;
        if (test)
        {
            request = new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
            .AddTestDevice("2077ef9a63d2b398840261c8221a0c9b")  // My test device.
            .Build();
            rewardedGoogleAd.LoadAd(request, "ca-app-pub-3940256099942544/1033173712");
        }
        else
        {
            request = new AdRequest.Builder().Build();
            rewardedGoogleAd.LoadAd(request, rewardedGoogleID);
        }
    }
    */
}
