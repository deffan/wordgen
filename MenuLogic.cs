﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System;

public class MenuLogic : MonoBehaviour
{
    public bool m_Active;
    private bool m_Initiated;
    public bool m_ToggleEscKey;
    private int m_MenuStatus;
    private int m_PrevMenuStatus;
    private RaycastHit m_Hit;
    private GameObject m_MenuBackground;
    private GameObject m_MenuBackHeaderText;
    private GameObject m_ButtonsObject;
    private GameObject m_DailyQuestObject;
    private GameObject m_BuyBlocksObject;
    private GameObject m_arrowBouncer;
    private GameObject m_newBlock;
    private List<dailyQuestScript> m_Dailys;
    private List<Block> m_BuyList;
    private ButtonScript m_DailyQuestExitButton;
    private ButtonScript m_BuyButton;
    private ButtonScript m_BuyExitButton;
    private Block m_SelectedBuyBlock;
    private textScript m_BuyTextInfo;
    private DateTime m_AntiSpamBuy;

    // Buttons
    private List<ButtonScript> m_Buttons;

    public void Start()
    {
        m_PrevMenuStatus = 0;
        m_MenuStatus = 0;
        m_ToggleEscKey = false;
        m_Active = false;
        m_ButtonsObject = transform.FindChild("MenuButtons").gameObject;
        m_MenuBackground = transform.FindChild("MenuBackground").gameObject;
        m_MenuBackHeaderText = transform.FindChild("MenuHeaderText").gameObject;
        m_DailyQuestObject = transform.FindChild("DailyQuestMenu").gameObject;
        m_BuyBlocksObject = transform.FindChild("BuyBlocksMenu").gameObject;
        m_arrowBouncer = m_ButtonsObject.transform.FindChild("ArrowBouncer").gameObject;
        m_newBlock = transform.FindChild("NewBlock").gameObject;
        m_Buttons = new List<ButtonScript>();
        m_Dailys = new List<dailyQuestScript>();
        m_BuyList = new List<Block>();
        m_AntiSpamBuy = DateTime.Now;
    }

    private void initiate()
    {
        // Buttons
        float btpPos = 0;
        for(int i = 0; i < 4; i++)
        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/ButtonFab");
            Material m_material = temp.GetComponent<MeshRenderer>().sharedMaterial;
            Mesh m_flat = temp.GetComponent<MeshFilter>().sharedMesh;

            GameObject box = new GameObject("MENUBUTTON");
            box.transform.SetParent(m_ButtonsObject.transform);
            box.transform.localPosition = new Vector3(0, btpPos, -7.5f);
            box.transform.localScale = new Vector3(0.78125f, 3.515625f, 1);
            box.transform.rotation = Quaternion.Euler(0, 0, -90);

            // Add default flat mesh 
            MeshRenderer rnd = box.AddComponent<MeshRenderer>();
            MeshFilter m = box.AddComponent<MeshFilter>();
            m.sharedMesh = GameObject.Instantiate(m_flat);
            rnd.material = m_material;
            rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            rnd.receiveShadows = false;

            BoxCollider b2 = box.AddComponent<BoxCollider>();
            b2.isTrigger = true;

            ButtonScript bs = box.AddComponent<ButtonScript>();
            bs.init(m.sharedMesh, 0);
            btpPos -= 0.85f;

            box.SetActive(false);
            m_Buttons.Add(bs);
        }

        // Exit button in DailyQuest menu
        // But... should probably make a function that returns a button instead...
        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/ButtonFab");
            Material m_material = temp.GetComponent<MeshRenderer>().sharedMaterial;
            Mesh m_flat = temp.GetComponent<MeshFilter>().sharedMesh;

            GameObject box = new GameObject("DQEXITBUTTON");
            box.transform.SetParent(m_DailyQuestObject.transform);
            box.transform.localPosition = new Vector3(0, -1.5f, 0);
            box.transform.localScale = new Vector3(0.78125f, 3.515625f, 1);
            box.transform.rotation = Quaternion.Euler(0, 0, -90);

            // Add default flat mesh 
            MeshRenderer rnd = box.AddComponent<MeshRenderer>();
            MeshFilter m = box.AddComponent<MeshFilter>();
            m.sharedMesh = GameObject.Instantiate(m_flat);
            rnd.material = m_material;
            rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            rnd.receiveShadows = false;

            BoxCollider b2 = box.AddComponent<BoxCollider>();
            b2.isTrigger = true;

            ButtonScript bs = box.AddComponent<ButtonScript>();
            bs.init(m.sharedMesh, ButtonScript.TYPE.EXIT);
            m_DailyQuestExitButton = bs;
            box.SetActive(false);
        }

        m_newBlock.gameObject.SetActive(false);

        // Arrow Bouncer UVs
        GAMEGLOBAL.LVL.setUVs(m_arrowBouncer.gameObject.GetComponent<MeshFilter>().mesh, 161, 35, 2048, 329, 982);
        m_arrowBouncer.gameObject.transform.localPosition = new Vector3(3f, -0.85f, m_Buttons[1].transform.position.z);
        m_arrowBouncer.gameObject.SetActive(false);

        // Daily quest
        GAMEGLOBAL.LVL.setUVs(m_DailyQuestObject.transform.GetChild(0).GetComponent<MeshFilter>().mesh, 614, 68, 2048, 1410, 974);
        m_DailyQuestObject.transform.GetChild(2);   // QuestTable.

        // Add textscript
        textScript header = m_DailyQuestObject.transform.GetChild(2).GetChild(0).gameObject.AddComponent<textScript>();
        header.init(0.4f);
        header.setText("<c1>Quest</c>");

        // Add textscript
        textScript header2 = m_DailyQuestObject.transform.GetChild(2).GetChild(1).gameObject.AddComponent<textScript>();
        header2.init(0.4f);
        header2.setText("<c1>Reward</c>");

        // Add Daily Quest rows
        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/questFab");
            temp = GameObject.Instantiate(temp);
            temp.transform.SetParent(m_DailyQuestObject.transform.GetChild(2));
            temp.transform.localPosition = new Vector3(0, 3, 0);
            dailyQuestScript d = temp.GetComponent<dailyQuestScript>();
            d.init();
            d.setQuest(ADTYPE.UNITY_REWARD, 1, "Watch Video <c1>1</c>", "50x$", 50);

            m_Dailys.Add(d);
        }

        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/questFab");
            temp = GameObject.Instantiate(temp);
            temp.transform.SetParent(m_DailyQuestObject.transform.GetChild(2));
            temp.transform.localPosition = new Vector3(0, 2.45f, 0);
            dailyQuestScript d = temp.GetComponent<dailyQuestScript>();
            d.init();
            d.setQuest(ADTYPE.UNITY_REWARD, 2, "Watch Video <c1>2</c>", "40x$", 40);

            m_Dailys.Add(d);
        }

        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/questFab");
            temp = GameObject.Instantiate(temp);
            temp.transform.SetParent(m_DailyQuestObject.transform.GetChild(2));
            temp.transform.localPosition = new Vector3(0, 1.9f, 0);
            dailyQuestScript d = temp.GetComponent<dailyQuestScript>();
            d.init();
            d.setQuest(ADTYPE.GOOGLE_REWARD, 3, "Watch Video <c1>3</c>", "30x$", 30);

            m_Dailys.Add(d);
        }

        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/questFab");
            temp = GameObject.Instantiate(temp);
            temp.transform.SetParent(m_DailyQuestObject.transform.GetChild(2));
            temp.transform.localPosition = new Vector3(0, 1.35f, 0);
            dailyQuestScript d = temp.GetComponent<dailyQuestScript>();
            d.init();
            d.setQuest(ADTYPE.GOOGLE_REWARD, 4, "Watch Video <c1>4</c>", "20x$", 20);

            m_Dailys.Add(d);
        }

        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/questFab");
            temp = GameObject.Instantiate(temp);
            temp.transform.SetParent(m_DailyQuestObject.transform.GetChild(2));
            temp.transform.localPosition = new Vector3(0, 0.8f, 0);
            dailyQuestScript d = temp.GetComponent<dailyQuestScript>();
            d.init();
            d.setQuest(ADTYPE.GOOGLE_REWARD, 5, "Watch Video <c1>5</c>", "10x$", 10);

            m_Dailys.Add(d);
        }

        m_DailyQuestObject.SetActive(false);


        // Buy Blocks Menu
        GAMEGLOBAL.LVL.setUVs(m_BuyBlocksObject.transform.GetChild(0).GetComponent<MeshFilter>().mesh, 356, 69, 2048, 1677, 759);

        //m_BuyBlocksObject
        Transform table = m_BuyBlocksObject.transform.GetChild(2);   // Table

        float yBlockPos = 2f;
        float xBlockPos = -3f;
        int letterval = 0;
        for(int i = 25; i >= 0; i--)
        {
            
            Block b = GAMEGLOBAL.LVL.getNewBlock(Block.TYPE.LETTER);
            b.setCharacter(GAMEGLOBAL.LVL.getCharacter(letterval), letterval);
            b.setGravity(false);
            b.m_Script.transform.SetParent(table);
            b.m_Script.transform.localPosition = new Vector3(xBlockPos, yBlockPos, 0);
            b.m_Script.m_Location = BlockScript.BLOCKLOCATION.BUYMENU;

            letterval++;
            xBlockPos++;

            b.background_x_coord = 8;
            b.m_Script.setBackUVs(8, 5);

            // new row
            if (xBlockPos == 4)
            {
                xBlockPos = -3f;
                yBlockPos--;
            }
            m_BuyList.Add(b);
            b.m_Script.transform.gameObject.SetActive(true);
        }

        // Buy Button
        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/ButtonFab");
            Material m_material = temp.GetComponent<MeshRenderer>().sharedMaterial;
            Mesh m_flat = temp.GetComponent<MeshFilter>().sharedMesh;

            GameObject box = new GameObject("BUYBUTTON");
            box.transform.SetParent(m_BuyBlocksObject.transform);
            box.transform.localPosition = new Vector3(-1.9f, -2f, 0);
            box.transform.localScale = new Vector3(0.78125f, 3.515625f, 1);
            box.transform.rotation = Quaternion.Euler(0, 0, -90);

            // Add default flat mesh 
            MeshRenderer rnd = box.AddComponent<MeshRenderer>();
            MeshFilter m = box.AddComponent<MeshFilter>();
            m.sharedMesh = GameObject.Instantiate(m_flat);
            rnd.material = m_material;
            rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            rnd.receiveShadows = false;

            BoxCollider b2 = box.AddComponent<BoxCollider>();
            b2.isTrigger = true;

            ButtonScript bs = box.AddComponent<ButtonScript>();
            bs.init(m.sharedMesh, ButtonScript.TYPE.BUY);
            m_BuyButton = bs;
            box.SetActive(false);
        }

        // Exit Button
        {
            GameObject temp = (GameObject)Resources.Load("Prefabs/ButtonFab");
            Material m_material = temp.GetComponent<MeshRenderer>().sharedMaterial;
            Mesh m_flat = temp.GetComponent<MeshFilter>().sharedMesh;

            GameObject box = new GameObject("BUYEXITBUTTON");
            box.transform.SetParent(m_BuyBlocksObject.transform);
            box.transform.localPosition = new Vector3(2, -2f, 0);
            box.transform.localScale = new Vector3(0.78125f, 3.515625f, 1);
            box.transform.rotation = Quaternion.Euler(0, 0, -90);

            // Add default flat mesh 
            MeshRenderer rnd = box.AddComponent<MeshRenderer>();
            MeshFilter m = box.AddComponent<MeshFilter>();
            m.sharedMesh = GameObject.Instantiate(m_flat);
            rnd.material = m_material;
            rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            rnd.receiveShadows = false;

            BoxCollider b2 = box.AddComponent<BoxCollider>();
            b2.isTrigger = true;

            ButtonScript bs = box.AddComponent<ButtonScript>();
            bs.init(m.sharedMesh, ButtonScript.TYPE.EXIT);
            m_BuyExitButton = bs;
            box.SetActive(false);
        }

        // Add textscript
        m_BuyTextInfo = table.GetChild(0).gameObject.AddComponent<textScript>();
        m_BuyTextInfo.init(0.4f);
        m_BuyTextInfo.setText("<c2>Letters are added to sidebar.</c>");
        table.GetChild(0).transform.localPosition = new Vector3(-2, -2.8f, 0);

        m_BuyBlocksObject.SetActive(false);
    }

    public void init()
    {
        initiate();

        mainMenu(); 

        m_MenuStatus = GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus;

        m_Active = true;
        m_ToggleEscKey = true;
    }

    public void mainMenu()
    {
        // Each time "reset" buttons
        for (int i = 0; i < m_Buttons.Count; i++)
            m_Buttons[i].gameObject.SetActive(false);

        GAMEGLOBAL.LVL.setUVs(m_MenuBackground.GetComponent<MeshFilter>().mesh, 483, 322, 2048, 915, 760);
        GAMEGLOBAL.LVL.setUVs(m_MenuBackHeaderText.GetComponent<MeshFilter>().mesh, 233, 55, 2048, 1418, 760);

        m_Buttons[0].init(null, ButtonScript.TYPE.PLAY);
        m_Buttons[1].init(null, ButtonScript.TYPE.DAILYQUEST);
        m_Buttons[2].init(null, ButtonScript.TYPE.EXIT);
        
    }

    private void pauseMenu()
    {
        GAMEGLOBAL.LVL.setUVs(m_MenuBackground.GetComponent<MeshFilter>().mesh, 483, 322, 2048, 915, 760);
        GAMEGLOBAL.LVL.setUVs(m_MenuBackHeaderText.GetComponent<MeshFilter>().mesh, 233, 55, 2048, 1418, 760);

        m_Buttons[0].init(null, ButtonScript.TYPE.RESUME);
        m_Buttons[1].init(null, ButtonScript.TYPE.RETRY);
        m_Buttons[2].init(null, ButtonScript.TYPE.DAILYQUEST);
        m_Buttons[3].init(null, ButtonScript.TYPE.EXIT);
    }

    private void failedMenu()
    {
        GAMEGLOBAL.LVL.setUVs(m_MenuBackground.GetComponent<MeshFilter>().mesh, 483, 322, 2048, 1407, 1089);
        GAMEGLOBAL.LVL.setUVs(m_MenuBackHeaderText.GetComponent<MeshFilter>().mesh, 233, 55, 2048, 1418, 830);

        m_Buttons[0].init(null, ButtonScript.TYPE.RETRY);
        m_Buttons[1].init(null, ButtonScript.TYPE.WATCHVIDEO);
        m_Buttons[2].init(null, ButtonScript.TYPE.EXIT);
        m_arrowBouncer.gameObject.SetActive(true);
        GAMEGLOBAL.LVL.m_Behaviour.bounce(m_arrowBouncer.gameObject);
    }

    private void successMenu()
    {
        GAMEGLOBAL.LVL.setUVs(m_MenuBackground.GetComponent<MeshFilter>().mesh, 483, 322, 2048, 915, 1089);
        GAMEGLOBAL.LVL.setUVs(m_MenuBackHeaderText.GetComponent<MeshFilter>().mesh, 233, 55, 2048, 1418, 900);

        m_Buttons[0].init(null, ButtonScript.TYPE.NEXT);
        m_Buttons[1].init(null, ButtonScript.TYPE.WATCHVIDEO);
        m_Buttons[2].init(null, ButtonScript.TYPE.EXIT);
        m_arrowBouncer.gameObject.SetActive(true);
        GAMEGLOBAL.LVL.m_Behaviour.bounce(m_arrowBouncer.gameObject);

        // Possibly display "New Block Visual"
        if (GAMEGLOBAL.LVL.m_currentLevel.m_Level < 25)
        {
            bool itsTime = false;

            switch (GAMEGLOBAL.LVL.m_currentLevel.m_Level + 1)
            {
                case 5: itsTime = true; m_newBlock.gameObject.SetActive(true); GAMEGLOBAL.LVL.setUVs(m_newBlock.gameObject.GetComponent<MeshFilter>().mesh, 322, 192, 2048, 666, 1817); break; // Switch
                case 10: itsTime = true; m_newBlock.gameObject.SetActive(true); GAMEGLOBAL.LVL.setUVs(m_newBlock.gameObject.GetComponent<MeshFilter>().mesh, 322, 192, 2048, 341, 1817); break; // Any  
                case 15: itsTime = true; m_newBlock.gameObject.SetActive(true); GAMEGLOBAL.LVL.setUVs(m_newBlock.gameObject.GetComponent<MeshFilter>().mesh, 322, 192, 2048, 17, 1817); break; // Bomb
                case 20: itsTime = true; m_newBlock.gameObject.SetActive(true); GAMEGLOBAL.LVL.setUVs(m_newBlock.gameObject.GetComponent<MeshFilter>().mesh, 322, 192, 2048, 17, 990); break; // Swap
                case 25: itsTime = true; m_newBlock.gameObject.SetActive(true); GAMEGLOBAL.LVL.setUVs(m_newBlock.gameObject.GetComponent<MeshFilter>().mesh, 322, 192, 2048, 17, 1315); break; // Gold
            }

            if(itsTime)
            {
                m_PrevMenuStatus = m_MenuStatus;
                m_MenuStatus = LEVEL_STATUS.NEWBLOCKVISUAL;
            }
        }
    }

    private void dailyQuestMenu()
    {
        m_MenuBackground.SetActive(false);
        m_MenuBackHeaderText.SetActive(false);
        m_ButtonsObject.SetActive(false);
        m_DailyQuestObject.SetActive(true);
        m_DailyQuestExitButton.gameObject.SetActive(true);
    }

    private void buyBlocksMenu()
    {
        m_MenuBackground.SetActive(false);
        m_MenuBackHeaderText.SetActive(false);
        m_ButtonsObject.SetActive(false);
        m_BuyBlocksObject.SetActive(true);

        for (int i = 0; i < m_BuyList.Count; i++)
            if (m_BuyList[i].m_selected)
                m_BuyList[i].deselect();

        m_BuyExitButton.gameObject.SetActive(true);
        m_BuyButton.gameObject.SetActive(true);

        // Gray out buy button if this
        if (GAMEGLOBAL.LVL.m_Coins < 100)
        {
            m_BuyButton.click();
            m_BuyTextInfo.setText("<c1>Not enough coins to buy.</c>");
        }
        else if(GAMEGLOBAL.LVL.getFreeSideBarSpace() == 0)
        {
            m_BuyButton.click();
            m_BuyTextInfo.setText("<c1>The sidebar is full with blocks.</c>");
        }

        
    }

    public void Run()
    {
        if (!m_Active)
        {
            // Each time "reset" buttons
            for (int i = 0; i < m_Buttons.Count; i++)
                m_Buttons[i].gameObject.SetActive(false);

            m_MenuStatus = GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus;

            // Depending on status, we initiate that menu
            switch (GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus)
            {
                case LEVEL_STATUS.MAINMENU: mainMenu(); break;
                case LEVEL_STATUS.PAUSED: pauseMenu(); break;
                case LEVEL_STATUS.FAILED: failedMenu(); break;
                case LEVEL_STATUS.SUCCESS: successMenu(); break;
                case LEVEL_STATUS.BUYBLOCKMENU: buyBlocksMenu(); break;
            }

            if(GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus == LEVEL_STATUS.BUYBLOCKMENU)
            {
                // Instantly move meny in this case...
                GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition = new Vector3(0, 0, 8);
                GAMEGLOBAL.LVL.m_Menu.SetActive(true);
                m_MenuBackground.SetActive(false);
                m_MenuBackHeaderText.SetActive(false);
            }
            else
                GAMEGLOBAL.LVL.m_Behaviour.menyMove(true);

            m_Active = true;
        }

        if (m_ToggleEscKey)
        {
            // Just toggle the esc key, otherwise GetKeyDown triggers twice and nothing happens
            m_ToggleEscKey = false;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && m_Active)
        {
            if (m_MenuStatus == LEVEL_STATUS.PAUSED)
            {
                pauseDone();
                GAMEGLOBAL.m_AntiPauseSpam = DateTime.Now.AddSeconds(2);
                return;
            }
            else if(m_MenuStatus == LEVEL_STATUS.DAILYQUESTMENU)
            {
                m_MenuBackground.SetActive(true);
                m_MenuBackHeaderText.SetActive(true);
                m_ButtonsObject.SetActive(true);
                m_DailyQuestObject.SetActive(false);
                m_MenuStatus = m_PrevMenuStatus;
                m_DailyQuestExitButton.gameObject.SetActive(false);
                return;
            }
            else if(m_MenuStatus == LEVEL_STATUS.NEWBLOCKVISUAL)
            {
                m_MenuStatus = m_PrevMenuStatus;
                m_newBlock.gameObject.SetActive(false);
                return;
            }
            else if(m_MenuStatus == LEVEL_STATUS.BUYBLOCKMENU)
            {
                GAMEGLOBAL.m_AntiPauseSpam = DateTime.Now.AddSeconds(2);
                m_BuyBlocksObject.SetActive(false);
                m_MenuBackground.SetActive(true);
                m_MenuBackHeaderText.SetActive(true);
                m_ButtonsObject.SetActive(true);
                m_SelectedBuyBlock = null;
                m_Active = false;
                GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = LEVEL_STATUS.RUNNING;
                GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition = new Vector3(0, 7, 8);
                GAMEGLOBAL.LVL.m_Menu.SetActive(false);
                return;
            }
        }

        // New block visual?
        if (m_MenuStatus == LEVEL_STATUS.NEWBLOCKVISUAL)
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_MenuStatus = m_PrevMenuStatus;
                m_newBlock.gameObject.SetActive(false);
            }
            return;
        }

        // Normal click
        if (Input.GetMouseButtonDown(0))
        {
            if (!Physics.Raycast(GAMEGLOBAL.LVL.m_Camera.ScreenPointToRay(Input.mousePosition), out m_Hit, 100))
                return;

            // We need to hit something (with a collider on it)
            if (!m_Hit.transform)
                return;

            // Buy Blocks Menu?
            if(m_MenuStatus == LEVEL_STATUS.BUYBLOCKMENU)
            {
                // Blocks
                BlockScript blockscript = m_Hit.collider.gameObject.GetComponent<BlockScript>();
                if(blockscript != null && blockscript.m_Location == BlockScript.BLOCKLOCATION.BUYMENU)
                {
                    // Select/Deselect
                    if (m_SelectedBuyBlock == null)
                    {
                        m_SelectedBuyBlock = blockscript.m_Block;
                        m_SelectedBuyBlock.select();
                    }
                    else if(m_SelectedBuyBlock.m_ID == blockscript.m_Block.m_ID)
                    {
                        m_SelectedBuyBlock.deselect();
                        m_SelectedBuyBlock = null;
                    }
                    else
                    {
                        m_SelectedBuyBlock.deselect();
                        m_SelectedBuyBlock = blockscript.m_Block;
                        m_SelectedBuyBlock.select();
                    }
                    return;
                }

                // Buttons
                ButtonScript bsc = m_Hit.collider.gameObject.GetComponent<ButtonScript>();
                if (bsc != null)
                {
                    if(bsc.gameObject.name == "BUYEXITBUTTON")
                    {
                        m_MenuBackground.SetActive(true);
                        m_MenuBackHeaderText.SetActive(true);
                        m_ButtonsObject.SetActive(true);
                        m_BuyBlocksObject.SetActive(false);
                        m_SelectedBuyBlock = null;
                        m_Active = false;
                        GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = LEVEL_STATUS.RUNNING;
                        GAMEGLOBAL.LVL.m_Menu.gameObject.transform.localPosition = new Vector3(0, 7, 8);
                        GAMEGLOBAL.LVL.m_Menu.SetActive(false);
                    }
                    else if(bsc.gameObject.name == "BUYBUTTON")
                    {
                        if(m_AntiSpamBuy < DateTime.Now && m_SelectedBuyBlock != null)
                        {
                            // Gray out buy button if this
                            if (GAMEGLOBAL.LVL.m_Coins < 0)
                            {
                                m_BuyButton.click();
                                m_BuyTextInfo.setText("<c1>Not enough coins to buy.</c>");
                            }
                            else if (GAMEGLOBAL.LVL.getFreeSideBarSpace() == 0)
                            {
                                m_BuyButton.click();
                                m_BuyTextInfo.setText("<c1>The sidebar is full with blocks.</c>");
                            }
                            else
                            {
                                // Add to sidebar
                                LetterBlock b = (LetterBlock)GAMEGLOBAL.LVL.getBlock(Block.TYPE.LETTER);
                                b.setCharacter(m_SelectedBuyBlock.m_LetterChar, m_SelectedBuyBlock.m_LetterValue);
                                b.m_Script.transform.position = new Vector3(GAMEGLOBAL.LVL.GAME_WIDTH + 0.5f, -2f, 0);
                                b.m_Script.gameObject.SetActive(true);
                                Rigidbody r = b.m_Script.GetComponent<Rigidbody>();
                                r.isKinematic = false;

                                // Deselect
                                m_SelectedBuyBlock.deselect();
                                m_SelectedBuyBlock = null;

                                // Save
                                GAMEGLOBAL.LVL.m_Coins -= 100;
                                GAMEGLOBAL.LVL.SavePlayerData(GAMEGLOBAL.LVL.m_Coins, GAMEGLOBAL.LVL.m_currentLevel.m_Level);

                                m_AntiSpamBuy = DateTime.Now.AddSeconds(1);

                                // Check again!
                                if (GAMEGLOBAL.LVL.m_Coins < 100)
                                {
                                    m_BuyButton.click();
                                    m_BuyTextInfo.setText("<c1>Not enough coins to buy more.</c>");
                                }
                                else if (GAMEGLOBAL.LVL.getFreeSideBarSpace() == 0)
                                {
                                    m_BuyButton.click();
                                    m_BuyTextInfo.setText("<c1>The sidebar is full with blocks.</c>");
                                }
                            }

                        }
                    }
                }
                return;
            }


            // Daily Quest Menu?
            if(m_MenuStatus == LEVEL_STATUS.DAILYQUESTMENU)
            {
                // Do quest button
                dailyQuestButtonScript ds = m_Hit.collider.gameObject.GetComponent<dailyQuestButtonScript>();
                if (ds != null)
                {
                    if(ds.m_Quest.m_Active)
                    {
                        // Play Video Ad.
                        GAMEGLOBAL.LVL.startVideo(ds);
                    }
                    return;
                }

                // Exit Button
                ButtonScript bsc = m_Hit.collider.gameObject.GetComponent<ButtonScript>();
                if (bsc != null && bsc.gameObject.name == "DQEXITBUTTON")
                {
                    m_MenuBackground.SetActive(true);
                    m_MenuBackHeaderText.SetActive(true);
                    m_ButtonsObject.SetActive(true);
                    m_DailyQuestObject.SetActive(false);
                    m_MenuStatus = m_PrevMenuStatus;
                    m_DailyQuestExitButton.gameObject.SetActive(false);
                }
                return;
            }

            ButtonScript bs = m_Hit.collider.gameObject.GetComponent<ButtonScript>();

            // Get marked button
            if (bs != null)
            {
                bs.click();
                switch (bs.m_ButtonType)
                {
                    case ButtonScript.TYPE.PLAY: playDone();  break;
                    case ButtonScript.TYPE.NEXT: nextDone(); break;
                    case ButtonScript.TYPE.RETRY: retryDone(); break;
                    case ButtonScript.TYPE.RESUME: pauseDone(); break;
                    case ButtonScript.TYPE.EXIT: exitDone(); break;
                    case ButtonScript.TYPE.DAILYQUEST:
                        m_PrevMenuStatus = m_MenuStatus;
                        m_MenuStatus = LEVEL_STATUS.DAILYQUESTMENU;
                        dailyQuestMenu();
                        break;
                    case ButtonScript.TYPE.WATCHVIDEO:
                        if(bs.m_Active)
                        {
                            GAMEGLOBAL.LVL.startVideo(null);
                            bs.m_Active = false;
                        }
                        break;
                }
            }
            GAMEGLOBAL.LVL.setHint(false, "");
        }

    }

    private void pauseDone()
    {
        GAMEGLOBAL.LVL.m_currentLevel.m_LevelStatus = LEVEL_STATUS.RUNNING;
        m_Active = false;
        GAMEGLOBAL.LVL.m_Behaviour.menyMove(false);
    }
    
    private void exitDone()
    {
        m_arrowBouncer.gameObject.SetActive(false);
        switch (m_MenuStatus)
        {
            case LEVEL_STATUS.MAINMENU: GAMEGLOBAL.LVL.exitGame(); break;   // Exit game
            case LEVEL_STATUS.PAUSED: m_MenuStatus = LEVEL_STATUS.MAINMENU; GAMEGLOBAL.LVL.m_Behaviour.doMoveCamera(CAMERA_MOVEMENT.EXIT_FROM_LEVEL); GAMEGLOBAL.LVL.m_Behaviour.menyMoveUpDown(); break;      // To main menu
            case LEVEL_STATUS.FAILED: m_MenuStatus = LEVEL_STATUS.MAINMENU; GAMEGLOBAL.LVL.m_Behaviour.doMoveCamera(CAMERA_MOVEMENT.EXIT_FROM_LEVEL); GAMEGLOBAL.LVL.m_Behaviour.menyMoveUpDown(); break;      // To main menu
            case LEVEL_STATUS.SUCCESS: m_MenuStatus = LEVEL_STATUS.MAINMENU; GAMEGLOBAL.LVL.m_Behaviour.doMoveCamera(CAMERA_MOVEMENT.EXIT_FROM_LEVEL); GAMEGLOBAL.LVL.m_Behaviour.menyMoveUpDown(); break;     // To main menu
            default: break;   // Should never happen
        }
    }

    private void retryDone()
    {
        m_Active = false;
        GAMEGLOBAL.LVL.destroyAll();
        GAMEGLOBAL.LVL.startLevel(GAMEGLOBAL.LVL.m_currentLevel.m_Level);
        GAMEGLOBAL.m_AntiPauseSpam = DateTime.Now.AddSeconds(2);
        GAMEGLOBAL.LVL.m_Behaviour.menyMove(false);
        m_arrowBouncer.gameObject.SetActive(false);
    }

    private void nextDone()
    {
        m_Active = false;
        GAMEGLOBAL.LVL.m_Behaviour.doMoveCamera(CAMERA_MOVEMENT.LEVEL_PROGRESSION);
        GAMEGLOBAL.LVL.destroyAll();
        GAMEGLOBAL.LVL.m_Behaviour.startNewLevel(1f, GAMEGLOBAL.LVL.m_currentLevel.m_Level + 1);
        GAMEGLOBAL.m_AntiPauseSpam = DateTime.Now.AddSeconds(2);
        GAMEGLOBAL.LVL.m_Behaviour.menyMove(false);
        m_arrowBouncer.gameObject.SetActive(false);
    }

    private void playDone()
    {
        m_Active = false;
        GAMEGLOBAL.LVL.m_Coins += 800;
        GAMEGLOBAL.LVL.LoadPlayerData();
        GAMEGLOBAL.LVL.destroyAll();
        GAMEGLOBAL.LVL.startLevel(GAMEGLOBAL.LVL.m_PlayerData.m_progress);
        GAMEGLOBAL.LVL.m_Behaviour.doMoveCamera(CAMERA_MOVEMENT.BEGIN);
        GAMEGLOBAL.m_AntiPauseSpam = DateTime.Now.AddSeconds(2);
        GAMEGLOBAL.LVL.m_Behaviour.menyMove(false);
    }

}
