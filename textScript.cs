﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class textScript : MonoBehaviour
{
    private List<Mesh> m_LetterList;
    private List<GameObject> m_LetterListGameObject;
    public string m_Txt;
    private bool m_Initiated = false;
    private float m_widthCalc;
    private float m_scale;

    public void init(float scale)
    {
        if(!m_Initiated)
        {
            m_scale = scale;
            if (scale < 0.5f)
                m_widthCalc = 0.009f;
            else
                m_widthCalc = scale / 45f;

            m_LetterList = new List<Mesh>(50);
            m_LetterListGameObject = new List<GameObject>(50);

            GameObject temp = (GameObject)Resources.Load("Prefabs/flat");
            Material material = temp.GetComponent<MeshRenderer>().sharedMaterial;
            Mesh flat = temp.GetComponent<MeshFilter>().sharedMesh;

            float zFix = 0.01f;
            for (int i = 0; i < 50; i++)
            {
                GameObject meshObject = new GameObject("LETTER_" + i);
                meshObject.transform.SetParent(gameObject.transform);
                meshObject.transform.localScale = new Vector3(m_scale, m_scale, 1f);
                meshObject.transform.rotation = Quaternion.Euler(0, 0, -90);

                MeshRenderer rnd = meshObject.AddComponent<MeshRenderer>();
                MeshFilter filter = meshObject.AddComponent<MeshFilter>();

                meshObject.transform.localPosition = new Vector3(i, 0, -1 - zFix);
                zFix += 0.01f;
                

                filter.sharedMesh = GameObject.Instantiate(flat);
                rnd.material = material;
                rnd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                rnd.receiveShadows = false;
                m_LetterListGameObject.Add(meshObject);
                m_LetterList.Add(filter.mesh);
            }
            m_Initiated = true;
        }
    }

    public void setUVSTRANSPARENT()
    {
        for (int i = 0; i < m_LetterList.Count; i++)
        {
            setUVs(m_LetterList[i], 40, 40, 2048, 700, 320);
        }
    }

    public void setText(string txt)
    {
        // Deactivate all letterobjects first
        for (int i = 0; i < m_LetterList.Count; i++)
            m_LetterListGameObject[i].SetActive(false);

        m_Txt = txt;
        setLetters(GAMEGLOBAL.LVL.getCharacters(m_Txt));
    }

    public void addText(string txt)
    {
        m_Txt += txt;
        setLetters(GAMEGLOBAL.LVL.getCharacters(m_Txt));
    }

    public void setLetters(List<TEXTURECHAR> chars)
    {
        float lastPos = 0;
        for (int i = 0; i < m_LetterList.Count; i++)
        {
            if (i < chars.Count)
            {
                // Activate this letterobject
                if (!m_LetterListGameObject[i].activeSelf)
                    m_LetterListGameObject[i].SetActive(true);

                // Set new UVs
                setUVs(m_LetterList[i], 40, 40, 2048, chars[i].uvx, chars[i].uvy + chars[i].color);

                if (i == 0)
                {
                    // If first letter
                    lastPos = m_LetterListGameObject[i].transform.localPosition.x;
                }
                else
                {
                    // Position each letter
                    m_LetterListGameObject[i].transform.localPosition = new Vector3(
                        lastPos + (chars[i - 1].width * m_widthCalc), // 0.01 beräknas så här, vidden 0.4/40pixlar = 0.01
                        m_LetterListGameObject[i].transform.localPosition.y,
                        m_LetterListGameObject[i].transform.localPosition.z);
                    lastPos = m_LetterListGameObject[i].transform.localPosition.x;
                }
            }
            else
                break;
        }
    }

    public void setUVs(Mesh m, float tileSize_x, float tileSize_y, float textureSize, float textureCoordinate_x, float textureCoordinate_y)
    {
        // To get top-left based coordinates...
        textureCoordinate_y = textureSize - tileSize_y - textureCoordinate_y;

        float offsetX = tileSize_x / textureSize;
        float offsetY = tileSize_y / textureSize;
        float x = textureCoordinate_x / textureSize;
        float y = textureCoordinate_y / textureSize;

        Vector2[] frontUVS = m.uv;

        frontUVS[0].x = x;
        frontUVS[0].y = y;

        frontUVS[1].x = x + offsetX;
        frontUVS[1].y = y;

        frontUVS[2].x = x + offsetX;
        frontUVS[2].y = y + offsetY;

        frontUVS[3].x = x;
        frontUVS[3].y = y + offsetY;

        m.uv = frontUVS;
    }
}
