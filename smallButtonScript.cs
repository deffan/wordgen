﻿using UnityEngine;
using System.Collections;

public class smallButtonScript : MonoBehaviour
{
    public class STATUS
    {
        public const int HIDDEN = 0;
        public const int GRAYED = 1;
        public const int SHOW = 2;
    }

    public class BUTTONTYPE
    {
        public const int BUY = 0;
        public const int SETTINGS = 1;
        public const int CANCEL = 2;
    }

    private Mesh m_flat = null;
    public int m_status;
    public int m_TYPE;

    public void init()
    {
        if (m_flat == null)
        {
            m_flat = gameObject.transform.GetComponent<MeshFilter>().mesh;
            m_TYPE = 0;
            m_status = 0;
        }
    }

    public void setType(int type)
    {
        m_TYPE = type;
    }

    public void setButtonStatus(int status)
    {
        if (m_status == STATUS.HIDDEN && status != m_status)
            gameObject.SetActive(true);
        else if (m_status == STATUS.HIDDEN)
        {
            gameObject.SetActive(false);
            return;
        }

        if (m_TYPE == BUTTONTYPE.BUY)
        {
            if(status == STATUS.GRAYED)
            {
                setUVs(52, 52, 2048, 404, 1338);
            }
            else
            {
                setUVs(52, 52, 2048, 184, 1338);
            }
        }
        else if(m_TYPE == BUTTONTYPE.SETTINGS)
        {
            setUVs(52, 52, 2048, 239, 1338);
        }
        else if (m_TYPE == BUTTONTYPE.CANCEL)
        {
            setUVs(113, 35, 2048, 0, 1354);
        }
    }

    public void setUVs(float tileSize_x, float tileSize_y, float textureSize, float textureCoordinate_x, float textureCoordinate_y)
    {
        // To get top-left based coordinates...
        textureCoordinate_y = textureSize - tileSize_y - textureCoordinate_y;

        float offsetX = tileSize_x / textureSize;
        float offsetY = tileSize_y / textureSize;
        float x = textureCoordinate_x / textureSize;
        float y = textureCoordinate_y / textureSize;

        Vector2[] frontUVS = m_flat.uv;

        frontUVS[0].x = x;
        frontUVS[0].y = y;

        frontUVS[1].x = x + offsetX;
        frontUVS[1].y = y;

        frontUVS[2].x = x + offsetX;
        frontUVS[2].y = y + offsetY;

        frontUVS[3].x = x;
        frontUVS[3].y = y + offsetY;

        m_flat.uv = frontUVS;
    }

}
